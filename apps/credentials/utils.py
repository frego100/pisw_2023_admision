import io

from reportlab.lib.pagesizes import letter, A5

from django.utils.translation import gettext_lazy as _

from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Table, SimpleDocTemplate, Paragraph, Image, TableStyle, Spacer
from reportlab.lib import colors
from reportlab.lib.units import inch

from apps.assignments.utils.lists import AssignedRole
def _setBold(text: str) -> str:
    return '<b>' + text + '</b>'

def clean_text(text):
    return "" if text == "None" else text

def simple_paragraph(text: str, alignment: int = 0, fontSize: int = 9) -> Paragraph:
    cleaned_text = clean_text(text)
    paragraph_style = ParagraphStyle(
        'paragraph_style',
        fontSize=fontSize,
        alignment=alignment,
        spaceBefore=0,
        spaceAfter=0,
    )
    return Paragraph(
        cleaned_text,
        paragraph_style
    )

def bold_paragraph(text: str, alignment: int = 0, fontSize: int = 9) -> Paragraph:
    cleaned_text = clean_text(text)
    paragraph_style = ParagraphStyle(
        'paragraph_style',
        fontSize=fontSize,
        alignment=alignment
    )
    return Paragraph(
        _setBold(cleaned_text),
        paragraph_style
    )

def simple_title(text: str , fontSize: int) -> Paragraph:
    cleaned_text = clean_text(text)
    title_style = ParagraphStyle(
        'title_style',
        parent=getSampleStyleSheet()["Title"],
        fontSize=fontSize,
    )
    return Paragraph(
        cleaned_text,
        title_style
    )

def table_custom(data, columns=5):
    if not data:
        return simple_paragraph("No hay elementos")
    
    data_split = [data[i:i + columns] for i in range(0, len(data), columns)]

    style = TableStyle([
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
        ('TOPPADDING', (0, 0), (-1, -1), 0),
    ])

    table = Table(data_split, style=style)

    return table


def add_content_with_spacer(content, generated_content, spacer_height=40):
    content.append(generated_content)
    content.append(Spacer(1, spacer_height))


def firma():

    table_data = [
        [bold_paragraph(
                "",
                alignment=1,
                fontSize=20  
                )
            ],
        [bold_paragraph(
                "Firma",
                alignment=1,
                fontSize=10
                )
            ],
    ]

    row_heights_reverse = [
        0.7*inch,
        0.3*inch,
        ]
    
    style_reverse = TableStyle([
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
        ('TOPPADDING', (0, 0), (-1, -1), 0),
        ('LEFTPADDING', (0, 0), (-1, -1), 10),
        ('RIGHTPADDING', (0, 0), (-1, -1), 10),
        ('LINEABOVE',(0,1),(0,1),1,colors.black),
        ])
    
    table_reverse = Table(table_data, style=style_reverse, rowHeights=row_heights_reverse)

    return table_reverse
def generate_credential(instance):
    buffer = io.BytesIO()
    left_margin = 10
    right_margin = 10
    top_margin = 10
    bottom_margin = 10 

    pdf = SimpleDocTemplate(
        buffer,
        pagesize=A5,
        leftMargin=left_margin,
        rightMargin=right_margin,
        topMargin=top_margin,
        bottomMargin=bottom_margin
    )
    content = []

    table_data = [
        [bold_paragraph(
                instance.raffle_winner_assignment.raffle_winner.raffle.round_inscription.evaluation.minimal_exam_name(),
                alignment=1,
                fontSize=15  
                )
            ],
        [bold_paragraph(
                instance.raffle_winner_assignment.raffle_winner.raffle.round_inscription.evaluation.minimal_evaluation_name(),
                alignment=1,
                fontSize=12
                )
            ],
        [simple_paragraph(
                instance.raffle_winner_assignment.raffle_winner.user_winner.full_name(),
                alignment=1,
                fontSize=15
                )
            ],
        [bold_paragraph(
                f"{AssignedRole(instance.raffle_winner_assignment.assigned_role).label}",
                alignment=1,
                fontSize=20
                )
            ],
        [simple_paragraph(
                f"AREA: {instance.raffle_winner_assignment.assigned_entrance.area.name}",
                alignment=0,
                fontSize=15
                )
            ],
        [simple_paragraph(
                f"COORDINACIÓN: {instance.raffle_winner_assignment.assigned_coordination.name}",
                alignment=0,
                fontSize=15
                )
            ],
        [simple_paragraph(
                f"ENTRADA: {instance.raffle_winner_assignment.assigned_entrance.name}",
                alignment=0,
                fontSize=15
                )
            ]
    ]

    row_heights = [
        0.5*inch,
        0.5*inch,
        0.5*inch,
        0.5*inch,
        0.5*inch,
        0.5*inch,
        0.5*inch
        ]

    style = TableStyle([
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
        ('TOPPADDING', (0, 0), (-1, -1), 0),
        ('LEFTPADDING', (0, 0), (-1, -1), 10),
        ('RIGHTPADDING', (0, 0), (-1, -1), 10),
        ('BOX', (0, 0), (-1, -1), 1, colors.black)
        ])
    
    table = Table(table_data, style=style, rowHeights=row_heights)
    

    add_content_with_spacer(content=content,generated_content=table)
    #content.append(table)

    table_data_reverse = [
        [bold_paragraph(
                f"FECHA: {instance.raffle_winner_assignment.raffle_winner.raffle.round_inscription.evaluation.datetime_formated()}",
                alignment=0,
                fontSize=10  
                )
            ],
        [bold_paragraph(
                f"DNI: {instance.raffle_winner_assignment.raffle_winner.user_winner.full_dni_with_name()}",
                alignment=0,
                fontSize=10
                )
            ],
        [firma()],
        [simple_paragraph(
                "Al ingresar y salir del área debe registrar su hora de ingreso y salida en el control de la puerta y al finalizar su labor, deberá depositar la presente credencial debidamente firmada, ya que solamente ambos (registro de ingreso y credencial firmada) se consideran para el control de su asistencia y pago correspondiente. En caso de verificarse que usted tiene alguna actuvidad laboral para la universidad en el amisma fecha y hora del examen no procederá el pago correspondiente.",
                alignment=1,
                fontSize=9
                )
            ],
    ]
    row_heights_reverse = [
        0.5*inch,
        0.5*inch,
        1*inch,
        1.5*inch,
        ]
    
    style_reverse = TableStyle([
        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
        ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
        ('TOPPADDING', (0, 0), (-1, -1), 0),
        ('LEFTPADDING', (0, 0), (-1, -1), 10),
        ('RIGHTPADDING', (0, 0), (-1, -1), 10),
        ('GRID', (0, 0), (-1, -1), 1, colors.black)
        ])
    
    table_reverse = Table(table_data_reverse, style=style_reverse, rowHeights=row_heights_reverse)
    
    content.append(table_reverse)
    #add_content_with_spacer(content=content,generated_content=table_reverse)

    pdf.build(content)
    buffer.seek(0)

    return buffer
