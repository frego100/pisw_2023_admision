from rest_framework import viewsets
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser
    )

from utils.permissions import (
    IsAdminOrReadOnly,
)

from django_filters.rest_framework import DjangoFilterBackend

from apps.credentials.models import (
    Credential,
    )

from .serializers import (
    CredentialSerializer,
    )


class CredentialViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminOrReadOnly,
        ]
    model = Credential
    serializer_class = CredentialSerializer
    queryset = Credential.objects.filter(
        is_active = True
        )
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['raffle_winner_assignment','is_active']