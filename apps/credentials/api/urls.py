from django.urls import path 

from rest_framework.routers import DefaultRouter

from .views import (
    CredentialViewSet,
)

router = DefaultRouter()
router.register(r'api/credential/credential', CredentialViewSet, basename="credential")

urlpatterns = [

]

urlpatterns += router.urls
