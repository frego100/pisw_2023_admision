from rest_framework import serializers

from apps.credentials.models import (
    Credential,
    )

from apps.assignments.api.serializers import (
    RaffleWinnerAssignmentSerializer,
    )

class CredentialSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['raffle_winner_assignment'] = RaffleWinnerAssignmentSerializer(instance.raffle_winner_assignment).data
        return representation   
    
    def create(self, validated_data):
        credential_instance = super().create(validated_data)
        credential_instance.generate_credential_file()
        return credential_instance
    
    class Meta:
        model = Credential
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }


class MinimalCredentialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Credential
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }