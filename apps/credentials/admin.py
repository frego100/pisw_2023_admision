from django.contrib import admin

from .models import (
    Credential
    )

class CredentialAdminConfig(admin.ModelAdmin):
    model = Credential

    list_display = (
        'id',
        'raffle_winner_assignment',
        'credential_file',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

admin.site.register(Credential,CredentialAdminConfig)