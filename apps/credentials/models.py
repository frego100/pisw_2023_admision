import io
from django.core.files.base import ContentFile

from reportlab.lib.pagesizes import letter, A5


from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.abstractmodels import BaseModel

from apps.assignments.models import RaffleWinnerAssignment

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle
from reportlab.lib import colors

from .utils import generate_credential

class Credential(BaseModel):

    raffle_winner_assignment = models.ForeignKey(
        RaffleWinnerAssignment,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name = _('winner')
        )
        
    credential_file = models.FileField(
        upload_to='credentials/',
        null=True,
        blank=True,
        verbose_name=_('file')
        )
    
    def generate_credential_file(self):

        buffer = generate_credential(self)

        filename = f"credentail_{self.id}.pdf"
    
        self.credential_file.save(filename, ContentFile(buffer.read()), save=True)