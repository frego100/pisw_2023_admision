from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.permissions import (
    IsAuthenticated, 
    IsAdminUser
    )
from apps.assignments.models import RaffleWinnerAssignment

from .serializers import (
    RaffleWinnerAssignmentSerializer,
)

#excel
import pandas as pd
from django.http import HttpResponse
from io import BytesIO
from rest_framework import status
from django_filters.rest_framework import DjangoFilterBackend

class RaffleWinnerAssignmentViewSet(viewsets.ModelViewSet):
    queryset = RaffleWinnerAssignment.objects.all()
    permission_classes = [
        IsAuthenticated,
        IsAdminUser
    ]
    serializer_class  = RaffleWinnerAssignmentSerializer

    queryset = RaffleWinnerAssignment.objects.filter(
        is_active = True
        )
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['raffle_winner__raffle']

    def list(self, request, *args, **kwargs):
   
        # Filtrar el queryset por el ID de la rifa
        queryset = self.filter_queryset(self.get_queryset())
        
        export_excel = request.query_params.get("export_excel", None)
        if export_excel:
            formatted_data = self.format_data(queryset)
            response = self.create_excel_response(formatted_data)
            return response
        
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def format_data(self, queryset):
        formatted_data = []
        for item in queryset:
            formatted_item = {
                'Rol asignado': item.assigned_role,
                'Nombre de la coordinación asignada': item.assigned_coordination.name,
                'Nombre de la entrada asignada': item.assigned_entrance.name,
                'Nombre del ganador del sorteo': item.raffle_winner.user_winner.first_name,
                'Apellido del ganador del sorteo': item.raffle_winner.user_winner.last_name,
                'Correo del ganador del sorteo': item.raffle_winner.user_winner.email,
                'DNI del ganador del sorteo': item.raffle_winner.user_winner.dni,
                'Área asignada al ganador': item.assigned_entrance.area.name
            }
            formatted_data.append(formatted_item)
        return formatted_data

    def create_excel_response(self, formatted_data):
        df = pd.DataFrame(formatted_data)
        excel_buffer = BytesIO()
        writer = pd.ExcelWriter(excel_buffer, engine='xlsxwriter')
        df.to_excel(writer, index=False)
        writer.close()
        excel_data = excel_buffer.getvalue()
        response = HttpResponse(excel_data, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename="datos.xlsx"'
        return response