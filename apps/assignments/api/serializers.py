from rest_framework import serializers
from apps.assignments.models import RaffleWinnerAssignment, AssignmentClassroom, AssignmentPavilion, AssignmentSchool
from apps.assignments.utils.lists import AssignedRole
from apps.areas.models import Classroom, Pavilion, School
from apps.coordinations.api.serializers import CoordinationSerializer
from apps.areas.api.serializers import EntranceSerializer
from apps.raffles.api.serializers import RaffleWinnerSerializer

class AssignmentClassroomSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssignmentClassroom
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class AssignmentPavilionSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssignmentPavilion
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class AssignmentSchoolSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssignmentSchool
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class MinimalRaffleWinnerAssignmentSerializer(serializers.ModelSerializer):
    is_user_with_credential = serializers.SerializerMethodField()
    credential = serializers.SerializerMethodField()

    def get_is_user_with_credential(self, obj):
        from apps.credentials.models import Credential
        credential = Credential.objects.filter(
            raffle_winner_assignment = obj,
            is_active = True
            ).first()
        
        if credential:
            return True
            
        return False
    
    def get_credential(self, obj):
        from apps.credentials.models import Credential
        from apps.credentials.api.serializers import MinimalCredentialSerializer
        
        credential = Credential.objects.filter(
            raffle_winner_assignment = obj,
            is_active = True
            ).first()
        
        if credential:
            return MinimalCredentialSerializer(credential).data
            
        return None
    
    class Meta:
        model = RaffleWinnerAssignment
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class RaffleWinnerAssignmentSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)
    assigned_id = serializers.UUIDField(write_only=True, required=True)
    is_user_with_credential = serializers.SerializerMethodField()
    credential = serializers.SerializerMethodField()

    def get_is_user_with_credential(self, obj):
        from apps.credentials.models import Credential
        credential = Credential.objects.filter(
            raffle_winner_assignment = obj,
            is_active = True
            ).first()
        
        if credential:
            return True
            
        return False
    
    def get_credential(self, obj):
        from apps.credentials.models import Credential
        from apps.credentials.api.serializers import MinimalCredentialSerializer
        
        credential = Credential.objects.filter(
            raffle_winner_assignment = obj,
            is_active = True
            ).first()
        
        if credential:
            return MinimalCredentialSerializer(credential).data
            
        return None

    class Meta:
        model = RaffleWinnerAssignment
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

    def validate(self, data):
        assigned_id = data.get('assigned_id')
        assigned_role = data.get('assigned_role')

        model_mapping = {
            AssignedRole.SUPERVISOR: Classroom,
            AssignedRole.ADMINISTRATOR: Pavilion,
            AssignedRole.TECHNICIAN: Pavilion,
            AssignedRole.CONTROLLER: School,
        }

        referenced_model = model_mapping.get(assigned_role)

        if assigned_id and referenced_model:
            try:
                referenced_model.objects.get(id=assigned_id)
            except referenced_model.DoesNotExist:
                raise serializers.ValidationError(f"The assigned_id in {referenced_model.__name__} does not exist for role {assigned_role}")

        return data
    
    def create(self, validated_data):
        assigned_id = validated_data.pop('assigned_id', None)
        raffle_winner = validated_data.get('raffle_winner', None)

        rwa = RaffleWinnerAssignment.objects.filter(raffle_winner=raffle_winner, is_active = True)
        
        for rafllewinner in rwa:
            rafllewinner.delete()
            
        raffle_winner_assignment = RaffleWinnerAssignment.objects.create(**validated_data)
        
        if assigned_id is not None:
            raffle_winner_assignment.create_assignment_object(assigned_id=assigned_id)

            
            
        return raffle_winner_assignment

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        assigned_id = validated_data.get('assigned_id', None)

        if assigned_id is not None:
            instance.create_assignment_object(assigned_id=assigned_id)
            
        return instance
        
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        try:
            assignment_data = self._get_assignment_data(instance)
            representation['assignment'] = assignment_data
        except ValueError as e:
            representation['assignment'] = None   

        representation['assigned_coordination'] = CoordinationSerializer(instance.assigned_coordination).data
        representation['assigned_entrance'] = EntranceSerializer(instance.assigned_entrance).data
        representation['raffle_winner'] = RaffleWinnerSerializer(instance.raffle_winner).data

        return representation  


    def _get_assignment_data(self, instance):
        assignment_object = instance.get_assignment_object()
        
        if isinstance(assignment_object, AssignmentClassroom):
            assignment_serializer = AssignmentClassroomSerializer(assignment_object)
        elif isinstance(assignment_object, AssignmentPavilion):
            assignment_serializer = AssignmentPavilionSerializer(assignment_object)
        elif isinstance(assignment_object, AssignmentSchool):
            assignment_serializer = AssignmentSchoolSerializer(assignment_object)
        else:
            raise ValueError("No se puede determinar el tipo de modelo para el assignment_object")

        return assignment_serializer.data