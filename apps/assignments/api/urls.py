from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import RaffleWinnerAssignmentViewSet

router = DefaultRouter()
router.register(r'api/rafflewinnerassignment', RaffleWinnerAssignmentViewSet, basename='assignment')

urlpatterns = [
    path('', include(router.urls)),
]