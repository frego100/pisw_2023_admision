from django.contrib import admin

from .models import (
    RaffleWinnerAssignment,
    AssignmentClassroom,
    AssignmentPavilion,
    AssignmentSchool
    )

class AssignmentSchoolAdminConfig(admin.ModelAdmin):
    model = AssignmentSchool

    list_display = (
        'id',
        'raffle_winner_assignment',
        'school',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class AssignmentPavilionAdminConfig(admin.ModelAdmin):
    model = AssignmentPavilion

    list_display = (
        'id',
        'raffle_winner_assignment',
        'pavilion',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class AssignmentClassroomAdminConfig(admin.ModelAdmin):
    model = AssignmentClassroom

    list_display = (
        'id',
        'raffle_winner_assignment',
        'classroom',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class RaffleWinnerAssignmentAdminConfig(admin.ModelAdmin):
    model = RaffleWinnerAssignment

    list_display = (
        'id',
        'assigned_coordination',
        'assigned_entrance',
        'raffle_winner',
        'assigned_role',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

admin.site.register(RaffleWinnerAssignment,RaffleWinnerAssignmentAdminConfig)
admin.site.register(AssignmentPavilion,AssignmentPavilionAdminConfig)
admin.site.register(AssignmentClassroom,AssignmentClassroomAdminConfig)
admin.site.register(AssignmentSchool,AssignmentSchoolAdminConfig)
