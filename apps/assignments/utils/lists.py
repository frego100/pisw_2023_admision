from django.db import models
from django.utils.translation import gettext_lazy as _

class AssignedRole(models.TextChoices):
    SUPERVISOR = "SUPERVISOR", _("SUPERVISOR")
    ADMINISTRATOR = "ADMINISTRATOR", _("ADMINISTRADOR")
    TECHNICIAN = "TECHNICIAN", _("TECNICO")
    CONTROLLER = "CONTROLLER", _("CONTROLLADOR")