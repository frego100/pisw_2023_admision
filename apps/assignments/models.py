from collections.abc import Iterable
from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.abstractmodels import BaseModel

from apps.coordinations.models import Coordination
from apps.areas.models import Entrance, Classroom, Pavilion, School
from apps.raffles.models import RaffleWinner

from .utils.lists import (
    AssignedRole,
    )

class RaffleWinnerAssignment(BaseModel):

    assigned_coordination = models.ForeignKey(
        Coordination,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('coordination')
        )
        
    assigned_entrance = models.ForeignKey(
        Entrance,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('entrance')
        )

    raffle_winner = models.ForeignKey(
        RaffleWinner,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('winner')
        )
    
    assigned_role = models.CharField(
        max_length=50, 
        choices=AssignedRole, 
        default=AssignedRole.SUPERVISOR
        )
    
    def delete(self, using=None, keep_parents=False):

        self.delete_assignments()
        self.is_active = False
        self.save()
    
    def delete_assignments(self):
        from apps.credentials.models import Credential

        Credential.objects.filter(raffle_winner_assignment=self).delete()   

        for assignment_class in (AssignmentClassroom, AssignmentPavilion, AssignmentSchool):
            assignments = assignment_class.objects.filter(raffle_winner_assignment=self,is_active = True)
            for assignment in assignments:
                assignment.delete()

    def create_assignment_object(self, assigned_id=None):
        self.delete_assignments()

        role_to_model = {
            AssignedRole.SUPERVISOR: (AssignmentClassroom, Classroom),
            AssignedRole.ADMINISTRATOR: (AssignmentPavilion, Pavilion),
            AssignedRole.TECHNICIAN: (AssignmentPavilion, Pavilion),
            AssignedRole.CONTROLLER: (AssignmentSchool, School),
        }

        assignment_class, model_class = role_to_model.get(self.assigned_role, (None, None))

        from apps.credentials.models import Credential

        credential = Credential.objects.create(raffle_winner_assignment=self)
        credential.generate_credential_file()

        if assignment_class and model_class:
            model_instance = model_class.objects.get(id=assigned_id) if assigned_id else None
            return assignment_class.objects.create(raffle_winner_assignment=self, **{model_class.__name__.lower(): model_instance})
        else:
            raise ValueError(f"Invalid role: {self.assigned_role}")
        

    def get_assignment_object(self):
        role_to_assignment = {
            AssignedRole.SUPERVISOR: AssignmentClassroom,
            AssignedRole.ADMINISTRATOR: AssignmentPavilion,
            AssignedRole.TECHNICIAN: AssignmentPavilion,
            AssignedRole.CONTROLLER: AssignmentSchool,
        }

        assignment_class = role_to_assignment.get(self.assigned_role, None)

        if assignment_class:
            try:
                return assignment_class.objects.get(raffle_winner_assignment=self)
            except assignment_class.DoesNotExist as e:
                raise ValueError(f"The referenced {assignment_class.__name__.lower()} does not exist: {e}")
        else:
            raise ValueError(f"Invalid role: {self.assigned_role}")


class AssignmentClassroom(BaseModel):

    raffle_winner_assignment = models.ForeignKey(
        RaffleWinnerAssignment,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('winner')
        )
    
    classroom = models.ForeignKey(
        Classroom,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('classroom')
        )

class AssignmentPavilion(BaseModel):

    raffle_winner_assignment = models.ForeignKey(
        RaffleWinnerAssignment,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('winner')
        )
    
    pavilion = models.ForeignKey(
        Pavilion,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('pavilion')
        )

class AssignmentSchool(BaseModel):

    raffle_winner_assignment = models.ForeignKey(
        RaffleWinnerAssignment,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('winner')
        )
    
    school = models.ForeignKey(
        School,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _('school')
        )