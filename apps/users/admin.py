from django.contrib import admin

from .models import User


class UserAdminConfig(admin.ModelAdmin):
    list_display = (
        'id',
        'email',
        'first_name',
        'last_name',
        'is_active',
        'user_type',
        'dni',
        'work',
        'created_at',
        'updated_at'
        )

    list_filter = (
        'is_active', 
        'user_type',
        )

    search_fields = (
        'email', 
        'first_name',
        'last_name',
        )

    ordering = ('-created_at',)

admin.site.register(User, UserAdminConfig)