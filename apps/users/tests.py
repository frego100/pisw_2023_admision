from django.test import TestCase
from django.contrib.auth import get_user_model
from .utils.lists import UserType

class UserModelTest(TestCase):
    def setUp(self):
        self.user_data = {
            'email': 'test@example.com',
            'user_type': UserType.TEACHER,
            'password': 'password123',
            'name': 'John Doe',
            'work': 'Teacher Work',
            'dni': '1234567890',
            'is_active': True,
            'is_staff': True,
        }

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(**self.user_data)

        self.assertEqual(user.email, self.user_data['email'])
        self.assertEqual(user.user_type, self.user_data['user_type'])
        self.assertTrue(user.check_password(self.user_data['password']))
        self.assertEqual(user.name, self.user_data['name'])
        self.assertEqual(user.work, self.user_data['work'])
        self.assertEqual(user.dni, self.user_data['dni'])
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        User = get_user_model()
        admin_user = User.objects.create_superuser(**self.user_data)

        self.assertEqual(admin_user.email, self.user_data['email'])
        self.assertEqual(admin_user.user_type, UserType.ADMIN)
        self.assertTrue(admin_user.check_password(self.user_data['password']))
        self.assertEqual(admin_user.name, self.user_data['name'])
        self.assertEqual(admin_user.work, self.user_data['work'])
        self.assertEqual(admin_user.dni, self.user_data['dni'])
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_superuser)

    def test_has_perm(self):
        User = get_user_model()
        user = User.objects.create_user(**self.user_data)

        self.assertFalse(user.has_perm('some_permission'))

        user.is_active = True
        user.user_type = UserType.ADMIN
        user.is_superuser = True
        user.save()

        self.assertTrue(user.has_perm('some_permission'))

    def test_has_module_perms(self):
        User = get_user_model()
        user = User.objects.create_user(**self.user_data)

        self.assertFalse(user.has_module_perms('some_module'))

        user.is_active = True
        user.user_type = UserType.ADMIN
        user.is_superuser = True
        user.save()

        self.assertTrue(user.has_module_perms('some_module'))
