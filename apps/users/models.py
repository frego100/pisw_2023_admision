from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.utils.translation import gettext_lazy as _

from .utils.lists import UserType
from utils.abstractmodels import BaseModel

# Create your models here.

class UserManager(BaseUserManager):
    def _create_user(self, password, email, is_superuser,user_type,**extra_fields):
        user = self.model(
            password = password,
            email = email,
            is_superuser = is_superuser,
            user_type = user_type,
            **extra_fields
        )

        user.save(using = self.db)
        return user

    def create_user(self, email,user_type, password=None , **extra_fields):
        return self._create_user(password, email, False,user_type, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        return self._create_user( password, email, True,"ADMIN", **extra_fields)
    

class User(BaseModel, AbstractBaseUser, PermissionsMixin):

    first_name = models.CharField(
        _("first name"),
        max_length=255,
        blank=True,
        null=False
        )
    
    last_name = models.CharField(
        _("last name"),
        max_length=255,
        blank=True,
        null=False
        )

    work = models.CharField(
        _("work"),
        max_length=255,
        blank=True,
        null=False
        )
    email = models.EmailField(
        _("email address"),
        max_length=255,
        unique=True,
        blank=False,
        null=False
        )

    user_type = models.CharField(
        _("user type"),
        max_length=100,
        choices=UserType,
        blank=False,
        null=False,
        default=UserType.TEACHER
        )
    
    #TIPO DE CONTRATO
    
    dni = models.CharField(
        _("DNI"),
        max_length=10,
        blank=False,
        unique=True,
        null=True
        )
    
    is_active = models.BooleanField(
        default=True
        )
    
    is_superuser = models.BooleanField(
        default=False
        )
    
    groups = None

    user_permissions = None
    
    last_login = None
    
    objects = UserManager()

    class Meta:
        verbose_name = _("usuario")
        verbose_name_plural = _("usuarios")

    USERNAME_FIELD = 'email'

    EMAIL_FIELD = 'email'

    REQUIRED_FIELDS = []

    def save(self, *args, **kwargs):
        user = super(User, self)
        if User.objects.filter(pk=user.pk).count() > 0:
            user_m = User.objects.filter(pk=user.pk)
            if user_m[0].password != self.password and self.password.find('_sha256') < 0:
                user.set_password(self.password)
        else:
            user.set_password(self.password)
        user.save(*args, **kwargs)
        return user
    
    def has_perm(self, perm, obj=None):
        if self.is_active and self.user_type == UserType.ADMIN and self.is_superuser == True:
            return True
        return False

    def has_module_perms(self, app_label):
        if self.is_active and self.user_type == UserType.ADMIN and self.is_superuser == True:
            return True
        return False

    def full_name(self):
        palabras = self.first_name.split()
        resultado = ", ".join(palabras)
        return f"{self.last_name.upper()}/{resultado}"
    
    def full_dni_with_name(self):
        palabras = self.first_name.split()
        resultado = ", ".join(palabras)
        return f"{self.dni} ({self.last_name.upper()}/{resultado})"
    
    @property
    def is_staff(self):
        return self.user_type == UserType.ADMIN
    
    def __str__(self):
        return self.first_name + " " +self.last_name