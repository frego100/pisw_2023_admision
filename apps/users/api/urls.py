from django.urls import path 

from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from .views import (
    UserViewSet,
    TransformGoogleTokenView,
    )

router = DefaultRouter()
router.register(r'api/user/user', UserViewSet, basename="user")

urlpatterns = [
    path('api/auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/auth/transformGoogleToken/',TransformGoogleTokenView.as_view(), name='token_obtain_pair_from_google'),
    ]

urlpatterns += router.urls
