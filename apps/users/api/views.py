import requests

from rest_framework import status
from rest_framework import viewsets
from rest_framework.utils import json
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import (
    IsAuthenticated, 
    IsAdminUser
    )

from rest_framework_simplejwt.tokens import RefreshToken

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter

from apps.users.models import (
    User
    )

from apps.users.utils.lists import UserType

from .serializers import (
    BasicUserSerializer, 
    AdminUserSerializer,
    )

from utils.paginations import CustomPagination

from .permissions import UserReadCreatePermission

class TransformGoogleTokenView(APIView):
    permission_classes = []

    def post(self, request):
        r = requests.get('https://www.googleapis.com/oauth2/v2/userinfo', params=request.data)
        data = json.loads(r.text)
        if 'error' in data:
            content = {
                "message": 'The token has errors!',
                "error": data['error']['message'],
                }
            return Response(content,status=status.HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(email=data['email'])
        except User.DoesNotExist:
            content = {"message": "The user does not have access"}
            return Response(content,status=status.HTTP_401_UNAUTHORIZED)
        token = RefreshToken.for_user(user)
        response = {
            'user': BasicUserSerializer(user).data,
            'access_token': str(token.access_token),
            'refresh_token': str(token),
        }
        return Response(response, status=status.HTTP_200_OK)

class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated, 
        UserReadCreatePermission
        ]
    model = User
    queryset = User.objects.filter(
        is_active = True
    )
    filter_backends = [DjangoFilterBackend, SearchFilter]
    #pagination_class = CustomPagination
    filterset_fields = ['user_type','is_active']
    search_fields = ['name', 'work','email','dni']
    
    def get_serializer_class(self):
        if self.request.user and self.request.user.user_type == UserType.ADMIN:
            return AdminUserSerializer
        return BasicUserSerializer
    