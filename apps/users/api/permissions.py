from rest_framework import permissions
from ..utils.lists import UserType

class UserReadCreatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        is_admin = request.user.user_type == UserType.ADMIN
        
        if view.action == 'list' or view.action == 'create':
            return is_admin
        elif view.action in ['retrieve', 'update', 'partial_update', 'destroy']:
            return True
        else:
            return False
        
    def has_object_permission(self, request, view, obj):
        is_admin = request.user.user_type == UserType.ADMIN
        
        if view.action in ['retrieve', 'update', 'partial_update']:
            return obj == request.user or is_admin
        elif view.action == 'destroy':
            return is_admin
        else:
            return False