from django.contrib import admin
from .models import (
    RoundInscription,
    UserInscription,
    PersonInscription
    )

# Register your models here.

class RoundInscriptionAdminConfig(admin.ModelAdmin):
    model = RoundInscription

    list_display = (
        'id',
        'evaluation',
        'created_by',
        'is_blocked',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class UserInscriptionAdminConfig(admin.ModelAdmin):
    model = UserInscription

    list_display = (
        'id',
        'round_inscription',
        'user_inscripted',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)


class PersonInscriptionAdminConfig(admin.ModelAdmin):
    model = PersonInscription

    list_display = (
        'id',
        'round_inscription',
        'person_inscripted',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

admin.site.register(RoundInscription,RoundInscriptionAdminConfig)
admin.site.register(UserInscription,UserInscriptionAdminConfig)
admin.site.register(PersonInscription,PersonInscriptionAdminConfig)