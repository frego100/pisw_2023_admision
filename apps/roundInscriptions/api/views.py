from django.shortcuts import render

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from django_filters.rest_framework import DjangoFilterBackend

from apps.roundInscriptions.models import (
    RoundInscription,
    UserInscription,
    )

from .serializers import (
    RoundInscriptionSerializer,
    UserInscriptionSerializer,
    )

from .permissions import (
    UserInscriptionPreferences,
    )

from utils.permissions import IsAdminOrReadOnly
# Create your views here.

class RoundInscriptionViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminOrReadOnly,
        ]
    model = RoundInscription
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['evaluation', 'created_by','is_active']
    serializer_class = RoundInscriptionSerializer

    queryset = RoundInscription.objects.filter(
        is_active = True
        )

class UserInscriptionViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        UserInscriptionPreferences,
        ]
    model = UserInscription
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['round_inscription', 'user_inscripted','is_active']
    serializer_class = UserInscriptionSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return UserInscription.objects.filter(
                is_active = True
            )
        else:
            return UserInscription.objects.filter(
                user_inscripted=user,
                is_active = True
                )
