from rest_framework import permissions
from rest_framework.permissions import BasePermission, SAFE_METHODS


class UserInscriptionPreferences(permissions.BasePermission):
    def has_permission(self, request, view):
        if view.action in ['list', 'retrieve', 'create', 'destroy']:
            return True
        else:
            return False
        
    def has_object_permission(self, request, view, obj):
        if view.action == 'retrieve':
            return obj.user_inscripted == request.user or request.user.user_type == 'ADMIN'
        elif view.action == 'destroy':
            return request.user.user_type == 'ADMIN' or (obj.user_inscripted == request.user and obj.round_inscription.is_blocked == False)
        else:
            return False
