from django.urls import path 

from rest_framework.routers import DefaultRouter

from .views import (
    RoundInscriptionViewSet,
    UserInscriptionViewSet,
)

router = DefaultRouter()
router.register(r'api/roundinscription/roundinscription', RoundInscriptionViewSet, basename="roundInscription")
router.register(r'api/roundinscription/userinscription', UserInscriptionViewSet, basename="userInscription")


urlpatterns = [

]

urlpatterns += router.urls
