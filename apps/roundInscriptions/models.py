from django.db import models

from apps.admissionsProcesses.models import Evaluation
from apps.users.models import User
from apps.persons.models import Person

from utils.abstractmodels import BaseModel

class RoundInscription(BaseModel):
    
    evaluation = models.OneToOneField(
        Evaluation,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        related_name='evaluation'
        )
    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )
    is_blocked = models.BooleanField(
        default=False,
        blank=False,
        null=False,
        )
    
    def __str__(self):
        return "Ronda de inscripcion para " + str(self.evaluation)
     
class UserInscription(BaseModel):
    

    round_inscription = models.ForeignKey(
        RoundInscription,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )
    
    user_inscripted = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )

class PersonInscription(BaseModel):
    

    round_inscription = models.ForeignKey(
        RoundInscription,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )
    
    person_inscripted = models.ForeignKey(
        Person,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        )
