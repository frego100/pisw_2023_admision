from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.abstractmodels import BaseModel

from apps.users.models import User

from .utils.lists import IncidentLevel

class Incident(BaseModel):
    
    name = models.CharField(
        max_length=50,
        blank=False,
        null=False,
        verbose_name = _("nombre de la incidencia"),
        )

    description = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        verbose_name = _("descripción de la incidencia"),
        )

    level = models.IntegerField(
        choices=IncidentLevel, 
        default=1
        )
    
    def __str__(self):
        return self.name + " -> "

class UserIncident(BaseModel):

    incident_teacher = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _("profesor"),
        )
    
    incident = models.ForeignKey(
        Incident,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        related_name = "teacher_incident",
        verbose_name = _("incidencia"),
        )
    
    incident_author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        related_name = "incident_author",
        verbose_name = _("usuario que creo la incidencia"),
        )
    
    def __str__(self):
        return str(self.incident_teacher) + " [" + str(self.incident) + "]"