from rest_framework import viewsets
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser
    )

from utils.permissions import (
    IsAdminOrReadOnly,
)

from django_filters.rest_framework import DjangoFilterBackend

from utils.paginations import CustomPagination

from apps.incidents.models import (
    Incident,
    UserIncident,
    )

from .serializers import (
    IncidentSerializer,
    UserIncidentSerializer,
    )
#excel
import pandas as pd
from django.http import HttpResponse
from io import BytesIO
from rest_framework.response import Response


class IncidentViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminUser,
        ]
    model = Incident
    serializer_class = IncidentSerializer
    queryset = Incident.objects.filter(
        is_active = True
        )
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['is_active']

class UserIncidentViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminOrReadOnly,
        ]
    model = UserIncident
    serializer_class = UserIncidentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['incident_teacher', 'incident','incident_author','is_active']
    queryset = UserIncident.objects.filter(
        is_active = True
        )
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        export_excel = request.query_params.get("export_excel",None)
        if export_excel:
            formatted_data = self.format_data(queryset)
            response = self.create_excel_response(formatted_data)
            return response
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
    def format_data(self, queryset):
        formatted_data = []
        for user_incident in queryset:
            formatted_item = {
                'Nombre del profesor': user_incident.incident_teacher.first_name,
                'Apellido del profesor': user_incident.incident_teacher.last_name,
                'Correo electrónico del profesor': user_incident.incident_teacher.email,
                'DNI del profesor': user_incident.incident_teacher.dni,
                'Nombre de la incidencia': user_incident.incident.name,
                'Descripción de la incidencia': user_incident.incident.description,
                'Nivel de la incidencia': user_incident.incident.level
            }
            formatted_data.append(formatted_item)
        return formatted_data
    
    def create_excel_response(self, formatted_data):
        df = pd.DataFrame(formatted_data)
        excel_buffer = BytesIO()
        writer = pd.ExcelWriter(excel_buffer, engine='xlsxwriter')
        df.to_excel(writer, index=False)
        writer.close()
        excel_data = excel_buffer.getvalue()
        response = HttpResponse(excel_data, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename="datos_incident_user.xlsx"'
        return response