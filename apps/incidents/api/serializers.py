from rest_framework import serializers

from apps.incidents.models import (
    Incident,
    UserIncident,
    )

from apps.users.api.serializers import (
    BasicUserSerializer,
    )

class IncidentSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        #   representation['level'] = LevelIncidentSerializer(instance.level).data
        return representation   
    
    class Meta:
        model = Incident
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

                              
class UserIncidentSerializer(serializers.ModelSerializer):

    incident_author = BasicUserSerializer(
        default = serializers.CurrentUserDefault(),
        required=False,
    )
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['incident_teacher'] = BasicUserSerializer(instance.incident_teacher).data
        representation['incident'] = IncidentSerializer(instance.incident).data
        return representation   

    class Meta:
        model = UserIncident
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }