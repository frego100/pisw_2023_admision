from django.db import models
from django.utils.translation import gettext_lazy as _

class IncidentLevel(models.IntegerChoices):
    LOW = 1, _("Bajo")
    NORMAL  = 2 ,_("Normal")
    HIGH  = 3, _("Alto")
    CRITICAL = 4, _("Crítico")