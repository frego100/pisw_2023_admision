from django.contrib import admin

from .models import (
    Incident,
    UserIncident,
    )

class IncidentAdminConfig(admin.ModelAdmin):
    model = Incident

    list_display = (
        'id',
        'name',
        'description',
        'is_active',
        'created_at',
        'updated_at'
        )
    ordering = ('-created_at',)

class UserIncidentAdminConfig(admin.ModelAdmin):
    model = UserIncident

    list_display = (
        'id',
        'incident_teacher',
        'incident',
        'incident_author',
        'is_active',
        'created_at',
        'updated_at'
        )
    ordering = ('-created_at',)


admin.site.register(Incident,IncidentAdminConfig)
admin.site.register(UserIncident,UserIncidentAdminConfig)