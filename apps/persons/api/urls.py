from django.urls import path 

from rest_framework.routers import DefaultRouter


from .views import (
    PersonViewSet,
    )

router = DefaultRouter()
router.register(r'api/person/person', PersonViewSet, basename="user")

urlpatterns = [
    ]

urlpatterns += router.urls
