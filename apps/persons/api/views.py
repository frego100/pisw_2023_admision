from django.shortcuts import render
from rest_framework import viewsets

from rest_framework.permissions import (
    IsAuthenticated, 
    IsAdminUser
    )

from apps.persons.models import Person

from .serializers import PersonSerializer

class PersonViewSet(viewsets.ModelViewSet):

    permission_classes = [
        IsAuthenticated, 
        IsAdminUser
        ]
    model = Person
    queryset = Person.objects.filter(
        is_active = True
        )
    serializer_class = PersonSerializer