from django.db import models
from django.utils.translation import gettext_lazy as _

from utils.abstractmodels import BaseModel

class PersonWork(BaseModel):
    
    name = models.CharField(
        max_length=50,
        blank=False,
        null=False,
        verbose_name = _("nombre del trabajo"),
        )

    description = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        verbose_name = _("descripción del trabajo"),
        )    
    

class Person(BaseModel):

    first_name = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        verbose_name = _("nombres"),
        )
    
    last_name = models.CharField(
        max_length=255,
        blank=True,
        null=False,
        verbose_name = _("apellidos"),
        )
    
    email = models.EmailField(
        max_length=255,
        unique=True,
        blank=True,
        null=False,
        verbose_name = _("correo"),
        )

    dni = models.CharField(
        max_length=10,
        blank=False,
        unique=True,
        null=False,
        verbose_name = _("dni"),
        )
    
    work = models.ForeignKey(
        PersonWork,
        on_delete=models.CASCADE,
        blank=False,
        null=False,
        verbose_name = _("trabajo"),
        )
