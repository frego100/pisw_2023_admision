from django.contrib import admin

from .models import (
    Person,
    PersonWork)

class PersonWorkAdminConfig(admin.ModelAdmin):
    model = PersonWork

    list_display = (
        'id',
        'name',
        'description',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class PersonAdminConfig(admin.ModelAdmin):
    model = Person

    list_display = (
        'id',
        'first_name',
        'last_name',
        'dni',
        'email',
        'work',
        'is_active',
        'created_at',
        'updated_at'
        )
    ordering = ('-created_at',)


admin.site.register(Person, PersonAdminConfig)
admin.site.register(PersonWork,PersonWorkAdminConfig)
