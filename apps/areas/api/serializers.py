from rest_framework import serializers
from ..models import Area, Pavilion, School, Classroom, Entrance

class AreaSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)
    class Meta:
        model = Area
        fields = '__all__'

class SchoolSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['area'] = AreaSerializer(instance.area).data
        return representation   
    
    class Meta:
        model = School
        fields = '__all__'

class PavilionSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['school'] = SchoolSerializer(instance.school).data
        return representation   
    
    class Meta:
        model = Pavilion
        fields = '__all__'

class ClassroomSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['pavilion'] = PavilionSerializer(instance.pavilion).data
        return representation   
    
    class Meta:
        model = Classroom
        fields = '__all__'

class EntranceSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['area'] = AreaSerializer(instance.area).data
        return representation   
    
    class Meta:
        model = Entrance
        fields = '__all__'