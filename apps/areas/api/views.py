from rest_framework import generics, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from ..models import Area, Pavilion, School, Classroom, Entrance
from .serializers import AreaSerializer, PavilionSerializer, SchoolSerializer, ClassroomSerializer,EntranceSerializer
from django_filters.rest_framework import DjangoFilterBackend

class AreaViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = Area.objects.filter(
        is_active=True
        )
    serializer_class = AreaSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'description','is_active']

class SchoolViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = School.objects.filter(
        is_active=True
        )
    serializer_class = SchoolSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'description','area','is_active']

class PavilionViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = Pavilion.objects.filter(
        is_active=True
        )
    serializer_class = PavilionSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'description','school','is_active']

class ClassroomViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = Classroom.objects.filter(
        is_active=True
        )
    serializer_class = ClassroomSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'description','rows','columns','pavilion','is_active']

class EntranceViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = Entrance.objects.filter(
        is_active=True
        )
    serializer_class = EntranceSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'description','area','is_active']