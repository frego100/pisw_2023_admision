from django.contrib import admin

from .models import Area, Pavilion, School, Classroom, Entrance

# Register your models here.

admin.site.register(Area)
admin.site.register(Pavilion)
admin.site.register(School)
admin.site.register(Classroom)
admin.site.register(Entrance)