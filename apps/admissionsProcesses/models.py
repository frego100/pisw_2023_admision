import uuid as uuid_lib

from django.db import models

from .utils.lists import (
    ExamType,
    EvaluationType,
    )

from utils.abstractmodels import BaseModel

class AdmissionProcess(BaseModel):

    year = models.IntegerField(
        "Año del Proceso",
        blank=False,
        null=False,
        )
    
    def __str__(self):
        return "Proceso " + str(self.year)
    
class Exam(BaseModel):

    type = models.CharField(
        'Tipo de examen',
        max_length=50, 
        choices=ExamType, 
        default='ORDINARIO'
        )
    number = models.IntegerField(
        "Numero de Examen",
        default = 1,
        blank=False,
        null=False,
        )
    admission_process = models.ForeignKey(
        AdmissionProcess, 
        on_delete=models.CASCADE,
        )
    
    def __str__(self):
        return str(self.admission_process) + " Examen " + str(self.type) + " " + str(self.number)
    
class Evaluation(BaseModel):


    number = models.IntegerField(
        "Numero de Evaluacion",
        blank=False,
        null=False,
        )
    
    type = models.CharField(
        max_length=50, 
        choices=EvaluationType, 
        default='KNOWLEDGE'
        )
    
    exam = models.ForeignKey(
        Exam, 
        on_delete=models.CASCADE,
        )
    
    scheduled_day = models.DateTimeField(
        blank=True,
        null=True
        )
    
    def __str__(self):
        return str(self.exam) + " Evaluación " + self.type + " " + str(self.number)
    
    def ordinal(self, numero):

        if 10 <= numero % 100 <= 20:
            sufijo = "MA"
        else:
            sufijo = {1: "RA", 2: "DA", 3: "RA"}.get(numero % 10, "TA")
        return str(numero) + sufijo
    
    def minimal_exam_name(self):
        return f"EXAMEN {self.exam.type} FASE {self.exam.number} {self.exam.admission_process.year}"
    
    def minimal_evaluation_name(self):
        return f"{self.ordinal(self.number)} EVALUACION DE {self.type}"
    
    def datetime_formated(self):
        # Diccionario de traducción de nombres de días y meses
        traduccion_espanol = {
            "Monday": "lunes",
            "Tuesday": "martes",
            "Wednesday": "miércoles",
            "Thursday": "jueves",
            "Friday": "viernes",
            "Saturday": "sábado",
            "Sunday": "domingo",
            "January": "enero",
            "February": "febrero",
            "March": "marzo",
            "April": "abril",
            "May": "mayo",
            "June": "junio",
            "July": "julio",
            "August": "agosto",
            "September": "septiembre",
            "October": "octubre",
            "November": "noviembre",
            "December": "diciembre"
        }

        date = self.scheduled_day
        if date:
            formated = date.strftime("%A %d de %B del %Y")
            # Traducción de nombres de días y meses
            for ingles, espanol in traduccion_espanol.items():
                formated = formated.replace(ingles, espanol)
            return formated.upper()
        return ""