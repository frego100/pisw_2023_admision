from django.apps import AppConfig


class admissionProcessConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.admissionsProcesses'
