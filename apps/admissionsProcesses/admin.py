from django.contrib import admin
from .models import AdmissionProcess,Evaluation,Exam

# Register your models here.

class AdmissionProcessAdminConfig(admin.ModelAdmin):
    model = AdmissionProcess

    list_display = (
        'id',
        'year',
        )

    ordering = ('-created_at',)


class ExamAdminConfig(admin.ModelAdmin):
    model = Exam

    list_display = (
        'id',
        'number',
        'type',
        'admission_process',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

class EvaluationAdminConfig(admin.ModelAdmin):
    model = Evaluation

    list_display = (
        'id',
        'number',
        'type',
        'exam',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)


admin.site.register(AdmissionProcess,AdmissionProcessAdminConfig)
admin.site.register(Exam,ExamAdminConfig)
admin.site.register(Evaluation,EvaluationAdminConfig)
