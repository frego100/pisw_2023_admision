from django.db import models
from django.utils.translation import gettext_lazy as _

class ExamType(models.TextChoices):
    CEPREUNSA = "CEPRE", _("Cepreunsa")
    CEPREUNSA_QUINTOS = "CEPRE QUINTOS" ,_("Cepreunsa ciclo quintos")
    ORDINARY = "ORDINARIO", _("Ordinario")
    EXTRAORDINARY = "EXTRAORDINARIO" ,_("Extraordinario")
    ORDINARY_BRANCHES  = "ORDINARIO FILIALES", _("Ordinario Filiales")

class EvaluationType(models.TextChoices):
    VOCATIONAL_PROFILE  = "PERFIL VOCACIONAL", _("Perfil Vocacional")
    KNOWLEDGE = "CONOCIMIENTOS", _("Conocimientos")
    ACADEMIC_APTITUDE = "APTITUD ACADEMICA", _("Aptitud Academica")