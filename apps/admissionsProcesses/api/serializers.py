from rest_framework import serializers

from ..models import (
    AdmissionProcess,
    Exam,
    Evaluation,
    )


class AdmissionProcessSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)
    class Meta:
        model = AdmissionProcess
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

class ExamSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['admission_process'] = AdmissionProcessSerializer(instance.admission_process).data
        return representation   
    
    class Meta:
        model = Exam
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

                              
class EvaluationSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)
    name = serializers.SerializerMethodField()

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['exam'] = ExamSerializer(instance.exam).data
        return representation   

    class Meta:
        model = Evaluation
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }

    def get_name(self, instance):
        return ' '.join(word.capitalize() for word in str(instance).split())