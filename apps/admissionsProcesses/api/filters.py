from django_filters.rest_framework import FilterSet
from django_filters import BooleanFilter
from apps.admissionsProcesses.models import Evaluation

class ProductFilter(FilterSet):
    has_round_inscription = BooleanFilter(method='filter_has_round_inscription')

    class Meta:
        model = Evaluation
        fields = ['exam', 'number', 'type', 'is_active']

    def filter_has_round_inscription(self, queryset, name, value):
        if value:
            return queryset.exclude(evaluation__isnull=True)
        else:
            return queryset.exclude(evaluation__isnull=False)