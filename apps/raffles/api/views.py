from django.shortcuts import render

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from apps.raffles.models import (
    Raffle,
    RaffleWinner,
    )

from .serializers import (
    RaffleSerializer,
    RaffleWinnerSerializer,
    )

from utils.permissions import (
    IsAdminOrReadOnly,
    CanNotUpdate,
    CanReadOnly
    )

class RaffleViewSet(viewsets.ModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminOrReadOnly,
        CanNotUpdate
        ]
    model = Raffle
    serializer_class = RaffleSerializer
    filter_backends = [
        DjangoFilterBackend
        ]
    queryset = Raffle.objects.filter(
        is_active = True
        )
    filterset_fields = [
        'raffler_user',
        'number_of_winners',
        'round_inscription',
        'is_active'
        ]

class RaffleWinnerViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [
        IsAuthenticated,
        IsAdminOrReadOnly,
        CanReadOnly
        ]
    model = RaffleWinner
    serializer_class = RaffleWinnerSerializer
    filter_backends = [
        DjangoFilterBackend
        ]
    filterset_fields = [
        'raffle',
        'user_winner',
        'is_active'
        ]
    
    queryset = RaffleWinner.objects.filter(
        is_active = True
        )