from random import sample

from rest_framework import serializers

from apps.raffles.models import (
    Raffle,
    RaffleWinner
    )
from apps.roundInscriptions.models import (
    UserInscription,
    )

from apps.users.api.serializers import (
    BasicUserSerializer,
    )

from apps.roundInscriptions.api.serializers import (
    RoundInscriptionShortListSerializer,
    )


from .utils import create_raffle_with_criteria


class RaffleSerializer(serializers.ModelSerializer):
    raffler_user = BasicUserSerializer(
        default = serializers.CurrentUserDefault(),
        required=False,
    )
    is_active = serializers.BooleanField(default=True)

    is_user_winner = serializers.SerializerMethodField()

    def get_is_user_winner(self, obj):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        if user:
            user_inscriotion = RaffleWinner.objects.filter(
                raffle = obj,
                user_winner = user
                ).first()
            
            if user_inscriotion:
                return True
        return False

    def validate_number_of_winners(self, value):
        round_inscription = self.initial_data.get('round_inscription')

        previous_winners = RaffleWinner.objects.filter(
            raffle__round_inscription=round_inscription,
            raffle__is_active=True
        ).values_list('user_winner', flat=True)

        eligible_users_count = UserInscription.objects.filter(
            round_inscription=round_inscription,
            is_active=True
        ).exclude(user_inscripted__in=previous_winners).count()


        if value == 0:
            raise serializers.ValidationError("You should draw at least one person.")
            
        if eligible_users_count == 0:
            raise serializers.ValidationError("There are not enough teachers registered for the raffle.")
        
        if value > eligible_users_count:
            raise serializers.ValidationError("The number of winners exceeds the number of eligible registered users.")
        
        return value
    


    def create(self, validated_data):
        return create_raffle_with_criteria(validated_data)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['round_inscription'] = RoundInscriptionShortListSerializer(instance.round_inscription).data
        return representation    

    class Meta:
        model = Raffle
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }
    
class RaffleWinnerSerializer(serializers.ModelSerializer):
    user_winner = BasicUserSerializer()
    raffle = RaffleSerializer()
    is_active = serializers.BooleanField(default=True)
    is_user_assigned = serializers.SerializerMethodField()
    assignment = serializers.SerializerMethodField()

    def get_assignment(self,obj):
        from apps.assignments.models import RaffleWinnerAssignment
        from apps.assignments.api.serializers import MinimalRaffleWinnerAssignmentSerializer
        
        assignment = RaffleWinnerAssignment.objects.filter(raffle_winner=obj,is_active=True).first()
        if assignment:
            return MinimalRaffleWinnerAssignmentSerializer(assignment).data
        else:
            return None

    def get_is_user_assigned(self, obj):
        from apps.assignments.models import RaffleWinnerAssignment

        return RaffleWinnerAssignment.objects.filter(raffle_winner=obj,is_active=True).exists()

    class Meta:
        model = RaffleWinner
        fields = '__all__'
        extra_kwargs = {
            'created_at': {'read_only': True},
            'updated_at': {'read_only': True},
        }