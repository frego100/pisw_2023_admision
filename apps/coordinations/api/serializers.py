from rest_framework import serializers
from ..models import Coordination

class CoordinationSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)
    class Meta:
        model = Coordination
        fields = '__all__'