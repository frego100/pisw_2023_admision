from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from ..models import Coordination
from .serializers import CoordinationSerializer

class CoordinationViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated, IsAdminUser]
    queryset = Coordination.objects.filter(
        is_active=True
        )
    serializer_class = CoordinationSerializer