from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CoordinationViewSet

router = DefaultRouter()
router.register(r'api/coordination', CoordinationViewSet, basename='coordination')

urlpatterns = [
    path('', include(router.urls)),
]