from django.contrib import admin

from .models import (
    Coordination,
)

class CoordinationAdminConfig(admin.ModelAdmin):
    model = Coordination

    list_display = (
        'id',
        'name',
        'description',
        'is_active',
        'created_at',
        'updated_at'
        )

    ordering = ('-created_at',)

admin.site.register(Coordination,CoordinationAdminConfig)