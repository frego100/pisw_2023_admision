from rest_framework.permissions import BasePermission, SAFE_METHODS

class IsAdminOrReadOnly(BasePermission):

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS or
            (request.user and request.user.is_staff)
        )
    

class CanNotUpdate(BasePermission):
    def has_permission(self, request, view):
        return bool (
            not request.method in ['PATCH', 'PUT']
        )
    
class CanReadOnly(BasePermission):

    def has_permission(self, request, view):
        return bool(
            request.method in SAFE_METHODS
        )    