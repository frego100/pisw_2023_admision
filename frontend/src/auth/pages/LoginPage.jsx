import icon from "../../assets/icono.png";
import iconName from "../../assets/unsa.png";
import iconNameColor from "../../assets/unsa-color.png";

import { useGoogleLogin } from "@react-oauth/google";
import { useNavigate } from "react-router-dom";
import { useAuthStore } from "../../hooks/useAuthStore";
import { toast } from 'react-toastify';
import { MessageConfig } from "../../modules/components/ToastMessage/MessageConfig";

export const LoginPage = () => {
  const { startLogin } = useAuthStore();

  const navigate = useNavigate();

  const loginGoogle = useGoogleLogin({
    onSuccess: (codeResponse) => {
      startLogin(codeResponse);
      navigate("/admin");
    },
    onError: (error) => toast.error('Error en el login.', MessageConfig),
  });

  return (
    <>
      <div className="w-100 conatiner bg-color-primary">
        <div className="row h-100 w-100 bg-color-primary d-flex align-items-center">
          <div className="col-auto d-flex">
            <img src={icon} className="icon-fluid-image" />
            <img src={iconName} className="iconName-fluid-image" />
          </div>
          <div className="col d-flex align-items-center p-0">
            <span className="text-white text-responsive">SISTEMA DE SELECCIÓN PARA EL PROCESO DE ADMISIÓN</span>
          </div>
        </div>
        <div
          className="bg-fondo-index d-flex justify-content-center align-items-center"
        >
          <div className="w-auto h-auto bg-color-gray opc-9 px-3 pt-4 pb-2 mx-5 rounded-5">
            <div className="my-1">
              <div className="d-flex justify-content-center">
                <img src={icon} className="me-3 icon2-fluid-image"></img>
                <img src={iconNameColor} className="iconName2-fluid-image"></img>
              </div>
            </div>
            <div className="d-flex justify-content-center">
              <button
                className="w-auto btn btn-secondary my-3"
                onClick={() => loginGoogle()}
              >
                Ingresa con su correo UNSA
              </button>
            </div>
            <span className="">
              Problemas con correos:{" "}
              <a href="www.google.com">soportecorreo@unsa.edu.pe</a>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};
