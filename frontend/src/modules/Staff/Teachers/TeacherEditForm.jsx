import { useState } from "react";
import Modal from "react-modal";


const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "620px",
  },
};

Modal.setAppElement("#root");

export const TeacherEditForm = ({ isOpen, onCloseModal }) => {
   return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <div className="d-flex justify-content-between">
        <h1 className="registrations-title">Nuevo Docente</h1>
      </div>
      
      <form className="p-3">
        <div className="mb-2">
          <label className="form-label">Nombres</label>
          <input type="text" className="form-control"></input>
        </div>

        <div className="mb-2">
          <label className="form-label">Apellidos</label>
          <input type="text" className="form-control"></input>
        </div>

        <div className="mb-2">
          <label className="form-label">DNI</label>
          <input type="text" className="form-control"></input>
        </div>

        <div className="mb-2">
          <label className="form-label">Correo</label>
          <input type="text" className="form-control"></input>
        </div>

        <div className="mb-2">
          <label className="form-label">Trabajo</label>
          <input type="text" className="form-control"></input>
        </div>

        <div className="col d-flex justify-content-center">
          <button type="button" className="btn btn-success w-25 me-5">Guardar</button>
          <button type="button" className="btn btn-danger w-25" onClick={onCloseModal}>Cancelar</button>
        </div>

      </form>
    </Modal>
  );
};
