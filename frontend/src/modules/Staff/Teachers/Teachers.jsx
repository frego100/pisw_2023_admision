import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
//import "./Teachers.css";
import { TeachersForms } from "./TeachersForms.jsx";
import { TeacherEditForm } from "./TeacherEditForm.jsx";
import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { GiTeacher } from "react-icons/gi";


import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Swal from 'sweetalert2'
import { usePersonStore } from "../../../hooks/usePersonStore.js";

export const Teachers = () => {
  const { startLoadingTeachers, teachers } = usePersonStore();
  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'first_name',
      headerName: 'Nombres',
      minWidth: 250,
      flex: 1,
    },
    {
      field: 'last_name',
      headerName: 'Apellidos',
      minWidth: 250,
      flex: 1,
    },
    {
      field: 'dni',
      headerName: 'DNI',
      minWidth: 80,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'email',
      headerName: 'Correo',
      minWidth: 150,
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'work',
      headerName: 'Trabajo',
      minWidth: 250,
      headerAlign: 'center',
      flex: 1,
    },
  ];
  const rows = teachers.map((teacher, index) => ({
    id: index + 1,
    first_name: teacher.first_name,
    last_name: teacher.last_name,
    dni: teacher.dni,
    email: teacher.email,
    work: teacher.work
  }))

  const [isOpenCreate, setIsOpenCreate] = useState(false);
  const onCloseCreateModal = () => {
    setIsOpenCreate(false);
  };
  const onOpenCreateModal = () => {
    setIsOpenCreate(true);
  };
  const [isOpenEdit, setIsOpenEdit] = useState(false);
  const onCloseEditModal = () => {
    setIsOpenEdit(false);
  };
  const onOpenEditModal = () => {
    setIsOpenEdit(true);
  };
  useEffect(() => {
    startLoadingTeachers();
  }, [])

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-center flex-wrap px-4">
          <h1 className="">Docentes <GiTeacher /></h1>
        </div>
      </div>

      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 10,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <TeachersForms
        isOpen={isOpenCreate}
        onCloseModal={onCloseCreateModal}
      />
      <TeacherEditForm
        isOpen={isOpenEdit}
        onCloseModal={onCloseEditModal}
      />
    </>
  );
};
