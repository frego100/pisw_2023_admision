import { useState } from "react";
import Modal from "react-modal";


const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "620px",
    width: "400px",
  },
};

Modal.setAppElement("#root");

export const TeachersForms = ({ isOpen, onCloseModal }) => {
  
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Nuevo Docente</h1>
      <hr />
      <form className="mx-3">
        <div className="form-group mb-2">
          <label className="w-100">Nombres<span className="text-danger"> *</span>
            <input type="text" className="form-control"></input>
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Apellidos<span className="text-danger"> *</span>
            <input type="text" className="form-control"></input>
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">DNI<span className="text-danger"> *</span>
            <input type="text" className="form-control"></input>
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Correo<span className="text-danger"> *</span>
            <input type="text" className="form-control"></input>
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Trabajo<span className="text-danger"> *</span>
            <input type="text" className="form-control"></input>
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="button" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={onCloseModal}>Cancelar</button>
        </div>

      </form>
    </Modal>
  );
};
