import { useState } from "react";
import Modal from "react-modal";
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";
import { useForm } from "react-hook-form";
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { filtersFetch } from "../../../helpers";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "630px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const ClassroomForm = ({ isOpen, onCloseModal }) => {
  const { startSavingClassroom, areas } = useSpaceStore();
  const { register, handleSubmit, formState: { errors }, reset } = useForm();
  const { getSchoolsByArea, getPavilionsBySchool } = filtersFetch();
  const [currentSchools, setCurrentSchools] = useState([]);
  const [currentPavilions, setCurrentPavilions] = useState([]);
  const onChangeArea = async (e) => {
    const filterSchools = await getSchoolsByArea(e.target.value);
    setCurrentSchools(filterSchools);
  }
  const onChangeSchool = async (e) => {
    const filterPavilions = await getPavilionsBySchool(e.target.value);
    setCurrentPavilions(filterPavilions);
  }
  const onSubmitClassroom = async (data) => {
    const newClassroom = {
      "name": data.name,
      "description": data.description,
      "pavilion": data.pavilion,
      "rows": Number(data.rows),
      "columns": Number(data.columns)
    }
    await startSavingClassroom(newClassroom);
    handleReset();
    onCloseModal();
  }
  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center ">Nueva aula</h1>
      <hr />
      <form className="mx-3" onSubmit={handleSubmit(onSubmitClassroom)}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre del aula<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="Digite el nombre del aula" {...register("name", { required: true })} />
          </label>
          {errors.name && <span className="text-danger">Debes ingresar nombre de aula</span>}
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Area a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" {...register("area", { required: true })} onChange={onChangeArea}>
              <option value="">Seleccione un Área</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
            {errors.area && <span className="text-danger">Debes seleccionar un área</span>}
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Escuela a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" {...register("school", { required: true })} onChange={onChangeSchool}>
              <option value="">Seleccione una Escuela</option>
              {
                currentSchools.map(school => (
                  <option key={school.id} value={school.id}>{school.name}</option>
                ))
              }
            </select>
            {errors.school && <span className="text-danger">Debes seleccionar una escuela</span>}
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Pabellón<span className="text-danger"> *</span>
            <select className="form-select" {...register("pavilion", { required: true })}>
              <option value="">Seleccione un pabellón</option>
              {
                currentPavilions.map(pavilion => (
                  <option key={pavilion.id} value={pavilion.id}>{pavilion.name}</option>
                ))
              }
            </select>
          </label>
          {errors.pavilion && <span className="text-danger">Debes seleccionar pabellón</span>}
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Descripción<span className="text-danger"> *</span>
            <textarea className="form-control" rows="3" placeholder="Digite descripción del aula" {...register("description", { required: true })} ></textarea>
          </label>
          {errors.description && <span className="text-danger">Debes ingresar descripción de Aula</span>}
        </div>
        <div className="row d-flex form-group mb-3">
          <label className="col">Filas<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="ej. 4" onKeyPress={onlyNumbers} {...register("rows", { required: true })} />
            {errors.rows && <span className="text-danger">Debes ingresar filas</span>}
          </label>
          <label className="col" >Columnas<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="ej. 4" onKeyPress={onlyNumbers} {...register("columns", { required: true })} />
            {errors.columns && <span className="text-danger">Debes ingresar filas</span>}
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
