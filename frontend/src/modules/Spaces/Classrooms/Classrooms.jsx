import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { ClassroomForm } from "./ClassroomForm";
import { ClassroomEditForm } from "./ClassroomEditForm";
import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { BiSolidBuildingHouse } from "react-icons/bi";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { CiCircleCheck } from "react-icons/ci";
import { CiCircleRemove } from "react-icons/ci";

import Swal from 'sweetalert2'
import { useSpaceStore } from "../../../hooks/useSpaceStore";


export const Classrooms = () => {
  const { classrooms, startLoadingClassrooms, startLoadingPavilions, startDeletingClassroom, setCurrentClassroom, startLoadingAreas, startLoadingSchools } = useSpaceStore();
  const [isOpenCreate, setIsOpenCreate] = useState(false);
  const onCloseCreateModal = () => {
    setIsOpenCreate(false);
  };
  const onOpenCreateModal = () => {
    setIsOpenCreate(true);
  };
  const [isOpenEdit, setIsOpenEdit] = useState(false);
  const onCloseEditModal = () => {
    setIsOpenEdit(false);
  };
  const onOpenEditModal = (classroom) => {
    setCurrentClassroom(classroom);
    setIsOpenEdit(true);
  };

  const onDeleteClassroom = async (classroom) => {
    Swal.fire({
      title: `<strong>Eliminar aula</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta aula.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingClassroom(classroom);
      }
    });
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Nombre',
      minWidth: 200,
      type: 'number',
      align: 'center',
      headerAlign: 'center',
      //editable: true,
      flex: 1,
    },
    {
      field: 'pavilion',
      headerName: 'Pabellón',
      minWidth: 200,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'description',
      headerName: 'Descripción',
      minWidth: 400,
    },
    {
      field: 'rows',
      headerName: 'Filas',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'columns',
      headerName: 'Columnas',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'actions',
      headerName: 'Acciones',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex flex-row justify-content-center">
            <button className="btn btn-warning text-white d-flex justify-content-center me-2" onClick={() => onOpenEditModal(params.row.classroom)}>
              <FaPencilAlt />
            </button>
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteClassroom(params.row.classroom)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ];

  const rows = classrooms.map((classroom, index) => ({
    id: index + 1,
    name: classroom.name,
    description: classroom.description,
    pavilion: classroom.pavilion.name,
    columns: classroom.columns,
    rows: classroom.rows,
    classroom: classroom
  }))

  useEffect(() => {
    startLoadingClassrooms();
    startLoadingAreas();
    startLoadingSchools();
    startLoadingPavilions();
  }, []);

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap px-4">
          <h1 className="">Aulas &nbsp; <BiSolidBuildingHouse/></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenCreateModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>

      <ClassroomForm
        isOpen={isOpenCreate}
        onCloseModal={onCloseCreateModal}
      />
      <ClassroomEditForm
        isOpen={isOpenEdit}
        onCloseModal={onCloseEditModal}
      />
    </>
  );
};
