import { useEffect, useState } from "react";
import Modal from "react-modal";
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";
import { useFormF } from "../../../hooks/useFormF";
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { filtersFetch } from "../../../helpers";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "630px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const ClassroomEditForm = ({ isOpen, onCloseModal }) => {
  const { currentClassroom, startUpdatingClassroom, pavilions, schools, areas } = useSpaceStore();
  const [values, handleInputChange, setValues] = useFormF({ name: currentClassroom.name, description: currentClassroom.description, rows: currentClassroom.rows, columns: currentClassroom.columns, pavilion: currentClassroom.pavilion.id });
  const { getPavilionsBySchool, getSchoolsByArea } = filtersFetch();
  const [changedArea, setChangedArea] = useState(false);
  const [changedSchool, setChangedSchool] = useState(false);
  const [currentSchools, setCurrentsSchools] = useState([]);
  const [currentPavilions, setCurrentsPavilions] = useState([]);

  const onChangeArea = async (e) => {
    setChangedArea(true);
    const filterSchools = await getSchoolsByArea(e.target.value);
    setCurrentsSchools(filterSchools);
  }
  const onChangeSchool = async (e) => {
    setChangedSchool(true);
    const filterPavilions = await getPavilionsBySchool(e.target.value);
    setCurrentsPavilions(filterPavilions);
  }

  const onSubmitClassroom = async (e) => {
    e.preventDefault();
    const updateClassroom = {
      "id": currentClassroom.id,
      "name": values.name,
      "pavilion": values.pavilion,
      "description": values.description,
      "rows": values.rows,
      "columns": values.columns
    }
    await startUpdatingClassroom(updateClassroom);
    onCloseModal();
    setChangedArea(false);
    setChangedSchool(false);
  }
  useEffect(() => {
    setValues({ name: currentClassroom.name, description: currentClassroom.description, rows: currentClassroom.rows, columns: currentClassroom.columns, pavilion: currentClassroom.pavilion.id });
  }, [currentClassroom])
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Editar aula</h1>
      <hr />
      <form className="mx-3" onSubmit={onSubmitClassroom}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre del aula
            <input type="text" className="form-control" placeholder="Digite el nombre del aula" defaultValue={currentClassroom.name} name="name" onChange={handleInputChange} />
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Area a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" defaultValue={currentClassroom.pavilion.school.area.id} onChange={onChangeArea}>
              <option value="">Seleccione un Área</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
          </label>
        </div>
        {
          changedArea ?
            <div className="form-group mb-2">
              <label className="w-100">Escuela a la que pertenece<span className="text-danger"> *</span>
                <select className="form-select" defaultValue={currentClassroom.pavilion.school.id} onChange={onChangeSchool}>
                  <option value="">Seleccione una Escuela</option>
                  {
                    currentSchools.map(school => (
                      <option key={school.id} value={school.id}>{school.name}</option>
                    ))
                  }
                </select>
              </label>
            </div>
            :
            <div className="form-group mb-2">
              <label className="w-100">Escuela a la que pertenece<span className="text-danger"> *</span>
                <select className="form-select" defaultValue={currentClassroom.pavilion.school.id} onChange={onChangeSchool}>
                  <option value="">Seleccione una Escuela</option>
                  {
                    schools.map(school => (
                      <option key={school.id} value={school.id}>{school.name}</option>
                    ))
                  }
                </select>
              </label>
            </div>
        }
        {
          changedSchool ?
            <div className="form-group mb-2">
              <label className="w-100">Pabellón a la que pertenece
                <select className="form-select" name="pavilion" onChange={handleInputChange} defaultValue={currentClassroom.pavilion.id}>
                  {
                    currentPavilions.map(pavilion => (
                      <option key={pavilion.id} value={pavilion.id}>{pavilion.name}</option>
                    ))
                  }
                </select>
              </label>
            </div>
            :
            <div className="form-group mb-2">
              <label className="w-100">Pabellón a la que pertenece
                <select className="form-select" name="pavilion" onChange={handleInputChange} defaultValue={currentClassroom.pavilion.id}>
                  {
                    pavilions.map(pavilion => (
                      <option key={pavilion.id} value={pavilion.id}>{pavilion.name}</option>
                    ))
                  }
                </select>
              </label>
            </div>
        }
        <div className="form-group mb-2">
          <label className="w-100">Descripción
            <textarea className="form-control" placeholder="Ingrese la descripción" rows="3" defaultValue={currentClassroom.description} name="description" onChange={handleInputChange}></textarea>
          </label>
        </div>
        <div className="row d-flex form-group mb-3">
          <label className="col">Filas
            <input type="text" className="form-control" placeholder="Digite el número de filas" onKeyPress={onlyNumbers} defaultValue={currentClassroom.rows} name="rows" onChange={handleInputChange} />
          </label>
          <label className="col">Columnas
            <input type="text" className="form-control" placeholder="Digite el número de columnas" onKeyPress={onlyNumbers} defaultValue={currentClassroom.columns} name="columns" onChange={handleInputChange} />
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={onCloseModal}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
