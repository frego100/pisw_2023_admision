import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { PavillonForm } from "./PavillonForm";
import { PavillonEditForm } from "./PavillonEditForm";
import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { FaBuilding } from "react-icons/fa";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Swal from 'sweetalert2'
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { filtersFetch } from "../../../helpers";


export const Pavillons = () => {
  const { pavilions, startLoadingPavilions, startDeletingPavilion, setCurrentPavilion, startLoadingSchools, startLoadingAreas, currentPavilion } = useSpaceStore();

  const [isOpenCreate, setIsOpenCreate] = useState(false);
  const onCloseCreateModal = () => {
    setIsOpenCreate(false);
  };
  const onOpenCreateModal = () => {
    setIsOpenCreate(true);
  };
  const [isOpenEdit, setIsOpenEdit] = useState(false);
  const onCloseEditModal = () => {
    setIsOpenEdit(false);
  };
  const onOpenEditModal = (pavilion) => {
    setCurrentPavilion(pavilion);
    setIsOpenEdit(true);
  };

  const onDeletePavillon = async (pavilion) => {
    Swal.fire({
      title: `<strong>Eliminar pabellón</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta pabellón.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingPavilion(pavilion);
      }
    });
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Nombre',
      width: 120,
      type: 'number',
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'school',
      headerName: 'Escuela',
      width: 110,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'description',
      headerName: 'Descripción',
      width: 200,
      flex: 1,
    },
    {
      field: 'actions',
      headerName: 'Acciones',
      width: 110,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <button className="btn btn-warning text-white d-flex justify-content-center me-2" onClick={() => onOpenEditModal(params.row.pavilion)}>
              <FaPencilAlt />
            </button>
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeletePavillon(params.row.pavilion)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ];

  const rows = pavilions.map((pavilion, index) => ({
    id: index + 1,
    name: pavilion.name,
    description: pavilion.description,
    school: pavilion.school.name,
    pavilion: pavilion
  }));

  useEffect(() => {
    startLoadingPavilions();
    startLoadingSchools();
    startLoadingAreas();
  }, []);

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap px-4">
          <h1 className="">Pabellones &nbsp; <FaBuilding/></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenCreateModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <PavillonForm
        isOpen={isOpenCreate}
        onCloseModal={onCloseCreateModal}
      />
      <PavillonEditForm
        isOpen={isOpenEdit}
        onCloseModal={onCloseEditModal}
      />
    </>
  );
};
