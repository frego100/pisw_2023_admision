import { useState } from "react";
import Modal from "react-modal";
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";

import "./Pavillons.css";
import { useForm } from "react-hook-form";
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { filtersFetch } from "../../../helpers/filtersFetch";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "650px",
    width: "450px"
  },
};

Modal.setAppElement("#root");

export const PavillonForm = ({ isOpen, onCloseModal }) => {
  const { startSavingPavilion, areas } = useSpaceStore();
  const { register, handleSubmit, formState: { errors }, reset } = useForm();
  const { getSchoolsByArea } = filtersFetch();
  const [currentSchools, setCurrentSchools] = useState([]);
  const onChangeArea = async (e) => {
    const filterSchools = await getSchoolsByArea(e.target.value);
    setCurrentSchools(filterSchools);
  }

  const onSubmitPavilion = async (data) => {
    const newPavilion = {
      "name": data.name,
      "description": data.description,
      "school": data.school
    }
    await startSavingPavilion(newPavilion);
    handleReset();
    onCloseModal();
  }
  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Nuevo Pabellón</h1>
      <hr />

      <form className="mx-3" onSubmit={handleSubmit(onSubmitPavilion)}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre del pabellón<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="Ingrese el nombre del pabellón" {...register("name", { required: true })} />
          </label>
          {errors.name && <span className="text-danger">Debes ingresar nombre de pabellón</span>}
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Area a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" {...register("area", { required: true })} onChange={onChangeArea}>
              <option value="">Seleccione un Área</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
            {errors.area && <span className="text-danger">Debes seleccionar un área</span>}
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Escuela a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" {...register("school", { required: true })}>
              <option value="">Seleccione una Escuela</option>
              {
                currentSchools.map(school => (
                  <option key={school.id} value={school.id}>{school.name}</option>
                ))
              }
            </select>
            {errors.school && <span className="text-danger">Debes seleccionar una escuela</span>}
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Descripción<span className="text-danger"> *</span>
            <textarea className="form-control" rows="2" placeholder="Ingrese descripción del pabellón" {...register("description", { required: true })}></textarea>
          </label>
          {errors.description && <span className="text-danger">Debes ingresar descripción de pabellón</span>}
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
        </div>

      </form>
    </Modal>
  );
};
