import { useEffect, useState } from "react";
import Modal from "react-modal";
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";

import "./Pavillons.css";
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { useFormF } from "../../../hooks/useFormF";
import { filtersFetch } from "../../../helpers";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "650px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const PavillonEditForm = ({ isOpen, onCloseModal }) => {
  const { currentPavilion, startUpdatingPavilion, schools, areas } = useSpaceStore();
  const [values, handleInputChange, setValues] = useFormF({ name: currentPavilion.name, description: currentPavilion.description, school: currentPavilion.school.id });
  const { getSchoolsByArea } = filtersFetch();
  const [currentSchools, setCurrentSchools] = useState([]);
  const [changedArea, setChangedArea] = useState(false);
  const onChangeArea = async (e) => {
    setChangedArea(true);
    const filterSchools = await getSchoolsByArea(e.target.value);
    setCurrentSchools(filterSchools);
  }

  const onSubmitPavilion = async (e) => {
    e.preventDefault();
    const updatePavilion = {
      "id": currentPavilion.id,
      "name": values.name,
      "school": values.school,
      "description": values.description
    }
    await startUpdatingPavilion(updatePavilion);
    setChangedArea(false);
    onCloseModal();
  }
  useEffect(() => {
    setValues({ name: currentPavilion.name, description: currentPavilion.description, school: currentPavilion.school.id });
  }, [currentPavilion])

  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Editar Pabellón</h1>
      <hr />

      <form className="mx-3" onSubmit={onSubmitPavilion}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre del pabellón
            <input type="text" className="form-control" placeholder="" defaultValue={currentPavilion.name} name="name" onChange={handleInputChange} />
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Area a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" defaultValue={currentPavilion.school.area.id} onChange={onChangeArea}>
              <option value="">Seleccione un Área</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
          </label>
        </div>
        {
          changedArea ?
            <div className="form-group mb-2">
              <label className="w-100">Escuela a la que pertenece
                <select className="form-select" name="school" onChange={handleInputChange} defaultValue={currentPavilion.school.id} >
                  {
                    currentSchools.map(school => (
                      <option key={school.id} value={school.id}>{school.name}</option>
                    ))
                  }
                </select>
              </label>
            </div>
            :
            <div className="form-group mb-2">
              <label className="w-100">Escuela a la que pertenece
                <select className="form-select" name="school" onChange={handleInputChange} defaultValue={currentPavilion.school.id} >
                  {
                    schools.map(school => (
                      <option key={school.id} value={school.id}>{school.name}</option>
                    ))
                  }
                </select>
              </label>
            </div>
        }
        <div className="form-group mb-3">
          <label className="w-100">Descripción
            <textarea className="form-control" rows="2" defaultValue={currentPavilion.description} name="description" onChange={handleInputChange}></textarea>
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={onCloseModal}>Cancelar</button>
        </div>

      </form>
    </Modal >
  );
};
