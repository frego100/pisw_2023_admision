import { useEffect, useState } from "react";
import Modal from "react-modal";
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";
import { useForm } from "react-hook-form";
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { useFormF } from "../../../hooks/useFormF";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "630px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const EntranceEditForm = ({ isOpen, onCloseModal }) => {
  const { startUpdatingEntrance, areas, currentEntrance } = useSpaceStore();
  const [values, handleInputChange, setValues] = useFormF({ name: currentEntrance.name, description: currentEntrance.description, area: currentEntrance.area.id });

  const onSubmitEntrance = async (e) => {
    e.preventDefault();
    const updateEntrance = {
      "id": currentEntrance.id,
      "name": values.name,
      "area": values.area,
      "description": values.description,
    }
    await startUpdatingEntrance(updateEntrance);
    onCloseModal();
  }
  useEffect(() => {
    setValues({ name: currentEntrance.name, description: currentEntrance.description, area: currentEntrance.area.id });
  }, [currentEntrance])
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Editar Entrada</h1>
      <hr />
      <form className="p-3" onSubmit={onSubmitEntrance}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre de la entrada
            <input type="text" className="form-control" placeholder="Escriba el nombre del aula" name="name" onChange={handleInputChange} defaultValue={currentEntrance.name} />
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Area
            <select className="form-select" name="area" onChange={handleInputChange} defaultValue={currentEntrance.area.id}>
              <option value="">Seleccione un area</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Descripción
            <textarea className="form-control" rows="3" placeholder="Ingrese descripción del aula" name="description" onChange={handleInputChange} defaultValue={currentEntrance.description} ></textarea>
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={onCloseModal}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
