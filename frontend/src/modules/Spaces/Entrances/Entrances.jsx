import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { EntranceForm } from "./EntranceForm";
import { EntranceEditForm } from "./EntranceEditForm";
import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { CiCircleCheck } from "react-icons/ci";
import { CiCircleRemove } from "react-icons/ci";
import { FaDoorOpen } from "react-icons/fa";



import Swal from 'sweetalert2'
import { useSpaceStore } from "../../../hooks/useSpaceStore";

export const Entrances = () => {
  const { entrances, startLoadingAreas, startLoadingEntrances, setCurrentEntrance, startDeletingEntrance } = useSpaceStore();
  const [isOpenCreate, setIsOpenCreate] = useState(false);
  const onCloseCreateModal = () => {
    setIsOpenCreate(false);
  };
  const onOpenCreateModal = () => {
    setIsOpenCreate(true);
  };
  const [isOpenEdit, setIsOpenEdit] = useState(false);
  const onCloseEditModal = () => {
    setIsOpenEdit(false);
  };
  const onOpenEditModal = (entrance) => {
    setCurrentEntrance(entrance);
    setIsOpenEdit(true);
  };

  const onDeleteEntrance = async (entrance) => {
    Swal.fire({
      title: `<strong>Eliminar entrada</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta entrada.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingEntrance(entrance);
      }
    });
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Nombre',
      minWidth: 200,
      type: 'number',
      align: 'center',
      headerAlign: 'center',
      //editable: true,
      flex: 1,
    },
    {
      field: 'area',
      headerName: 'Área',
      minWidth: 200,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'description',
      headerName: 'Descripción',
      minWidth: 200,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'actions',
      headerName: 'Acciones',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex flex-row justify-content-center">
            <button className="btn btn-warning text-white d-flex justify-content-center me-2" onClick={() => onOpenEditModal(params.row.entrance)}>
              <FaPencilAlt />
            </button>
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteEntrance(params.row.entrance)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ];

  const rows = entrances.map((entrance, index) => ({
    id: index + 1,
    name: entrance.name,
    area: entrance.area.name,
    description: entrance.description,
    entrance: entrance
  }))


  useEffect(() => {
    startLoadingAreas();
    startLoadingEntrances();
  }, []);

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap px-4">
          <h1 className="">Entradas <FaDoorOpen /></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenCreateModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>

      <EntranceForm
        isOpen={isOpenCreate}
        onCloseModal={onCloseCreateModal}
      />
      <EntranceEditForm
        isOpen={isOpenEdit}
        onCloseModal={onCloseEditModal}
      />
    </>
  );
};
