import { useEffect, useState } from "react";
import Modal from "react-modal";
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";
import { useForm } from "react-hook-form";
import { useSpaceStore } from "../../../hooks/useSpaceStore";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "630px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const EntranceForm = ({ isOpen, onCloseModal }) => {
  const { areas, startSavingEntrance } = useSpaceStore();
  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  const onSubmitEntrance = async (data) => {
    const newEntrance = {
      "name": data.name,
      "area": data.area,
      "description": data.description,
    }
    await startSavingEntrance(newEntrance);
    handleReset();
    onCloseModal();
  }
  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Nueva Entrada</h1>
      <hr />
      <form className="p-3" onSubmit={handleSubmit(onSubmitEntrance)}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre de la entrada<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="Escriba el nombre del aula" {...register("name", { required: true })} />
            {errors.name && <span className="text-danger">Debes ingresar nombre de la entrada</span>}
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Area<span className="text-danger"> *</span>
            <select className="form-select" {...register("area", { required: true })}>
              <option value="">Seleccione un area</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
            {errors.area && <span className="text-danger">Debes seleccionar un area</span>}
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Descripción<span className="text-danger"> *</span>
            <textarea className="form-control" rows="3" placeholder="Ingrese descripción del aula" {...register("description", { required: true })} ></textarea>
            {errors.description && <span className="text-danger">Debes ingresar descripción de la entrada</span>}
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
