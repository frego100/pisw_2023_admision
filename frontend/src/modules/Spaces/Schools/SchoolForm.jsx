import Modal from "react-modal";
import { useForm } from "react-hook-form"
import Select from 'react-select'
import { useState } from 'react';
import { useSpaceStore } from "../../../hooks/useSpaceStore";
import { toast } from 'react-toastify';
import { MessageConfig } from "../../components/ToastMessage/MessageConfig";
//import "./Schools.css";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "650px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const SchoolForm = ({ isOpen, onCloseModal }) => {
  const { areas, startSavingSchool } = useSpaceStore();
  const { register, handleSubmit, formState: { errors }, reset } = useForm();

  const onSubmitSchool = async (data) => {
    const newSchool = {
      "name": data.name,
      "area": data.area,
      "description": data.description
    }
    await startSavingSchool(newSchool);
    handleReset();
    onCloseModal();
  }
  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Nueva Escuela</h1>
      <hr />
      <form className="mx-3" onSubmit={handleSubmit(onSubmitSchool)}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre de la escuela<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="Ingrese nombre de escuela" {...register("name", { required: true })} />
            {errors.name && <span className="text-danger">Debes ingresar un nombre de escuela</span>}
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Área a la que pertenece<span className="text-danger"> *</span>
            <select className="form-select" {...register("area", { required: true })}>
              <option value="">Seleccione un Área</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
            {errors.area && <span className="text-danger">Debes seleccionar un área</span>}
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Descripción<span className="text-danger"> *</span>
            <textarea className="form-control" rows="2" placeholder="Ingrese descripción de escuela" {...register("description", { required: true })}></textarea>
            {errors.description && <span className="text-danger">Debes ingresar descripción de escuela</span>}
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={() => {onCloseModal(); handleReset();}}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
