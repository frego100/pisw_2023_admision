import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { SchoolForm } from "./SchoolForm";
import { SchoolEditForm } from "./SchoolEditForm";
import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { FaCity } from "react-icons/fa6";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { useSpaceStore } from "../../../hooks/useSpaceStore";

import Swal from 'sweetalert2'

export const Schools = () => {
  const { schools, startLoadingSchools, startLoadingAreas, startDeletingSchool, setCurrentSchool } = useSpaceStore();
  const [isOpenCreate, setIsOpenCreate] = useState(false);
  const onCloseCreateModal = () => {
    setIsOpenCreate(false);
  };
  const onOpenCreateModal = () => {
    setIsOpenCreate(true);
  };
  const [isOpenEdit, setIsOpenEdit] = useState(false);
  const onCloseEditModal = () => {
    setIsOpenEdit(false);
  };
  const onOpenEditModal = (school) => {
    setCurrentSchool(school);
    setIsOpenEdit(true);
  };

  const onDeleteSchool = async (school) => {
    Swal.fire({
      title: `<strong>Eliminar escuela</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta escuela.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingSchool(school);
      }
    });
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      width: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Nombre',
      width: 90,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'area',
      headerName: 'Área',
      width: 200,
      flex: 1,
    },
    {
      field: 'description',
      headerName: 'Descripción',
      width: 200,
      flex: 1,
    },
    {
      field: 'actions',
      headerName: 'Acciones',
      width: 110,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <button className="btn btn-warning text-white d-flex justify-content-center me-2" onClick={() => onOpenEditModal(params.row.school)}>
              <FaPencilAlt />
            </button>
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteSchool(params.row.school)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ];

  const rows = schools.map((school, index) => ({
    id: index + 1,
    name: school.name,
    area: school.area.name,
    description: school.description,
    school: school
  }));

  useEffect(() => {
    startLoadingSchools();
    startLoadingAreas();
  }, []);

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap px-4">
          <h1 className="">Escuelas &nbsp; <FaCity/></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenCreateModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <SchoolForm
        isOpen={isOpenCreate}
        onCloseModal={onCloseCreateModal}
      />
      <SchoolEditForm
        isOpen={isOpenEdit}
        onCloseModal={onCloseEditModal}
      />
    </>
  );
};
