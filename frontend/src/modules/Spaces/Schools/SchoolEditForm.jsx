import Modal from "react-modal";
import { useForm } from "react-hook-form"
import { onlyNumbers } from "../../components/Functions/OnlyNumbersInput";
import Select from 'react-select'
import React, { useEffect, useState } from 'react';
import { useFormF } from "../../../hooks/useFormF";
import { useSpaceStore } from "../../../hooks/useSpaceStore";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "650px",
    width: "400px"
  },
};

Modal.setAppElement("#root");

export const SchoolEditForm = ({ isOpen, onCloseModal }) => {
  const { currentSchool, startUpdatingSchool, areas } = useSpaceStore();
  const [values, handleInputChange, setValues] = useFormF({ name: currentSchool.name, area: currentSchool.area.id, description: currentSchool.description });

  const onSubmitSchool = async (e) => {
    e.preventDefault();
    const updateSchool = {
      "id": currentSchool.id,
      "name": values.name,
      "area": values.area,
      "description": values.description
    }
    await startUpdatingSchool(updateSchool);
    onCloseModal();
  }
  useEffect(() => {
    setValues({ name: currentSchool.name, area: currentSchool.area.id, description: currentSchool.description });
  }, [currentSchool])

  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Editar Escuela</h1>
      <hr />
      <form className="p-3" onSubmit={onSubmitSchool}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre de la escuela
            <input type="text" className="form-control" placeholder="Ingrese nombre" defaultValue={currentSchool.name} name="name" onChange={handleInputChange} />
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Área a la que pertenece
            <select className="form-select" name="area" onChange={handleInputChange} defaultValue={currentSchool.area.id}>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Descripción
            <textarea className="form-control" rows="2" defaultValue={currentSchool.description} name="description" onChange={handleInputChange}></textarea>
          </label>
        </div>
        <div className="col d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={onCloseModal}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
