import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { IncidentForm } from "./IncidentForm";
import { useIncidentStore } from "../../hooks/useIncidentStore";

import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { TbFaceIdError } from "react-icons/tb";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { IncidentUpdateForm } from "./IncidentUpdateForm";
import Swal from 'sweetalert2'

export const Incidents = () => {
  const { incidents, startLoadingIncidents, startDeletingIncident, setCurrentIncident } = useIncidentStore();

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'incidentName',
      headerName: 'Nombre',
      minWidth: 200,
      flex: 1,
    },
    {
      field: 'incidentDescription',
      headerName: 'Descripción',
      minWidth: 400,
    },
    {
      field: 'incidentLevel',
      headerName: 'Nivel de incidencia',
      type: 'number',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'status',
      headerName: 'Estado',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'incident',
      headerName: 'Acciones',
      minWidth: 150,
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <button className="btn btn-warning text-white d-flex justify-content-center me-2" onClick={() => onOpenModalUpdate(params.row.incident)}>
              <FaPencilAlt />
            </button>
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteIncident(params.row.incident)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ];

  const rows = incidents.map((incident, index) => ({
    id: index + 1,
    incidentName: incident.name,
    incidentDescription: incident.description,
    incidentLevel: incident.level,
    status: incident.is_active ? 'Activo' : 'Inactivo',
    incident: incident,
  }));

  const [isOpen, setIsOpen] = useState(false);
  const [isOpenUpdate, setIsOpenUpdate] = useState(false);
  const onCloseModal = () => {
    setIsOpen(false);
  };
  const onOpenModal = () => {
    setIsOpen(true);
  };
  const onCloseModalUpdate = () => {
    setIsOpenUpdate(false);
  };
  const onOpenModalUpdate = (incident) => {
    setCurrentIncident(incident);
    setIsOpenUpdate(true);
  };
  const onCreateIncident = () => {
    onOpenModal();
  }
  const onDeleteIncident = async (incident) => {
    Swal.fire({
      title: `<strong>Eliminar incidencia</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta incidencia.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingIncident(incident);
      }
    });
  }

  useEffect(() => {
    startLoadingIncidents();
  }, [])
  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap">
          <h1 className="">Incidencias &nbsp; <TbFaceIdError/></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={() => onCreateIncident()}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="h-auto bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <IncidentForm
        isOpen={isOpen}
        onCloseModal={onCloseModal}
      />
      <IncidentUpdateForm
        isOpen={isOpenUpdate}
        onCloseModal={onCloseModalUpdate}
      />
    </>
  );
};