import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { IncidentTeacherForm } from "./IncidentTeacherForm";
import { usePersonStore } from "../../hooks/usePersonStore";
import { useIncidentStore } from "../../hooks/useIncidentStore";

import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { BiSolidReport } from "react-icons/bi";
import { RiFileExcel2Fill } from "react-icons/ri";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Swal from 'sweetalert2'

export const IncidentsTeacher = () => {
  const { startLoadingTeachers } = usePersonStore();
  const { startLoadingIncidents, startLoadingTeacherIncidents, startExportTeacherIncidents, startDeletingTeacherIncident, teacherIncidents } = useIncidentStore();

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'created_at',
      headerName: 'Fecha',
      minWidth: 120,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'first_name',
      headerName: 'Docente',
      minWidth: 120,
    },
    {
      field: 'description',
      headerName: 'Descripción',
      minWidth: 250,
      align: 'center',
      headerAlign: 'center',
      flex: 1,
    },
    {
      field: 'incident_level',
      headerName: 'Nivel',
      type: 'number',
      minWidth: 80,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'teacherIncident',
      headerName: 'Acciones',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteTeacherIncident(params.row.teacherIncident)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ]

  const rows = teacherIncidents.map((teacherIncident, index) => ({
    id: index + 1,
    created_at: teacherIncident.created_at.split("T00:42:54.993185Z"),
    first_name: teacherIncident.incident_teacher.first_name,
    description: teacherIncident.incident.description,
    incident_level: teacherIncident.incident.level,
    teacherIncident: teacherIncident,
  }));

  const [isOpen, setIsOpen] = useState(false);
  const onCloseModal = () => {
    setIsOpen(false);
  };
  const onOpenModal = () => {
    setIsOpen(true);
    startLoadingTeachers();
    startLoadingIncidents();
  };
  const onDeleteTeacherIncident = async (teacherIncident) => {
    Swal.fire({
      title: `<strong>Eliminar incidencia docente</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar la incidencia de este docente.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingTeacherIncident(teacherIncident);
      }
    });
  }


  useEffect(() => {
    startLoadingTeacherIncidents();
  }, [])
  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap">
          <div className="d-flex align-items-center">
            <h1 className="">Incidencias de Docentes</h1>
          </div>
          <div className="d-flex align-items-center">
            <button className="btn btn-icon btn-outline-success me-3 fs-5 px-2 py-1" onClick={startExportTeacherIncidents}><RiFileExcel2Fill></RiFileExcel2Fill></button>
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <IncidentTeacherForm
        isOpen={isOpen}
        onCloseModal={onCloseModal}
      />
    </>
  );
};