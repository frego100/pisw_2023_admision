import React, { useEffect, useState } from "react";
import Modal from "react-modal";

import "./Incidents.css";
import { usePersonStore } from "../../hooks/usePersonStore";
import { useIncidentStore } from "../../hooks/useIncidentStore";
import { useForm } from "react-hook-form";
import Select from 'react-select'
import { toast } from 'react-toastify';
import { MessageConfig } from "../components/ToastMessage/MessageConfig";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "500px",
    maxHeight: "630px",
    width: "400px",
  },
};

Modal.setAppElement("#root");

export const IncidentTeacherForm = ({ isOpen, onCloseModal }) => {
  const { register, handleSubmit, reset } = useForm();
  const { teachers } = usePersonStore();
  const { incidents, startSavingTeacherIncident } = useIncidentStore();

  const optionsDocent = teachers.map((teacher) => ({
    value: teacher.id, label: teacher.first_name + " " + teacher.last_name
  }));

  const optionsIncident = incidents.map((incident) => ({
    value: incident.id, label: incident.name
  }));

  const [selectedOptionDocent, setSelectedOptionDocent] = useState(null);
  const [selectedOptionIncident, setSelectedOptionIncident] = useState(null);

  const handleSelectChangeDocent = (selectedOptionDocent) => {
    setSelectedOptionDocent(selectedOptionDocent);
  };
  const handleSelectChangeIncident = (selectedOptionIncident) => {
    setSelectedOptionIncident(selectedOptionIncident);
  };

  const onSubmitTeacherIncident = async () => {
    if (selectedOptionDocent && selectedOptionIncident) {
      const newTeacherIncident = {
        "incident_teacher": selectedOptionDocent.value,
        "incident": selectedOptionIncident.value
      }
      await startSavingTeacherIncident(newTeacherIncident);
      handleReset();
      onCloseModal();
    } else {
      toast.warning('Debe seleccionar una opción antes de enviar el formulario.', MessageConfig);
    }
  }
  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h3 className="text-center">Nuevo incidente de docente</h3>
      <hr />
      <form className="h-50 mx-3" onSubmit={handleSubmit(onSubmitTeacherIncident)}>
        <div className="form-group my-5">
          <label className="w-100">Docente<span className="text-danger"> *</span>
            <Select
              options={optionsDocent}
              value={selectedOptionDocent}
              onChange={handleSelectChangeDocent}
              placeholder="Seleccionar docente"
            />
          </label>
        </div>
        <div className="form-group my-5">
          <label className="w-100">Incidencia<span className="text-danger"> *</span>
            <Select
              options={optionsIncident}
              value={selectedOptionIncident}
              onChange={handleSelectChangeIncident}
              placeholder="Seleccionar incidencia"
            />
          </label>
        </div>
        <div className="h-50 d-flex align-items-end">
          <div className="w-100 d-flex justify-content-center">
            <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
            <button type="button" className="btn btn-danger w-25 p-1" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
          </div>
        </div>
      </form>
    </Modal>
  );
};
