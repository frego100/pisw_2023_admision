import { useState } from "react";
import Modal from "react-modal";

import "./Incidents.css";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

Modal.setAppElement("#root");

export const IncidentClassForm = ({ isOpen, onCloseModal }) => {
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      {/* <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Hello</h2> */}
      <div className="d-flex justify-content-between">
        <h1 className="registrations-title">Nuevo incidente de aula</h1>
      </div>

      <form className="p-3">
        <div className="mb-3">
          <label className="form-label">Fecha</label>
          <input type="text" className="form-control" disabled placeholder="Ej: 20/02/2024"></input>
        </div>
        <br></br>

        <div className="mb-3">
          <label className="form-label">Escuela</label>
          <select className="form-select" id="school">
            <option value={1}>Ingeniería civil</option>
            <option value={2}>Ingeniería de sistemas</option>
            <option value={3}>Ingeniería mecánica</option>
            <option value={4}>Ingeniería industrial</option>
          </select>
        </div>
        <br></br>

        <div className="mb-3">
          <label className="form-label">Nº de aula</label>
          <input type="text" className="form-control" placeholder="Ej: 201"></input>
        </div>
        <br></br>

        <div className="mb-3">
          <label className="form-label">Descripción</label>
          <textarea className="form-control" rows={3}></textarea>
        </div>
        <br></br>

        <br></br>
        <div className="row justify-content-center">
          <button type="button" className="btn btn-register btn-size">Guardar</button>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button type="button" className="btn btn-danger btn-size" onClick={onCloseModal}>Cancelar</button>
        </div>

      </form>
    </Modal>
  );
};
