import { useEffect, useState } from "react";
import Modal from "react-modal";

import "./Incidents.css";
import { useForm } from "react-hook-form";
import { useIncidentStore } from "../../hooks/useIncidentStore";
import { useFormF } from "../../hooks/useFormF";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "580px",
    width: '400px'
  },
};

Modal.setAppElement("#root");

export const IncidentUpdateForm = ({ isOpen, onCloseModal }) => {
  const { startUpdatingIncident, currentIncident } = useIncidentStore();
  const [values, handleInputChange, setValues] = useFormF({ name: currentIncident.name, description: currentIncident.description, level: currentIncident.level });
  const onSubmitIncident = async (e) => {
    e.preventDefault();
    const updateIncident = {
      "id": currentIncident.id,
      "name": values.name,
      "description": values.description,
      "level": Number(values.level)
    }
    await startUpdatingIncident(updateIncident);
    onCloseModal();
  }
  useEffect(() => {
    setValues({ name: currentIncident.name, description: currentIncident.description, level: currentIncident.level });
  }, [currentIncident])

  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >

      <h1 className="text-center">Editar Incidencia</h1>
      <hr />
      <form className="mx-3" onSubmit={onSubmitIncident}>
        <div className="mb-2">
          <label className="w-100">Nombre de Incidencia
            <input className="form-control" name="name" onChange={handleInputChange} defaultValue={currentIncident.name} />
          </label>
        </div>
        <div className="mb-2">
          <label className="w-100">Descripción
            <textarea className="form-control" name="description" onChange={handleInputChange} defaultValue={currentIncident.description}></textarea>
          </label>
        </div>
        <div className="mb-3">
          <label className="w-100">Nivel
            <select className="form-select" id="school" name="level" onChange={handleInputChange} defaultValue={currentIncident.level}>
              <option value={0}>Seleccione nivel</option>
              <option value={1}>Bajo</option>
              <option value={2}>Normal</option>
              <option value={3}>Alto</option>
              <option value={4}>Crítico</option>
            </select>
          </label>
        </div>
        <div className="row justify-content-center">
          <button type="submit" className="btn btn-primary btn-size">Guardar</button>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button type="button" className="btn btn-danger btn-size" onClick={onCloseModal}>Cancelar</button>
        </div>

      </form>
    </Modal >
  );
};
