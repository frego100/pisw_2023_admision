import React, { useState } from "react";
import Modal from "react-modal";

import { useForm } from "react-hook-form";
import { useIncidentStore } from "../../hooks/useIncidentStore";
import Select from 'react-select'
import { toast } from 'react-toastify';
import { MessageConfig } from "../components/ToastMessage/MessageConfig";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "630px",
    width: "400px",
  },
};

Modal.setAppElement("#root");

export const IncidentForm = ({ isOpen, onCloseModal }) => {
  const { register, handleSubmit, formState: { errors }, reset } = useForm();
  const { startSavingIncident } = useIncidentStore();

  const handleSelectChange = (selectedOption) => {
    setSelectedOption(selectedOption);
  };

  const onSubmitIncident = async (data) => {
    const newIncident = {
      "name": data.name,
      "description": data.description,
      "level": Number(data.level)
    }
    await startSavingIncident(newIncident);
    handleReset();
    onCloseModal();
  }
  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Nueva Incidencia</h1>
      <hr />
      <form className="mx-3" onSubmit={handleSubmit(onSubmitIncident)}>
        <div className="form-group mb-2">
          <label className="w-100">Nombre de Incidencia<span className="text-danger"> *</span>
            <input type="text" className="form-control" placeholder="Ingrese nombre de incidencia" {...register("name", { required: true })} />
            {errors.name && <span className="text-danger">Debes ingresar un nombre para la incidencia</span>}
          </label>
        </div>
        <div className="form-group mb-2">
          <label className="w-100">Descripción<span className="text-danger"> *</span>
            <textarea className="form-control" rows={3} placeholder="Ingrese descripción de incidencia" {...register("description", { required: true })}></textarea>
            {errors.description && <span className="text-danger">Debes ingresar una descripción</span>}
          </label>
        </div>
        <div className="form-group mb-3">
          <label className="w-100">Nivel<span className="text-danger"> *</span>
            <select className="form-select" {...register("level", { required: true })}>
                <option value={1}>Bajo</option>
                <option value={2}>Normal</option>
                <option value={3}>Alto</option>
                <option value={4}>Crítico</option>
            </select>
            {errors.level && <span className="text-danger">Debes ingresar un nivel</span>}
          </label>
        </div>
        <div className="d-flex justify-content-center">
          <button type="submit" className="btn btn-success w-25 me-5 p-1">Guardar</button>
          <button type="button" className="btn btn-danger w-25 p-1" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
        </div>
      </form>
    </Modal >
  );
};
