import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { useRaffleStore } from "../../hooks";
import { RaffleForm } from "./RaffleForm";
import { RaffleWinners } from "./RaffleWinners";

import { GiCardPickup } from "react-icons/gi";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { FaTrashAlt } from "react-icons/fa";
import Swal from 'sweetalert2'

export const Raffles = () => {
  const { raffles, startLoadingRaffles, startDeletingRaffle, startLoadingRaffleWinners } = useRaffleStore();
  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'evaluationName',
      headerName: 'Proceso',
      minWidth: 380,
      flex: 1,
    },
    {
      field: 'rafflerUserEmail',
      headerName: 'Usuario Sorteador',
      minWidth: 250,
    },
    {
      field: 'numberOfWinners',
      headerName: 'N° de ganadores',
      type: 'number',
      minWidth: 80,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'winners',
      headerName: 'Ver',
      minWidth: 150,
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex justify-content-center">
            <button onClick={() =>
              onOpenModalWinners(params.row.raffle)}
              className="btn btn-success" >
              Ganadores
            </button>
          </div>
        );
      },
    },
    {
      field: 'delete',
      headerName: 'Eliminar',
      minWidth: 100,
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex flex-row justify-content-center">
            <button className="btn btn-danger d-flex justify-content-center"
              onClick={() =>
                onDeleteRaffle(params.row.raffle)}
            >
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
  ];

  const rows = raffles.map((raffle, index) => ({
    id: index + 1,
    evaluationName: raffle.round_inscription.evaluation.name,
    rafflerUserEmail: raffle.raffler_user.email,
    numberOfWinners: raffle.number_of_winners,
    raffle: raffle,
  }));

  const [isOpen, setIsOpen] = useState(false);
  const [isOpenModalWinners, setIsOpenModalWinner] = useState(false);
  const [currentRaffle, setCurrentRaffle] = useState({ "id": "" });

  const onOpenModal = () => {
    setIsOpen(true);
  }
  const onCloseModal = () => {
    setIsOpen(false);
  }
  const onOpenModalWinners = (raffle) => {
    startLoadingRaffleWinners(raffle);
    setIsOpenModalWinner(true);
    setCurrentRaffle(raffle);
  }
  const onCloseModalWinners = () => {
    setIsOpenModalWinner(false);
  }

  const onDeleteRaffle = async (raffle) => {
    Swal.fire({
      title: `<strong>Eliminar sorteo</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar este sorteo.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingRaffle(raffle);
      }
    });
  }

  useEffect(() => {
    startLoadingRaffles();
  }, []);
  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap">
          <h1 className="">Sorteos &nbsp; <GiCardPickup/></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <RaffleForm isOpen={isOpen} onCloseModal={onCloseModal} />
      <RaffleWinners isOpen={isOpenModalWinners} onCloseModal={onCloseModalWinners} raffle={currentRaffle} />
    </>
  );
};