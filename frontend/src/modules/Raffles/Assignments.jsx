import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import "./Raffles.css";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { FaUsersGear } from "react-icons/fa6";
import { FaEye } from "react-icons/fa";

import { RiFileExcel2Fill } from "react-icons/ri";
import { FaFilePdf } from "react-icons/fa";
import { useParams } from 'react-router-dom';

import { useSpaceStore, useAssignmentStore, useRaffleStore } from "../../hooks";
import { AssignmentForm } from "./AssignmentForm";
import Swal from "sweetalert2";

export const Assignments = () => {
  const { startLoadingEntrances, startLoadingAreas, startLoadingSchools, startLoadingPavilions, startLoadingClassrooms } = useSpaceStore();
  const { startLoadingCoordinations } = useAssignmentStore();
  const { raffleWinners, setCurrentRaffleWinner, startLoadingRaffleWinnersById, startExportRaffleWinners } = useRaffleStore();
  const { startDeletingAssignment } = useAssignmentStore();
  const { id } = useParams();
  const onViewPDF = (url) => {
    let urlPDF = "https://lavacalola.online" + url;
    window.open(urlPDF, "_blank");
  }
  useEffect(() => {
    startLoadingRaffleWinnersById(id);
  }, [])

  const onDeleteAssignment = async (assignment) => {
    Swal.fire({
      title: `<strong>Eliminar Asignación</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta Asignación.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingAssignment(assignment);
      }
    });
  }

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'name',
      headerName: 'Nombre',
      minWidth: 120,
      flex: 1,
    },
    {
      field: 'email',
      headerName: 'Autor',
      minWidth: 120,
      flex: 1,
    },
    {
      field: 'raffleWinner',
      headerName: 'Asignar cargo',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <button className="btn btn-primary" onClick={() => onOpenModal(params.row.raffleWinner)} disabled={params.row.raffleWinner.is_user_assigned}><FaUsersGear></FaUsersGear></button>
          </div>
        );
      },
    },
    {
      field: 'actions',
      headerName: 'Acciones',
      minWidth: 150,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center gap-2">
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteAssignment(params.row.raffleWinner.assignment)} disabled={!params.row.raffleWinner.is_user_assigned}>
              <FaTrashAlt />
            </button>
            <button className="btn btn-warning d-flex justify-content-center text-white" onClick={() => onViewPDF(params.row.raffleWinner.assignment.credential.credential_file)} disabled={!params.row.raffleWinner.is_user_assigned}>
              <FaEye />
            </button>
          </div>
        );
      },
    }
  ];

  const rows = raffleWinners.map((rafleWinner, index) => ({
    id: index + 1,
    name: rafleWinner.user_winner.first_name + ' ' + rafleWinner.user_winner.last_name,
    email: rafleWinner.user_winner.email,
    raffleWinner: rafleWinner,
    actions: rafleWinner
  }));

  const [isOpen, setIsOpen] = useState(false);
  const onCloseModal = () => {
    setIsOpen(false);
  };
  const onOpenModal = (raffleWinner) => {
    setCurrentRaffleWinner(raffleWinner);
    setIsOpen(true);
  };
  const [isReport, setIsReport] = useState(false)
  const isReportActive = () => {
    if (raffleWinners.every(raffleWinner => raffleWinner.is_user_assigned))
      setIsReport(true);
    else
      setIsReport(false);
  }

  useEffect(() => {
    startLoadingEntrances();
    startLoadingCoordinations();
    startLoadingAreas();
  }, [])
  useEffect(() => {
    isReportActive();
  }, [raffleWinners])

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap">
          <h1 className="">Asignación de cargos</h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-icon btn-outline-success me-3 fs-5 px-2 py-1" disabled={!isReport} onClick={() => startExportRaffleWinners(raffleWinners[0].raffle)}><RiFileExcel2Fill></RiFileExcel2Fill></button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded h-50d overflow-auto">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <AssignmentForm
        isOpen={isOpen}
        onCloseModal={onCloseModal}
      />
    </>
  );
};