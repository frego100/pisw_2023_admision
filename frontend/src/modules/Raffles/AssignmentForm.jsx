import { useState } from "react";
import Modal from "react-modal";
import { useSpaceStore, useAssignmentStore, useRaffleStore } from "../../hooks";
import { useForm } from "react-hook-form";
import { filtersFetch } from "../../helpers";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "800px",
    width: '400px'
  },
};

Modal.setAppElement("#root");

export const AssignmentForm = ({ isOpen, onCloseModal }) => {
  const { coordinations, startSavingAssignment } = useAssignmentStore();
  const { entrances, classrooms, pavilions, schools, areas } = useSpaceStore();
  const { currentRaffleWinner } = useRaffleStore();
  const { register, handleSubmit, formState: { errors }, reset, watch } = useForm();
  const { getPavilionsBySchool, getSchoolsByArea, getClassroomsByPavilion, getEntrancesByArea } = filtersFetch();
  const [currentSchools, setCurrentSchools] = useState([]);
  const [currentPavilions, setCurrentPavilions] = useState([]);
  const [currentClassrooms, setCurrentClassrooms] = useState([]);
  const [currentEntrances, setCurrentEntrances] = useState([]);

  const onChangeArea = async (e) => {
    const filterSchools = await getSchoolsByArea(e.target.value);
    setCurrentSchools(filterSchools);
    const filterEntrances = await getEntrancesByArea(e.target.value);
    setCurrentEntrances(filterEntrances);
  }
  const onChangeSchool = async (e) => {
    const filterPavilions = await getPavilionsBySchool(e.target.value);
    setCurrentPavilions(filterPavilions);
  }
  const onChangePavilion = async (e) => {
    const filterClassrooms = await getClassroomsByPavilion(e.target.value);
    setCurrentClassrooms(filterClassrooms);
  }

  const onSubmitAssigment = async (data) => {
    let space_id = "";
    if (data.assigned_role === "CONTROLLER") {
      space_id = data.school;
    } else if (data.assigned_role === "SUPERVISOR") {
      space_id = data.classroom;
    } else {
      space_id = data.pavilion;
    }
    const newAssigment = {
      "assigned_id": space_id,
      "assigned_role": data.assigned_role,
      "assigned_coordination": data.assigned_coordination,
      "assigned_entrance": data.assigned_entrance,
      "raffle_winner": currentRaffleWinner.id,
    }
    startSavingAssignment(newAssigment);
    handleReset();
    onCloseModal();
  }

  const handleReset = () => {
    reset();
  };
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h3 className="text-center">Asignación del cargo</h3>
      <hr />
      <form className="mx-3" onSubmit={handleSubmit(onSubmitAssigment)}>
        <div className="mb-2">
          <label className="">Coordinacion<span className="text-danger"> *</span></label>
          <select className="form-select" {...register("assigned_coordination", { required: true })}>
            <option value="">Seleccione una Coordinación</option>
            {
              coordinations.map(coordination => (
                <option key={coordination.id} value={coordination.id}>{coordination.name}</option>
              ))
            }
          </select>
          {errors.assigned_coordination && <span className="text-danger">Debes seleccionar una coordinacion</span>}
        </div>
        <div className="mb-2">
          <label className="">Cargo<span className="text-danger"> *</span></label>
          <select className="form-select" {...register("assigned_role", { required: true })}>
            <option value={"SUPERVISOR"}>Supervisor</option>
            <option value={"ADMINISTRATOR"}>Administrador</option>
            <option value={"TECHNICIAN"}>Técnico</option>
            <option value={"CONTROLLER"}>Controlador</option>
          </select>
          {errors.assigned_role && <span className="text-danger">Debes seleccionar un cargo</span>}
        </div>
        <div className="w-100 row mb-3">
          <div className="col">
            <label className="">Área<span className="text-danger"> *</span></label>
            <select className="form-select" {...register("area", { required: true })} onChange={onChangeArea}>
              <option value="">Seleccione una Entrada</option>
              {
                areas.map(area => (
                  <option key={area.id} value={area.id}>{area.name}</option>
                ))
              }
            </select>
            {errors.area && <span className="text-danger">Debes seleccionar un área</span>}
          </div>
          <div className="col">
            <label className="">Escuela<span className="text-danger"> *</span></label>
            <select className="form-select" {...register("school", { required: true })} onChange={onChangeSchool}>
              <option value="">Seleccione un Escuela</option>
              {
                currentSchools.map(school => (
                  <option key={school.id} value={school.id}>{school.name}</option>
                ))
              }
            </select>
            {errors.school && <span className="text-danger">Debes seleccionar una escuela</span>}
          </div>
        </div>
        {watch("assigned_role") !== "CONTROLLER" &&
          <div className="w-100 row mb-3">
            <div className="col">
              <label className="">Pabellón<span className="text-danger"> *</span></label>
              <select className="form-select" {...register("pavilion", { required: true })} onChange={onChangePavilion}>
                <option value="">Seleccione un Pabellon</option>
                {
                  currentPavilions.map(pavilion => (
                    <option key={pavilion.id} value={pavilion.id}>{pavilion.name}</option>
                  ))
                }
              </select>
              {errors.pavilion && <span className="text-danger">Debes seleccionar un pabellon</span>}
            </div>
            {watch("assigned_role") === "SUPERVISOR" &&
              <div className="col">
                <label className="">Aula<span className="text-danger"> *</span></label>
                <select className="form-select" {...register("classroom", { required: true })}>
                  <option value="">Seleccione un Aula</option>
                  {
                    currentClassrooms.map(classroom => (
                      <option key={classroom.id} value={classroom.id}>{classroom.name}</option>
                    ))
                  }
                </select>
                {errors.classroom && <span className="text-danger">Debes seleccionar un aula</span>}
              </div>
            }
          </div>
        }
        <div className="mb-2">
          <label className="">Entrada<span className="text-danger"> *</span></label>
          <select className="form-select" {...register("assigned_entrance", { required: true })}>
            <option value="">Seleccione una Entrada</option>
            {
              currentEntrances.map(entrance => (
                <option key={entrance.id} value={entrance.id}>{entrance.name}</option>
              ))
            }
          </select>
          {errors.assigned_entrance && <span className="text-danger">Debes seleccionar una entrada</span>}
        </div>
        <div className="d-flex h-100 justify-content-center align-items-end mt-4">
          <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
          <button type="button" className="btn btn-danger btn-size" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
