import Modal from "react-modal";
import { useForm } from "react-hook-form"
import { Link } from "react-router-dom";
import "./Raffles.css"
import { useRaffleStore, useRoundStore } from "../../hooks";
import { useEffect, Component } from "react";
// import "./Process.css";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "auto",
    maxHeight: "800px",
    width: "600px",
    maxWidth: "800px"
  },
};

Modal.setAppElement("#root");

export const RaffleWinners = ({ isOpen, onCloseModal }) => {
  const { raffleWinners } = useRaffleStore();

  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center">Ganadores</h1>
      <hr />
      <div className="table-responsive shadow-ms h-45d">
        <table className="table table-striped table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Trabajo</th>
              <th scope="col">Email</th>
            </tr>
          </thead>
          <tbody className="table-group-divider">
            {
              raffleWinners.map((raffleWinner, index) => (
                <tr key={raffleWinner.id}>
                  <th scope="row">{index + 1}</th>
                  <td>{raffleWinner.user_winner.first_name}</td>
                  <td>{raffleWinner.user_winner.work}</td>
                  <td>{raffleWinner.user_winner.email}</td>
                </tr>
              ))
            }
          </tbody>
        </table>
      </div>
      <div className="d-flex justify-content-center mt-3">
        {raffleWinners.length !== 0 && (
          <Link to={`/admin/assignments/${raffleWinners[0].raffle.id}`}
            className="btn btn-success"
            state={raffleWinners}
          >Asignar roles</Link>
        )}
      </div>
    </Modal>
  );
};
