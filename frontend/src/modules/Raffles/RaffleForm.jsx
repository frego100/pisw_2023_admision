import Modal from "react-modal";
import { useForm } from "react-hook-form"
import { useFormF, useRaffleStore, useRoundStore } from "../../hooks";
import { useEffect, useState } from "react";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: 'auto',
    maxHeight: "600px",
  },
};

Modal.setAppElement("#root");

export const RaffleForm = ({ isOpen, onCloseModal, round }) => {
  const { register, handleSubmit, formState: { errors }, onChange, reset } = useForm();
  const [selectedRound, setSelectedRound] = useState(null);
  const { startSavingRaffle } = useRaffleStore();
  const { rounds, startLoadingRounds } = useRoundStore();
  const handleRoundChange = (e) => {
    const selectedRoundID = e.target.value;
    setSelectedRound(selectedRoundID);
  }
  const onSubmitRaffle = async (data) => {
    const newRaffle = {
      "round_inscription": round ? round.id : data.round_inscription,
      "number_of_winners": Number(data.number_of_winners)
    }
    await startSavingRaffle(newRaffle);
    handleReset();
    onCloseModal();
  }
  const handleReset = () => {
    reset();
  };
  useEffect(() => {
    if (!round) {
      startLoadingRounds();
    }
  }, [])
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <hr />
      <h1 className="text-center">Nuevo Sorteo</h1>
      <hr />
      <form onSubmit={handleSubmit(onSubmitRaffle)} className="mx-3">
        {
          round
            ?
            <>
              <div className="col">
                <label className="">Nombre del proceso</label>
                <div className="mt-2">
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control"
                      disabled
                      value={round.evaluation.name}
                    />
                  </div>
                </div>
              </div>
              <br></br>
              <div className="col">
                <label className="">Número de ganadores</label>
                <div className="">
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese un número"
                      {...register("number_of_winners", { required: true, min: 1, max: round.teachers_enrrolled })}
                    />
                  </div>
                  {errors.number_of_winners && <span>Debes ingresar un número entre 1 y {round.teachers_enrrolled}</span>}
                </div>
              </div>
            </>
            :
            <>
              <div className="col">
                <label className="">Seleccione proceso</label>
                <div className="">
                  <select className="form-select" {...register("round_inscription", { onChange: handleRoundChange })}>
                    {
                      rounds.map((round, index) => (
                        <option key={index} value={round.id}>{round.evaluation.name}</option>
                      ))
                    }
                  </select>
                </div>
              </div>
              <br></br>
              <div className="col">
                <label className="">Número de ganadores</label>
                <div className="">
                  <div className="input-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Ingrese un número"
                      {...register("number_of_winners", { required: true, min: 1, max: selectedRound ? rounds.find(round => round.id === selectedRound).teachers_enrrolled : undefined })}
                    />
                  </div>
                  {errors.number_of_winners && <span>Debes ingresar un número entre 1 y {selectedRound ? rounds.find(round => round.id === selectedRound).teachers_enrrolled : undefined}</span>}
                </div>
              </div>
            </>
        }
        <br></br>
        <br></br>
        <div className="d-flex h-100 justify-content-center align-items-end">
          <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
          <button type="button" className="btn btn-danger btn-size" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
        </div>
      </form>
    </Modal>
  );
};
