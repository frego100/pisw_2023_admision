import { Link, Navigate, Route, Routes } from "react-router-dom";
import { ProtectedRoutes } from "./ProtectedRoutes";
import { AdminRoutes } from "./AdminRoutes";
import { TeacherRoutes } from "./TeacherRoutes";
import { useAuthStore } from "../../hooks/useAuthStore";
import { useSelector } from "react-redux";
import { LoginPage } from "../../auth/pages/LoginPage";
import "../components/Loader/Loader.css";
import { toast, ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

export const GeneralRoutes = () => {
  const user = useSelector((state) => state.auth.user);
  const { status } = useAuthStore();

  if (status === 'checking') {
    return (
      <div className="spinner-body">
        <div className="loader-container">
          <div className="loader"></div>
          <h3>Cargando</h3>
        </div>
      </div>  
    )
  }
  return (
    <>
    <Routes>
      <Route
        element={<ProtectedRoutes isAllowed={!!user} redirectTo="/auth" />}
      >
        <Route
          path="/admin/*"
          element={
            <ProtectedRoutes
              isAllowed={!!user && user.user_type === "ADMIN"}
              redirectTo="/teacher"
            >
              <AdminRoutes />
            </ProtectedRoutes>
          }
        />
        <Route
          path="/teacher/*"
          element={
            <ProtectedRoutes isAllowed={!!user && user.user_type === "TEACHER"}>
              <TeacherRoutes />
            </ProtectedRoutes>
          }
        />
      </Route>
      <Route path="/*" element={<Navigate to="/auth/login" />} />
    </Routes>
    <ToastContainer />
    </>
  );
};
