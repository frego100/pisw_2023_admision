import { Routes, Route, Navigate } from "react-router-dom";
import { AdminNavbar } from "../components/Navbar/Admin/AdminNavbar.jsx"
import { Processes } from "../Processes/Processes.jsx";
import { RegistrationRounds } from "../Processes/RegistrationRounds.jsx";
import { Assignments } from "../Raffles/Assignments.jsx";
import { Pavillons } from "../Spaces/Pavillons/Pavillons.jsx";
import { Schools } from "../Spaces/Schools/Schools.jsx";
import { Classrooms } from "../Spaces/Classrooms/Classrooms.jsx";
import { Incidents } from "../Incidents/Incidents.jsx";
import { IncidentsTeacher } from "../Incidents/IncidentsTeacher.jsx";
import { Teachers } from "../Staff/Teachers/Teachers.jsx";

import { useState } from "react";
import { BiChevronLeftCircle, BiChevronRightCircle } from "react-icons/bi";
import { Raffles } from "../Raffles/Raffles.jsx";
import { Entrances } from "../Spaces/Entrances/Entrances.jsx";

BiChevronLeftCircle
export const AdminRoutes = () => {
  const [menu, setMenu] = useState("");
  const [menuAbierto, setMenuAbierto] = useState(true);
  const funcionOnCloseMenu = () => {
    if (menu === "") setMenu("close");
    else setMenu("");
    setMenuAbierto(!menuAbierto);
  };
  return (
    <>
      <AdminNavbar menu={menu} />
      <div className="home-section bg-color-gray h-100" >
        <div className="home-content bg-color-gray pb-2">
          <i
            className="bx bx-menu"
            id="bx-menu"
            onClick={funcionOnCloseMenu} >
            {menuAbierto ? <BiChevronLeftCircle className="toggle" /> : <BiChevronRightCircle className="toggle" />}
          </i>
          <span className="text">SISTEMA DE SELECCIÓN PARA EL PROCESO DE ADMISIÓN</span>
        </div>
        <div className="bg-color-gray w-100 h-100 pb-3">
          <Routes>
            <Route path="/raffles" element={<Raffles />} />
            <Route path="/assignments/:id" element={<Assignments />} />
            <Route path="/registration-rounds" element={<RegistrationRounds />} />
            <Route path="/processes" element={<Processes />} />
            <Route path="/spaces/pavillons" element={<Pavillons />} />
            <Route path="/spaces/schools" element={<Schools />} />
            <Route path="/spaces/classrooms" element={<Classrooms />} />
            <Route path="/spaces/entrances" element={<Entrances />} />
            <Route path="/incidents" element={<Incidents />} />
            <Route path="/incidents-teacher" element={<IncidentsTeacher />} />
            <Route path="/staff/teachers" element={<Teachers />} />

            <Route path="/*" element={<Navigate to="/admin/registration-rounds" />} />
          </Routes>
        </div>
      </div>
    </>
  );
};
