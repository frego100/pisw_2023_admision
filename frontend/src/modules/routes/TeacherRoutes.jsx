import { Routes, Route, Navigate } from "react-router-dom";
import { Registrations } from "../Registrations/Registrations";
import { useState } from "react";
import { TeacherNavbar } from "../components/Navbar/Teacher/TeacherNavbar.jsx";
import { BiChevronLeftCircle, BiChevronRightCircle } from "react-icons/bi";
import { RegistrationForm } from "../Registrations/RegistrationForm";
import { Registered } from "../Registrations/Registered";
import { Result } from "../Registrations/Result";
import { PersonalRegistrations } from "../Registrations/PersonalRegistrations.jsx";
import { StepperInscription } from "../Registrations/StepperIncription.jsx";

export const TeacherRoutes = () => {
  const [menu, setMenu] = useState("");
  const [menuAbierto, setMenuAbierto] = useState(true);
  const funcionOnCloseMenu = () => {
    if (menu === "") setMenu("close");
    else setMenu("");
    setMenuAbierto(!menuAbierto);
  };
  return (
    <>
      <div>
        <TeacherNavbar menu={menu} />
        <div className="home-section bg-color-gray">
          <div className="home-content bg-color-gray pb-2">
            <i className="bx bx-menu" id="bx-menu" onClick={funcionOnCloseMenu}>
              {menuAbierto ? (
                <BiChevronLeftCircle className="toggle" />
              ) : (
                <BiChevronRightCircle className="toggle" />
              )}
            </i>
            <span className="text">
              SISTEMA DE SELECCIÓN PARA EL PROCESO DE ADMISIÓN
            </span>
          </div>
          <div className="bg-color-gray w-100 h-100">
          <Routes>
            <Route path="/registrations" element={<Registrations />} />
            <Route path="/registration-form" element={<RegistrationForm />} />
            <Route path="/registered" element={<Registered />} />
            <Route path="/result" element={<Result />} />
            <Route path="/my-registrations" element={<PersonalRegistrations />} />
            <Route path="/stepper" element={<StepperInscription />} />
            <Route
              path="/*"
              element={<Navigate to="/teacher/registrations" />}
            />
          </Routes>
        </div>
      </div>
      </div>
    </>
  );
};
