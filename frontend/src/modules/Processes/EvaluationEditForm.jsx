import Modal from "react-modal";
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form"
import { useEvaluationStore, useFormF, useRoundStore } from "../../hooks";
import { BsXLg } from "react-icons/bs";
import Select from 'react-select';
import DatePicker from 'react-datepicker'
import { setHours, setMinutes } from 'date-fns';

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        height: "auto",
        maxHeight: "580px",
        width: '400px'
    },
};

Modal.setAppElement("#root");

export const EvaluationEditForm = ({ isOpen, onCloseModal }) => {
    const { exams, startUpdatingEvaluation, currentEvaluation, setCurrentEvaluation } = useEvaluationStore();
    const [values, handleInputChange, setValues] = useFormF({ number: currentEvaluation.number, type: currentEvaluation.type, exam: currentEvaluation.exam.id, scheduled_day: new Date(currentEvaluation.scheduled_day) });
    const [startDate, setStartDate] = useState(() => {
        return setHours(setMinutes(new Date(), 30, 16));
    });
    const onSubmitEvaluation = async (e) => {
        e.preventDefault();
        const updateEvaluation = {
            "id": currentEvaluation.id,
            "type": values.type,
            "number": Number(values.number),
            "exam": values.exam,
            "scheduled_day": startDate,
        }
        await startUpdatingEvaluation(updateEvaluation);
        onCloseModal();
    }
    useEffect(() => {
        setValues({ number: currentEvaluation.number, type: currentEvaluation.type, exam: currentEvaluation.exam.id, scheduled_day: new Date(currentEvaluation.scheduled_day) })
        setStartDate(setHours(setMinutes(new Date(currentEvaluation.scheduled_day), new Date(currentEvaluation.scheduled_day).getMinutes()), new Date(currentEvaluation.scheduled_day).getHours()));
    }, [currentEvaluation.scheduled_day]);
    return (
        <Modal
            isOpen={isOpen}
            //   onAfterOpen={afterOpenModal}
            onRequestClose={onCloseModal}
            style={customStyles}
            contentLabel="Example Modal"
            className="modal"
            overlayClassName="modal-fondo"
            closeTimeoutMS={200}
        >
            <h1 className="text-center">Editar Evaluación</h1>
            <hr />
            <form className="p-3" onSubmit={onSubmitEvaluation}>
                <div className="card-body">
                    <div className="form-group mb-2">
                        <label className="w-100">Seleccione el tipo de evaluación
                            <select className="form-select" name="type" onChange={handleInputChange} defaultValue={currentEvaluation.type}>
                                <option value="PERFIL VOCACIONAL">Perfil vocacional</option>
                                <option value="CONOCIMIENTOS">Conocimientos</option>
                                <option value="APTITUD ACADEMICA">Aptitud Académica</option>
                            </select>
                        </label>
                    </div>
                    <div className="form-group mb-2">
                        <label className="w-100">Número de evaluación
                            <input type="number" className="form-control" placeholder="Ingresar número" name="number" onChange={handleInputChange} defaultValue={currentEvaluation.number} />
                        </label>
                    </div>
                    <div className="form-group mb-2">
                        <label className="w-100">Seleccione examen
                            <select className="form-select" name="exam" onChange={handleInputChange} defaultValue={currentEvaluation.exam.id}>
                                <option value="">Seleccionar</option>
                                {
                                    exams.map(exam => (
                                        <option key={exam.id} value={exam.id}>Proceso {exam.admission_process.year} Examen {exam.type}</option>
                                    ))
                                }
                            </select>
                        </label>
                    </div>
                    <div className="w-100 form-group mb-3">
                        <label className="">Seleccione la Fecha de la Evaluación</label>
                        <DatePicker
                            className="w-100 form-control"
                            showIcon
                            selected={startDate}
                            onChange={(date) => { setStartDate(date) }}
                            showTimeSelect
                            timeFormat="HH:mm"
                            dateFormat="MMMM d, yyyy h:mm aa"
                        />
                    </div>
                    <div className="d-flex h-100 justify-content-center align-items-end">
                        <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
                        <button type="button" className="btn btn-danger btn-size" onClick={onCloseModal}>Cancelar</button>
                    </div>
                </div>
            </form>
        </Modal>
    );
};
