import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { useEvaluationStore, useRoundStore } from "../../hooks";
import { ProcessForm } from "./ProcessForm";
import { RaffleForm } from "../Raffles/RaffleForm";

import { GiNotebook } from "react-icons/gi";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';

import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import Swal from 'sweetalert2'

export const RegistrationRounds = () => {
  const { rounds, startLoadingRounds, startChangingStateRound, startDeletingRound } = useRoundStore();
  const { startLoadingEvaluations } = useEvaluationStore();

  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'evaluationName',
      headerName: 'Nombre',
      minWidth: 380,
      flex: 1,
    },
    {
      field: 'createdByEmail',
      headerName: 'Autor',
      minWidth: 250,
    },
    {
      field: 'teachersEnrolled',
      headerName: 'Inscritos',
      type: 'number',
      minWidth: 80,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'state',
      headerName: 'Abierto',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <div className="form-check form-switch">
              <input className="form-check-input" type="checkbox" role="switch" 
                    defaultChecked={!params.row.round.is_blocked} 
                    onChange={() => handleSelectChange(params.row.round)} 
                    disabled={params.row.round.round_was_raffled}
              />
            </div>
          </div>
        );
      },
    },
    {
      field: 'actions',
      headerName: 'Acciones',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="d-flex flex-row justify-content-center">
            <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteRound(params.row.round)}>
              <FaTrashAlt />
            </button>
          </div>
        );
      },
    },
    {
      field: 'round',
      headerName: 'Sorteo',
      minWidth: 150,
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex justify-content-center">
            {params.row.round.round_was_raffled ? (
              <Link to="/admin/raffles" className="btn btn-secondary w-7d" >
                Ver
              </Link>
            ) : (
              <button
                className="btn btn-success w-7d"
                onClick={() => onOpenModalRaffle(params.row.round)}
              >
                Crear
              </button>
            )}
          </div>
        );
      },
    },
  ];

  const rows = rounds.map((round, index) => ({
    id: index + 1,
    evaluationName: round.evaluation.name,
    createdByEmail: round.created_by.email,
    teachersEnrolled: round.teachers_enrrolled,
    state: round,
    actions: round,
    round: round,
  }));

  const [isOpen, setIsOpen] = useState(false);
  const [isOpenRaffle, setIsOpenRaffle] = useState(false);
  const [currentRound, setCurrentRound] = useState({ "evaluation": { "name": "" } });
  const onCloseModal = () => {
    setIsOpen(false);
  };
  const onOpenModal = () => {
    setIsOpen(true);
    startLoadingEvaluations();
  };
  const onCloseModalRaffle = () => {
    setIsOpenRaffle(false);
  };
  const onOpenModalRaffle = (currentRound) => {
    if(currentRound.is_blocked){
      setIsOpenRaffle(true);
      setCurrentRound(currentRound);
    } else {
      Swal.fire({
        title: `<strong>Ronda de inscripcion abierta</strong>`,
        icon: "warning",
        html: `
          No puede generar un sorteo si la ronda sigue abierta para registrarse. Cierre la ronda.
        `,
        showCloseButton: false,
        showCancelButton: true,
        showConfirmButton: false,
        cancelButtonText: `
          Volver
        `,
        cancelButtonAriaLabel: "Thumbs down",
      });
    }
  };

  const handleSelectChange = async (round) => {
    const changeRound = {
      ...round,
      "is_blocked": !round.is_blocked
    }
    await startChangingStateRound(changeRound);
  }

  const onDeleteRound = async (round) => {
    Swal.fire({
      title: `<strong>Eliminar ronda de inscripción</strong>`,
      icon: "warning",
      html: `
        Esta seguro de eliminar esta ronda de inscripción.
      `,
      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText: `
        Si
      `,
      confirmButtonAriaLabel: "Thumbs up, great!",
      cancelButtonText: `
        No
      `,
      cancelButtonAriaLabel: "Thumbs down",
      preConfirm: async () => {
        return startDeletingRound(round);
      }
    });
  }

  useEffect(() => {
    startLoadingRounds();
  }, []);

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap px-4">
          <h1 className="">Rondas de Inscripción &nbsp; <GiNotebook/></h1>
          <div className="d-flex align-items-center">
            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenModal}>Nuevo</button>
          </div>
        </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
      <ProcessForm isOpen={isOpen} onCloseModal={onCloseModal} />
      <RaffleForm isOpen={isOpenRaffle} onCloseModal={onCloseModalRaffle} round={currentRound} />
    </>
  );
};