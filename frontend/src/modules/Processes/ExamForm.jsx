import Modal from "react-modal";
import { useForm } from "react-hook-form"
import { useEvaluationStore, useRoundStore } from "../../hooks";
import { onlyNumbers } from "../components/Functions/OnlyNumbersInput";

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        height: "auto",
        maxHeight: "580px",
        width: "400px"
    },
};

Modal.setAppElement("#root");

export const ExamForm = ({ isOpen, onCloseModal }) => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { admissionProcesses, startSavingExam } = useEvaluationStore();

    const onSubmitExam = async (data) => {
        const newExam = {
            "admission_process": data.admission_process,
            "type": data.type,
            "number": Number(data.number)
        }
        await startSavingExam(newExam);
        onCloseModal();
    }
    return (
        <Modal
            isOpen={isOpen}
            //   onAfterOpen={afterOpenModal}
            onRequestClose={onCloseModal}
            style={customStyles}
            contentLabel="Example Modal"
            className="modal"
            overlayClassName="modal-fondo"
            closeTimeoutMS={200}
        >
            <h1 className="text-center">Nuevo Examen</h1>
            <hr />
            <form className="mx-3" onSubmit={handleSubmit(onSubmitExam)}>
                <div className="form-group mb-2">
                    <label className="w-100">Seleccione el tipo de examen<span className="text-danger"> *</span>
                        <select className="form-select" {...register("type", { required: true })}>
                            <option selected>Seleccionar</option>
                            <option value="CEPRE">Cepreunsa</option>
                            <option value="CEPRE QUINTOS">Cepreunsa Ciclo Quintos</option>
                            <option value="ORDINARIO">Ordinario</option>
                            <option value="EXTRAORDINARIO">Extraordinario</option>
                            <option value="ORDINARIO FILIALES">Ordinario Filiales</option>
                        </select>
                        {errors.type_exame && <span>Debes seleccionar un tipo de examen</span>}
                    </label>
                </div>
                <div className="form-group mb-2">
                    <label className="w-100" >Número de examen<span className="text-danger"> *</span>
                        <input type="number" className="form-control" id="number_evaluation" placeholder="Ingresar número" onKeyPress={onlyNumbers} {...register("number", { required: true })} />
                        {errors.number && <span>Debes ingresar un número de examen</span>}
                    </label>
                </div>
                <div className="form-group mb-3">
                    <label className="w-100">Seleccione proceso de admisión<span className="text-danger"> *</span>
                        <select className="form-select" {...register("admission_process", { required: true })}>
                            <option selected>Seleccionar</option>
                            {
                                admissionProcesses.map(admissionProcess => (
                                    <option key={admissionProcess.id} value={admissionProcess.id}> Proceso {admissionProcess.year}</option>
                                ))
                            }
                        </select>
                        {errors.admission_process && <span>Debes seleccionar un proceso de admisión</span>}
                    </label>
                </div>
                <div className="d-flex h-100 justify-content-center align-items-end">
                    <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
                    <button type="button" className="btn btn-danger btn-size" onClick={onCloseModal}>Cancelar</button>
                </div>
            </form>
        </Modal>
    );
};
