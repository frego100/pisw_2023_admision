import Modal from "react-modal";
import { useForm } from "react-hook-form"
import { useEvaluationStore, useRoundStore } from "../../hooks";
import { BsXLg } from "react-icons/bs";
import Select from 'react-select'
import { onlyNumbers } from "../components/Functions/OnlyNumbersInput";

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        height: "auto",
        maxHeight: "580px",
        width: '400px'
    },
};

Modal.setAppElement("#root");

export const AdmissionProcessForm = ({ isOpen, onCloseModal }) => {
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const { startSavingAdmissionProcess } = useEvaluationStore();

    const onSubmitAdmissionProcess = async (data) => {
        const newAdmissionProcess = {
            "year": data.year
        }
        await startSavingAdmissionProcess(newAdmissionProcess);
        handleReset();
        onCloseModal();
    }
    const handleReset = () => {
        reset();
      };
    return (
        <Modal
            isOpen={isOpen}
            //   onAfterOpen={afterOpenModal}
            onRequestClose={onCloseModal}
            style={customStyles}
            contentLabel="Example Modal"
            className="modal"
            overlayClassName="modal-fondo"
            closeTimeoutMS={200}
        >

            <h1 className="text-center">Nuevo Proceso de Admisión</h1>
            <hr />

            <form className="mx-3" onSubmit={handleSubmit(onSubmitAdmissionProcess)}>
                <div className="form-group mb-3">
                    <label className="w-100">Año del proceso<span className="text-danger"> *</span>
                        <input type="text" className="form-control" id="age_process" onKeyPress={onlyNumbers} placeholder="Ingresar el año" {...register("year", { required: true })}></input>
                        {errors.year && <span>Debes ingresar un año</span>}
                    </label>
                </div>
                <div className="d-flex h-100 justify-content-center align-items-end">
                    <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
                    <button type="button" className="btn btn-danger btn-size" onClick={() => {onCloseModal(); handleReset();}}>Cancelar</button>
                </div>
            </form>
        </Modal>
    );
};
