
import { useEffect, useState } from "react";
import { EvaluationForm } from "./EvaluationForm";
import { AdmissionProcessForm } from "./AdmissionProcessForm";
import { ExamForm } from "./ExamForm";

import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';

import { FaTrashAlt } from "react-icons/fa";
import { FaPencilAlt } from "react-icons/fa";
import { useEvaluationStore } from "../../hooks";
import Swal from "sweetalert2";
import { EvaluationEditForm } from "./EvaluationEditForm";

export const Processes = () => {
    const { startLoadingAdmissionProcesses, startLoadingExams, startLoadingEvaluations, admissionProcesses, exams, evaluations, startDeletingAdmissionProcess, startDeletingExam, startDeletingEvaluation, setCurrentEvaluation } = useEvaluationStore();

    const onDeleteAdmissionProcess = async (admissionProcess) => {
        Swal.fire({
            title: `<strong>Eliminar Proceso de Admisión</strong>`,
            icon: "warning",
            html: `
              Esta seguro de eliminar este Proceso de Admisión.
            `,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: `
              Si
            `,
            confirmButtonAriaLabel: "Thumbs up, great!",
            cancelButtonText: `
              No
            `,
            cancelButtonAriaLabel: "Thumbs down",
            preConfirm: async () => {
                return startDeletingAdmissionProcess(admissionProcess);
            }
        });
    }

    const onDeleteExam = async (exam) => {
        Swal.fire({
            title: `<strong>Eliminar Examen</strong>`,
            icon: "warning",
            html: `
              Esta seguro de eliminar este Examen.
            `,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: `
              Si
            `,
            confirmButtonAriaLabel: "Thumbs up, great!",
            cancelButtonText: `
              No
            `,
            cancelButtonAriaLabel: "Thumbs down",
            preConfirm: async () => {
                return startDeletingExam(exam);
            }
        });
    }

    const onDeleteEvaluation = async (evaluation) => {
        Swal.fire({
            title: `<strong>Eliminar Evaluación</strong>`,
            icon: "warning",
            html: `
              Esta seguro de eliminar esta Evaluación.
            `,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: `
              Si
            `,
            confirmButtonAriaLabel: "Thumbs up, great!",
            cancelButtonText: `
              No
            `,
            cancelButtonAriaLabel: "Thumbs down",
            preConfirm: async () => {
                return startDeletingEvaluation(evaluation);
            }
        });
    }

    const columnsProcess = [
        {
            field: 'id',
            headerName: 'ID',
            minWidth: 20,
            align: 'center',
            headerAlign: 'center',
            disableColumnMenu: true,
        },
        {
            field: 'year',
            headerName: 'Año del proceso',
            minWidth: 140,
            align: 'center',
            headerAlign: 'center',
            flex: 1,
        },
        {
            field: 'actions',
            headerName: 'Acciones',
            minWidth: 150,
            headerAlign: 'center',
            disableColumnMenu: true,
            renderCell: (params) => {
                return (
                    <div className="w-100 d-flex flex-row justify-content-center">
                        <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteAdmissionProcess(params.row.admissionProcess)}>
                            <FaTrashAlt />
                        </button>
                    </div>
                );
            },
        }
    ]

    const rowsAdmissionProcesses = admissionProcesses.map((admissionProcess, index) => ({
        id: index + 1,
        year: admissionProcess.year,
        admissionProcess: admissionProcess
    }))

    const columnsExams = [
        {
            field: 'id',
            headerName: 'ID',
            minWidth: 20,
            align: 'center',
            headerAlign: 'center',
            disableColumnMenu: true,
        },
        {
            field: 'type',
            headerName: 'Tipo de examen',
            minWidth: 200,
        },
        {
            field: 'number',
            headerName: 'Número de examen',
            type: 'number',
            minWidth: 150,
            align: 'center',
            headerAlign: 'center',
        },
        {
            field: 'admission_process',
            headerName: 'Proceso de admisión',
            minWidth: 200,
            align: 'center',
            headerAlign: 'center',
        },
        {
            field: 'actions',
            headerName: 'Acciones',
            minWidth: 150,
            headerAlign: 'center',
            disableColumnMenu: true,
            renderCell: (params) => {
                return (
                    <div className="w-100 d-flex flex-row justify-content-center">
                        <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteExam(params.row.exam)}>
                            <FaTrashAlt />
                        </button>
                    </div>
                );
            },
        }
    ]

    const rowsExams = exams.map((exam, index) => ({
        id: index + 1,
        type: exam.type,
        number: exam.number,
        admission_process: `Proceso ${exam.admission_process.year}`,
        exam: exam
    }))

    const columnsEvaluations = [
        {
            field: 'id',
            headerName: 'ID',
            minWidth: 20,
            align: 'center',
            headerAlign: 'center',
            disableColumnMenu: true,
        },
        {
            field: 'type',
            headerName: 'Tipo de examen',
            minWidth: 200,
        },
        {
            field: 'number',
            headerName: 'Número de examen',
            type: 'number',
            minWidth: 150,
            align: 'center',
            headerAlign: 'center',
        },
        {
            field: 'name',
            headerName: 'Proceso de admisión',
            minWidth: 200,
            flex: 1,
        },
        {
            field: 'scheduled_day',
            headerName: 'Fecha',
            minWidth: 200,
            flex: 1,
        },
        {
            field: 'scheduled_hour',
            headerName: 'Hora',
            minWidth: 200,
            disableColumnMenu: true,
            flex: 1,
        },
        {
            field: 'actions',
            headerName: 'Acciones',
            minWidth: 150,
            headerAlign: 'center',
            disableColumnMenu: true,
            renderCell: (params) => {
                return (
                    <div className="w-100 d-flex flex-row justify-content-center">
                        <button className="btn btn-warning text-white d-flex justify-content-center me-2" onClick={() => onOpenEvaluationEditModal(params.row.evaluation)}>
                            <FaPencilAlt />
                        </button>
                        <button className="btn btn-danger d-flex justify-content-center" onClick={() => onDeleteEvaluation(params.row.evaluation)}>
                            <FaTrashAlt />
                        </button>
                    </div>
                );
            },
        }
    ]

    const rowsEvaluations = evaluations.map((evaluation, index) => ({
        id: index + 1,
        type: evaluation.type,
        number: evaluation.number,
        name: evaluation.name,
        scheduled_day: new Date(evaluation.scheduled_day).toLocaleDateString(),
        scheduled_hour: new Date(evaluation.scheduled_day).toLocaleTimeString(),
        evaluation: evaluation
    }))

    const [isOpenAdmissionProcess, setIsOpenAdmissionProcess] = useState(false);
    const [isOpenExam, setIsOpenExam] = useState(false);
    const [isOpenEvaluation, setIsOpenEvaluation] = useState(false);
    const [isOpenEvaluationEdit, setIsOpenEvaluationEdit] = useState(false);

    const onOpenAdmissionProcessModal = () => {
        setIsOpenAdmissionProcess(true);
    }
    const onCloseAdmissionProcessModal = () => {
        setIsOpenAdmissionProcess(false);
    };
    const onOpenExamModal = () => {
        setIsOpenExam(true);
    }
    const onCloseExamModal = () => {
        setIsOpenExam(false);
    };
    const onOpenEvaluationModal = () => {
        setIsOpenEvaluation(true);
    }
    const onCloseEvaluationModal = () => {
        setIsOpenEvaluation(false);
    };
    const onOpenEvaluationEditModal = (evaluation) => {
        setCurrentEvaluation(evaluation);
        setIsOpenEvaluationEdit(true);
    }
    const onCloseEvaluationEditModal = () => {
        setIsOpenEvaluationEdit(false);
    };

    useEffect(() => {
        startLoadingEvaluations();
        startLoadingExams();
        startLoadingAdmissionProcesses();
    }, [])

    return (
        <>
            <div className="w-100 row">
                <div className="h-100 col-part pt-4 ps-4 pe-2">
                    <div className="bg-white h-100 py-4 rounded">
                        <div className="d-flex justify-content-between flex-wrap px-4 mb-4">
                            <span className="fs-4">Proceso de admisión</span>
                            <div className="d-flex align-items-center">
                                <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenAdmissionProcessModal}>Nuevo</button>
                            </div>
                        </div>
                        <div className="d-flex justify-content-between flex-wrap px-4">
                            <Box sx={{ width: '100%' }}>
                                <DataGrid
                                    rows={rowsAdmissionProcesses}
                                    columns={columnsProcess}
                                    initialState={{
                                        pagination: {
                                            paginationModel: {
                                                pageSize: 5,
                                            },
                                        },
                                    }}
                                    pageSizeOptions={[5]}
                                    disableRowSelectionOnClick
                                    autoHeight
                                />
                            </Box>
                        </div>
                    </div>
                </div>
                <div className="h-100 col-part pt-4 pl-2 pe-2">
                    <div className="bg-white py-4 rounded">
                        <div className="d-flex justify-content-between flex-wrap px-4 mb-4">
                            <span className="fs-4">Examenes</span>
                            <div className="d-flex align-items-center">
                                <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenExamModal}>Nuevo</button>
                            </div>
                        </div>
                        <div className="d-flex justify-content-between flex-wrap px-4">
                            <Box sx={{ width: '100%' }}>
                                <DataGrid
                                    rows={rowsExams}
                                    columns={columnsExams}
                                    initialState={{
                                        pagination: {
                                            paginationModel: {
                                                pageSize: 5,
                                            },
                                        },
                                    }}
                                    pageSizeOptions={[5]}
                                    disableRowSelectionOnClick
                                    autoHeight
                                />
                            </Box>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-100 row ps-4 pe-2 py-4">
                <div className="bg-white py-4 rounded">
                    <div className="d-flex justify-content-between flex-wrap px-4 mb-4">
                        <span className="fs-4">Evaluación</span>
                        <div className="d-flex align-items-center">
                            <button className="btn btn-primary bg-color-primary border-color-primary" onClick={onOpenEvaluationModal}>Nuevo</button>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between flex-wrap px-4">
                        <Box sx={{ width: '100%' }}>
                            <DataGrid
                                rows={rowsEvaluations}
                                columns={columnsEvaluations}
                                initialState={{
                                    pagination: {
                                        paginationModel: {
                                            pageSize: 5,
                                        },
                                    },
                                }}
                                pageSizeOptions={[5]}
                                disableRowSelectionOnClick
                                autoHeight
                            />
                        </Box>
                    </div>
                </div>
            </div>
            <EvaluationForm isOpen={isOpenEvaluation} onCloseModal={onCloseEvaluationModal} />
            <EvaluationEditForm isOpen={isOpenEvaluationEdit} onCloseModal={onCloseEvaluationEditModal} />
            <AdmissionProcessForm isOpen={isOpenAdmissionProcess} onCloseModal={onCloseAdmissionProcessModal} />
            <ExamForm isOpen={isOpenExam} onCloseModal={onCloseExamModal} />
        </>
    )
}
