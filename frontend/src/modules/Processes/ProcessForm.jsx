import Modal from "react-modal";
import { useForm } from "react-hook-form"
import { useEvaluationStore, useRoundStore } from "../../hooks";
import { BsXLg } from "react-icons/bs";
import Select from 'react-select'
import React, { useState } from 'react';
import { toast } from 'react-toastify';
import { MessageConfig } from "../components/ToastMessage/MessageConfig";

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        height: "auto",
        maxHeight: "580px",
    },
};

Modal.setAppElement("#root");

export const ProcessForm = ({ isOpen, onCloseModal }) => {
    const { register, handleSubmit } = useForm();
    const { evaluations } = useEvaluationStore();
    const options = evaluations.map((evaluation) => ({
        value: evaluation.id, label: evaluation.name
    }));
    const [selectedOption, setSelectedOption] = useState(null);

    const handleSelectChange = (selectedOption) => {
        setSelectedOption(selectedOption);
    };
    const { startSavingRound } = useRoundStore();
    const onSubmit = async () => {
        if (selectedOption) {
            const newRound = {
                "evaluation": selectedOption.value,
            };
            await startSavingRound(newRound);
            onCloseModal();
        } else {
            toast.warning('Debe seleccionar una opción antes de enviar el formulario.', MessageConfig);
        }
    };

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onCloseModal}
            style={customStyles}
            contentLabel="Example Modal"
            className="modal"
            overlayClassName="modal-fondo"
            closeTimeoutMS={200}
        >
            <h1 className="fs-2 text-p1 text-center text-primary">Nuevo Ronda de Inscripción</h1>
            <hr />

            <form onSubmit={handleSubmit(onSubmit)} className="p-3">
                <div className="card-body">
                    <div className="form-group mb-4">
                        <label className="mb-2">Seleccione evaluación</label>
                        <Select
                            options={options}
                            value={selectedOption}
                            onChange={handleSelectChange}
                            placeholder="Seleccione Evaluación"

                        />
                    </div>
                    <div className="d-flex h-100 justify-content-center align-items-end">
                        <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
                        <button type="button" className="btn btn-danger btn-size" onClick={onCloseModal}>Cancelar</button>
                    </div>
                </div>
            </form>
        </Modal>
    );
};
