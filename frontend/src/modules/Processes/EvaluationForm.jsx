import Modal from "react-modal";
import { useState } from "react";
import { useForm } from "react-hook-form"
import { useEvaluationStore, useRoundStore } from "../../hooks";
import { BsXLg } from "react-icons/bs";
import Select from 'react-select'
import DatePicker from 'react-datepicker'
import { setHours, setMinutes } from 'date-fns';
import "./Processes.css";
import { onlyNumbers } from "../components/Functions/OnlyNumbersInput";


const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        height: "auto",
        maxHeight: "580px",
        width: '400px'
    },
};

Modal.setAppElement("#root");

export const EvaluationForm = ({ isOpen, onCloseModal }) => {
    const { exams, startSavingEvaluation } = useEvaluationStore();
    const { register, handleSubmit, formState: { errors }, reset } = useForm();
    const [startDate, setStartDate] = useState(
        setHours(setMinutes(new Date(), 30), 16),
    );
    const onSubmitEvaluation = async (data) => {
        const newEvaluation = {
            "type": data.type,
            "number": Number(data.number),
            "exam": data.exam,
            "scheduled_day": startDate
        }
        await startSavingEvaluation(newEvaluation);
        handleReset();
        onCloseModal();
    }
    const handleReset = () => {
        reset();
    };
    return (
        <Modal
            isOpen={isOpen}
            //   onAfterOpen={afterOpenModal}
            onRequestClose={onCloseModal}
            style={customStyles}
            contentLabel="Example Modal"
            className="modal"
            overlayClassName="modal-fondo"
            closeTimeoutMS={200}
        >
            <h1 className="text-center">Nueva Evaluación</h1>
            <hr />
            <form className="mx-3" onSubmit={handleSubmit(onSubmitEvaluation)}>
                <div className="form-group mb-2">
                    <label className="w-100">Seleccione el tipo de evaluación<span className="text-danger"> *</span>
                        <select className="form-select" {...register("type", { required: true })}>
                            <option value="">Seleccionar</option>
                            <option value="PERFIL VOCACIONAL">Perfil vocacional</option>
                            <option value="CONOCIMIENTOS">Conocimientos</option>
                            <option value="APTITUD ACADEMICA">Aptitud Académica</option>
                        </select>
                        {errors.type && <span className="text-danger">Debes seleccionar tipo de evaluación</span>}
                    </label>
                </div>
                <div className="form-group mb-2">
                    <label className="w-100">Número de evaluación<span className="text-danger"> *</span>
                        <input type="number" className="form-control" placeholder="Ingresar número de evaluación" onKeyPress={onlyNumbers} {...register("number", { required: true })} />
                        {errors.number && <span className="text-danger">Debes ingresar número de evaluación</span>}
                    </label>
                </div>
                <div className="form-group mb-2">
                    <label className="w-100">Seleccione examen<span className="text-danger"> *</span>
                        <select className="form-select" {...register("exam", { required: true })}>
                            {
                                exams.map(exam => (
                                    <option key={exam.id} value={exam.id}>Proceso {exam.admission_process.year} Examen {exam.type}</option>
                                ))
                            }
                        </select>
                        {errors.exam && <span className="text-danger">Debes seleccionar un examen</span>}
                    </label>
                </div>
                <div className="w-100 form-group mb-3">
                    <label className="">Seleccione la Fecha de la Evaluación<span className="text-danger"> *</span>
                        <DatePicker
                            className="w-100 form-control"
                            showIcon
                            selected={startDate}
                            onChange={(date) => setStartDate(date)}
                            showTimeSelect
                            timeFormat="HH:mm"
                            injectTimes={[
                                setHours(setMinutes(new Date(), 1), 0),
                                setHours(setMinutes(new Date(), 5), 12),
                                setHours(setMinutes(new Date(), 59), 23),
                            ]}
                            dateFormat="MMMM d, yyyy h:mm aa"
                        />
                    </label>
                </div>
                <div className="d-flex h-100 justify-content-center align-items-end">
                    <button type="submit" className="btn btn-success btn-size me-5">Guardar</button>
                    <button type="button" className="btn btn-danger btn-size" onClick={() => { onCloseModal(); handleReset(); }}>Cancelar</button>
                </div>
            </form>
        </Modal>
    );
};
