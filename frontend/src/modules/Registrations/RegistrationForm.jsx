import { Link, useLocation, useNavigate } from "react-router-dom";
import { UserInfo } from "./UserInfo";
import { useState } from "react";
import { useAuthStore, useRegistrationStore } from "../../hooks";
import { useForm } from "react-hook-form";
export const RegistrationForm = ({handleNext}) => {
  const { handleSubmit } = useForm();
  const navigate = useNavigate();
  const { startSavingRegistration } = useRegistrationStore();
  const [isOpen, setIsOpen] = useState(false);
  const { user } = useAuthStore();
  const { state } = useLocation();
  const onCloseModal = () => {
    setIsOpen(false);
  };
  const onOpenModal = () => {
    setIsOpen(true);
  };
  const onSubmitRegistration = async () => {
    const newRegistration = {
      "round_inscription": state.id
    }
    await startSavingRegistration(newRegistration);
    handleNext();
  }
  return (
    <>
    <div className="w-50d mb-3">
      <form onSubmit={handleSubmit(onSubmitRegistration)} className="card">
        <h5 className="card-header text-center">Información de Usuario</h5>
        <div className="card-body">
          <h5 className="card-title">Proceso</h5>
          <p className="card-text">{state.evaluation.name}</p>
          <h5 className="card-title">Correo del Postulante</h5>
          <p className="card-text">{user.email}</p>
          <h5 className="card-title">Rol del Postulante</h5>
          <p className="card-text">{user.user_type === "TEACHER" && "DOCENTE"}</p>
          <h5 className="card-title">Trabajo del Postulante</h5>
          <p className="card-text">{user.work ? user.work : "No tiene trabajo"} </p>
          <hr />
          <div className="d-flex justify-content-around">
            <button type="button" className="btn btn-secondary" onClick={onOpenModal}>
              Editar Perfil
            </button>
            <button type="submit" className="btn btn-success">Inscribirse</button>
          </div>
        </div>
      </form>
    </div>
      <UserInfo
        isOpen={isOpen}
        onCloseModal={onCloseModal}
      />
    </>
  );
};
