import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { GiArchiveRegister } from "react-icons/gi";
import { useRegistrationStore } from "../../hooks/useRegistrationStore";
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';

export const PersonalRegistrations = () => {
  const { registrations, startLoadingRegistrations } = useRegistrationStore();

  console.log(registrations)
  useEffect(() => {
    startLoadingRegistrations();
  }, [])
  const columns = [
    {
      field: 'id',
      headerName: 'ID',
      minWidth: 20,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
    },
    {
      field: 'admision_process',
      headerName: 'Proceso',
      minWidth: 120,
      headerAlign: 'center',
      flex: 1,
      align: "center"
    },
    {
      field: 'exam',
      headerName: 'Examen',
      minWidth: 200,
      headerAlign: 'center',
      flex: 1,
      align: "center"
    },
    {
      field: 'evaluation',
      headerName: 'Evaluación',
      minWidth: 200,
      headerAlign: 'center',
      flex: 1,
      align: "center"
    },
    {
      field: 'state',
      headerName: 'Estado',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex flex-row justify-content-center">
            <div className={'p-1 border border-'+(params.row.state ? 'success' : 'warning')+' rounded'}>
              <span className={'text-'+(params.row.state ? 'success' : 'warning')}>{params.row.state ? 'Terminado' : 'En proceso'}</span>
            </div>
          </div>
        );
      },
    },
    {
      field: 'result',
      headerName: 'Resultados',
      minWidth: 90,
      align: 'center',
      headerAlign: 'center',
      disableColumnMenu: true,
      renderCell: (params) => {
        return (
          <div className="w-100 d-flex flex-row justify-content-center">
            <div className={'p-1 border border-'+(params.row.state ? (params.row.result ? 'success' : 'danger') : 'warning')+' rounded'}>
              <span className={'text-'+(params.row.state ? (params.row.result ? 'success' : 'danger') : 'warning')}>{params.row.state ? (params.row.result ? 'Ganador' : 'Perdedor') : 'En espera'}</span>
            </div>
          </div>
        );
      },
    },
  ];

  const rows = registrations.map((registration, index) => ({
    id: index + 1,
    admision_process: `Proceso ${registration.round_inscription.evaluation.exam.admission_process.year}`,
    exam: `${registration.round_inscription.evaluation.exam.type} ${registration.round_inscription.evaluation.exam.number}`,
    evaluation: `${registration.round_inscription.evaluation.type} ${registration.round_inscription.evaluation.number}`,
    state: registration.round_inscription.round_was_raffled,
    result: registration.is_user_winner,
    registration: registration,
  }));
  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <div className="d-flex justify-content-between flex-wrap">
          <h1 className="">Mis inscripciones  &nbsp; <GiArchiveRegister/></h1>
          </div>
      </div>
      <br></br>
      <div className="bg-white mx-4 p-3 rounded">
        <Box sx={{ width: '100%' }}>
          <DataGrid
            rows={rows}
            columns={columns}
            initialState={{
              pagination: {
                paginationModel: {
                  pageSize: 5,
                },
              },
            }}
            pageSizeOptions={[5]}
            disableRowSelectionOnClick
            autoHeight
          />
        </Box>
      </div>
    </>
  );
};
