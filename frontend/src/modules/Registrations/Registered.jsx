import { Link, useLocation, useNavigate } from "react-router-dom"
import registered from "../../assets/ok.png"
import { useRegistrationStore } from "../../hooks";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Swal from 'sweetalert2';

export const Registered = ({ handleBack }) => {
    const { handleSubmit } = useForm()
    const { registrations, startDeletingRegistration, currentRegistration, setCurrentRegistration } = useRegistrationStore();
    const { state } = useLocation();

    useEffect(() => {
        setCurrentRegistration(registrations.filter(registration => state.id === registration.round_inscription.id)[0]);
    }, [])
    const onSubmitDeleteRegistration = async () => {
        Swal.fire({
            title: `<strong>Eliminar Inscripción</strong>`,
            icon: "warning",
            html: `
              Esta seguro de eliminar inscripción.
            `,
            showCloseButton: false,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText: `
              Si
            `,
            confirmButtonAriaLabel: "Thumbs up, great!",
            cancelButtonText: `
              No
            `,
            cancelButtonAriaLabel: "Thumbs down",
            preConfirm: async () => {
                await startDeletingRegistration(currentRegistration);
                handleBack();
            }
        });
    }
    return (
        <div className="h-100 d-flex justify-content-center align-items-center">
            <form onSubmit={handleSubmit(onSubmitDeleteRegistration)} className="card mb-3 p-3" style={{ "maxWidth": "640px" }}>
                <div className="row g-0">
                    <div className="col-md-4">
                        <img src={registered} className="img-fluid rounded-start" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body text-center">
                            <h5 className="card-title">Inscripción Realizada</h5>
                            <p className="card-text">La inscripción se completo correctamente, se procedera a realizar la selección en unos días.</p>
                            <p className="card-text"><small className="text-body-secondary">Sorteo activo</small></p>
                            <div className="d-flex justify-content-around">
                                <button type="submit" className="btn btn-danger">Eliminar Inscripción</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}