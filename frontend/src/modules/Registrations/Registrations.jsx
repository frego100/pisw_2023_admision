import { Link } from "react-router-dom";
import { useRegistrationStore } from "../../hooks";
import { useEffect } from "react";

export const Registrations = () => {
  const { openRounds, startLoadingOpenRounds, startLoadingRegistrations } = useRegistrationStore();
  useEffect(() => {
    startLoadingOpenRounds();
    startLoadingRegistrations();
  }, [])
  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <hr />
        <div className="d-flex justify-content-center">
          <h1 className="">Procesos Abiertos</h1>
        </div>
        <hr />
        <div className="d-flex justify-content-center">
          <div className="d-flex flex-row flex-wrap">
            {
              openRounds.map((round) => (
                <div key={round.id}>
                  <div className="card card-w-responsive text-center mx-2">
                    <div className="card-header">{round.round_was_raffled ? "Terminado" : (round.is_blocked) ? "Cerrado" : "Abierto"}</div>
                    <div className="card-body">
                      <h5 className="card-title">Proceso de Admisión</h5>
                      <p className="card-text">
                        {round.evaluation.name}
                      </p>
                      <div className="mt-auto mb-0">
                        {
                          (round.is_user_inscripted)
                            ?
                            (round.round_was_raffled) ?
                              <Link
                                className="btn btn-primary"
                                to="/teacher/stepper/"
                                state={{ ...round, indexStep: 2 }}
                                style={{ pointerEvents: (round.round_was_raffled && !round.is_user_inscripted) ? 'none' : '' }}
                              >
                                {(round.round_was_raffled && !round.is_user_inscripted) ? "Terminado" : "Ver"}
                              </Link> : <Link
                                className="btn btn-primary"
                                to="/teacher/stepper/"
                                state={{ ...round, indexStep: 1 }}
                                style={{ pointerEvents: (round.round_was_raffled && !round.is_user_inscripted) ? 'none' : '' }}
                              >
                                {(round.round_was_raffled && !round.is_user_inscripted) ? "Terminado" : "Ver"}
                              </Link>
                            :
                            <Link
                              className="btn btn-primary"
                              to="/teacher/stepper/"
                              state={{ ...round, indexStep: 0 }}
                              style={{ pointerEvents: (round.round_was_raffled && !round.is_user_inscripted) ? 'none' : '' }}
                            >
                              {(round.round_was_raffled && !round.is_user_inscripted) ? "Terminado" : "Ver"}
                            </Link>
                        }
                      </div>
                    </div>
                    <div className="card-footer text-muted">{new Date(round.evaluation.scheduled_day).toLocaleDateString() + " - " + new Date(round.evaluation.scheduled_day).toLocaleTimeString()}</div>
                  </div>
                  <br />
                </div>
              ))
            }
          </div>
        </div>

      </div>
    </>
  );
};
