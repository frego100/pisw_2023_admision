import React, { useState } from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { RegistrationForm } from './RegistrationForm';
import { StepContent } from '@mui/material';
import { Registered } from './Registered';
import { Link, useLocation } from 'react-router-dom';
import { Result } from './Result';

const steps = ['Inscripción', 'En proceso', 'Respuesta'];

export const StepperInscription = () => {
  const { state } = useLocation();
  const [activeStep, setActiveStep] = useState(state.indexStep);
  const [skipped, setSkipped] = useState(new Set());

  const isStepOptional = (step) => {
    return step === -1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <>
      <div className="bg-white mx-4 p-3 rounded">
        <br></br>
        <Box sx={{ width: '100%' }}>
          <Stepper activeStep={activeStep}>
            {steps.map((label, index) => {
              const stepProps = {};
              const labelProps = {};
              if (isStepSkipped(index)) {
                stepProps.completed = false;
              }
              return (
                <Step key={label} {...stepProps}>
                  <StepLabel {...labelProps}>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
          {activeStep === steps.length ? (''
          ) : (
            <>
              <br></br>
              {activeStep === 0 ?
                <div className='d-flex justify-content-center'>
                  <RegistrationForm handleNext={handleNext} />
                </div>
                : ''}
              {activeStep === 1 ?
                <div className='d-flex justify-content-center'>
                  <Registered handleBack={handleBack} />
                </div>
                : ''}
              {activeStep === 2 ?
                <div className='d-flex justify-content-center'>
                  <Result />
                </div>
                : ''}
              <div className='d-flex justify-content-center'>
                <Link
                  className="btn btn-secondary"
                  to="/teacher/registrations/"
                >
                  Volver
                </Link>
              </div>
            </>
          )}
        </Box>
      </div>
    </>
  );
};
