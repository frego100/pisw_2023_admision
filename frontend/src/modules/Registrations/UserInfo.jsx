import { useState } from "react";
import Modal from "react-modal";

import "./UserInfo.css";
import { useAuthStore } from "../../hooks";
import { useForm } from "react-hook-form";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    height: "550px"
  },
};

Modal.setAppElement("#root");

export const UserInfo = ({ isOpen, onCloseModal }) => {
  const { register, handleSubmit } = useForm();
  const { user, startUpdateProfile } = useAuthStore();
  const onSubmitEditProfile = async (data) => {
    const updatedProfile = {
      "user": user.id,
      "work": data.work
    }
    await startUpdateProfile(updatedProfile);
    onCloseModal();
  }
  return (
    <Modal
      isOpen={isOpen}
      //   onAfterOpen={afterOpenModal}
      onRequestClose={onCloseModal}
      style={customStyles}
      contentLabel="Example Modal"
      className="modal"
      overlayClassName="modal-fondo"
      closeTimeoutMS={200}
    >
      <h1 className="text-center text-primary">Perfil de Usuario</h1>
      <hr />

      <form onSubmit={handleSubmit(onSubmitEditProfile)} className="p-3">
        <div className="mb-3">
          <label className="form-label">Correo del Postulante</label>
          <input
            type="text"
            className="form-control"
            id="user-email"
            aria-describedby="emailHelp"
            value={user.email}
            disabled
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Rol del Postulante</label>
          <input
            type="text"
            className="form-control"
            id="user_role"
            aria-describedby="emailHelp"
            value={user.user_type === "TEACHER" && "DOCENTE"}
            disabled
          />
        </div>
        <div className="mb-3">
          <label className="form-label">DNI</label>
          <input
            type="text"
            className="form-control"
            id="user_dni"
            aria-describedby="emailHelp"
            disabled
            value={user.dni}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Trabajo</label>
          <textarea
            type="text"
            className="form-control"
            id="user-job"
            defaultValue={user.work}
            {...register("work")}
          ></textarea>
        </div>
        <div className="d-grid gap-2">
          <button className="btn btn-primary" type="submit">
            Guardar
          </button>
        </div>
      </form>
    </Modal>
  );
};
