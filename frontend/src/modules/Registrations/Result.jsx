import { Link, useLocation } from "react-router-dom"
import accepted from "../../assets/accepted.png"
import denied from "../../assets/denied.png"

export const Result = () => {
    const { state } = useLocation();
    const onViewCredential = (url) => {
        let urlPDF = "https://lavacalola.online" + url;
        window.open(urlPDF, "_blank");
    }
    return (
        <div className="w-100 h-100 d-flex justify-content-center align-items-center">
            <div className="card mb-3 p-3" style={{ "maxWidth": "640px" }}>
                <div className="row g-0">
                    <div className="col-md-4">
                        <img src={state.is_user_winner ? accepted : denied} className="img-fluid rounded-start" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body text-center">
                            <h5 className="card-title">Resultado</h5>
                            {
                                state.is_user_winner ?
                                    <>
                                        <div>
                                            <p className="card-text">Felicidades fue seleccionado.</p>
                                        </div>
                                        {
                                            state.is_user_with_credential ?
                                                <button className="btn btn-primary my-2" onClick={() => onViewCredential(state.credential.credential_file)}>Ver Credencial</button>
                                                :
                                                <button className="btn btn-warning my-2" disabled>Esperando asignación de cargo</button>
                                        }
                                    </>
                                    :
                                    <p className="card-text">Lo lamentamos, no fue seleccionado.</p>
                            }
                            <p className="card-text"><small className="text-body-secondary">Sorteo finalizado</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}