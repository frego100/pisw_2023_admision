import { Bounce } from 'react-toastify';
export const MessageConfig = () => {
    return {
        position: "bottom-center",
        autoClose: 200,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        transition: Bounce,
    }
}