import React, { useContext, useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";

import "./TeacherNavbar.css";
import icono from "../../../../assets/icono.png";
import unsa from "../../../../assets/unsa.png";
import { BiSolidSchool, BiLogOut } from "react-icons/bi";
import { BsPersonGear, BsFillCaretDownFill } from "react-icons/bs";
import { CgProfile } from "react-icons/cg"
import { useAuthStore } from "../../../../hooks/useAuthStore";
import { useRegistrationStore } from "../../../../hooks";

export const TeacherNavbar = ({ menu }) => {

  const { user, startLogout } = useAuthStore();
  const { startLogoutRegistration } = useRegistrationStore();
  const navigate = useNavigate()
  const onLogout = () => {
    startLogout();
    startLogoutRegistration();
    navigate("/auth/login");
  }

  const [menus, setMenus] = useState([
    {
      "id": 1,
      "value": ""
    },
    {
      "id": 2,
      "value": ""
    }
  ]);

  const handleMenus = (id) => {
    const newState = [...menus]
    if (newState[id - 1].value === "showMenu") {
      newState[id - 1] = {
        ...newState[id - 1],
        value: "",
      }
    } else {
      newState[id - 1] = {
        ...newState[id - 1],
        value: "showMenu",
      }
    }
    const stateAll = newState.map(e => {
      if (e.id !== id) {
        e.value = "";
      }
      return e;
    })
    setMenus(stateAll);
  }

  const [menuClose, setMenuClose] = useState('');

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth <= 768) {
        setMenuClose('close');
      } else {
        setMenuClose('');
      }
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <>
      <div className={`sidebar-teacher ${menu}`} id="sidebar">
        <br></br>
        <br></br>
        <div className="logo-details">
          <i className="">
            <img
              src={icono}
              className="icono"
            ></img>
          </i>
          <span className="logo_name">
            <img
              src={unsa}
              className="unsa"
            ></img>
          </span>
        </div>

        <ul className="nav-links">
          <li className={`${menus[0].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(1)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <BiSolidSchool
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Procesos</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/teacher/registrations">Ver Procesos Activos</Link>
            </ul>
          </li>
          <li className={`${menus[1].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(2)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <BsPersonGear
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Inscripciones</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/teacher/my-registrations">Ver Inscripciones</Link>
            </ul>
          </li>
          <br></br>
          <br></br>
          <li>
            <div className="profile-details">
              <div className="profile-content">
                <CgProfile className="img-profile"></CgProfile>
              </div>
              <div className="name-job">
                <div className="profile_name">
                  {user?.first_name.split(" ")[0]}
                </div>
                <div className="job">{user?.user_type === "TEACHER" && "Docente"}</div>
              </div>
              <BiLogOut className="img-logout" onClick={onLogout}></BiLogOut>
            </div>
          </li>
        </ul>
      </div>
    </>
  );
};
