import React, { useContext, useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";


// STYLES
import "./AdminNavbar.css";
import icono from "../../../../assets/icono.png";
import unsa from "../../../../assets/unsa.png";
import { BiSolidSchool, BiLogOut } from "react-icons/bi";
import { BsPersonGear, BsFillCaretDownFill } from "react-icons/bs";
import { CgProfile } from "react-icons/cg"
import { GiPerspectiveDiceSixFacesRandom } from "react-icons/gi";
import { TbFaceIdError } from "react-icons/tb";
import { BiSolidReport } from "react-icons/bi";
import { PiExamBold } from "react-icons/pi";
import { useAuthStore } from "../../../../hooks/useAuthStore";

export const AdminNavbar = ({ menu }) => {
  const { user, startLogout } = useAuthStore();
  const navigate = useNavigate();
  const onLogout = () => {
    startLogout();
    navigate("/auth/login");
  }

  const [menus, setMenus] = useState([
    {
      "id": 1,
      "value": ""
    },
    {
      "id": 2,
      "value": ""
    },
    {
      "id": 3,
      "value": ""
    },
    {
      "id": 4,
      "value": ""
    },
    {
      "id": 5,
      "value": ""
    },
    {
      "id": 6,
      "value": ""
    }
  ]);

  const handleMenus = (id) => {
    const newState = [...menus]
    if (newState[id - 1].value === "showMenu") {
      newState[id - 1] = {
        ...newState[id - 1],
        value: "",
      }
    } else {
      newState[id - 1] = {
        ...newState[id - 1],
        value: "showMenu",
      }
    }
    const stateAll = newState.map(e => {
      if (e.id !== id) {
        e.value = "";
      }
      return e;
    })
    setMenus(stateAll);
  }

  const [menuClose, setMenuClose] = useState('');

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth <= 768) {
        setMenuClose('close');
      } else {
        setMenuClose('');
      }
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);


  return (
    <>
      <div className={`sidebar-admin ${menu}`} id="sidebar">
        <br></br>
        <br></br>
        <div className="logo-details">
          <i className="">
            <img
              src={icono}
              className="icono"
            ></img>
          </i>
          <span className="logo_name">
            <img
              src={unsa}
              className="unsa"
            ></img>
          </span>
        </div>

        <ul className="nav-links">
          <li className={`${menus[0].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(1)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <BiSolidSchool
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Espacios</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/admin/spaces/classrooms">Aulas</Link>
              <Link to="/admin/spaces/pavillons">Pabellones</Link>
              <Link to="/admin/spaces/schools">Escuelas</Link>
              <Link to="/admin/spaces/entrances">Entradas</Link>
            </ul>
          </li>
          <li className={`${menus[1].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(2)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <BsPersonGear
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Personal</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/admin/staff/teachers">Docentes</Link>
            </ul>
          </li>
          <li className={`${menus[2].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(3)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <GiPerspectiveDiceSixFacesRandom
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Sorteos</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/admin/raffles">Ver Sorteos</Link>
            </ul>
          </li>
          <li className={`${menus[3].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(4)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <TbFaceIdError
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Incidencias</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/admin/incidents">Incidencias</Link>

              <Link to="/admin/incidents-teacher">Incidencias docentes</Link>

            </ul>
          </li>
          <li className={`${menus[5].value}`}>
            <div className="iocn-link" onClick={() => handleMenus(6)}>
              <a style={{ cursor: 'pointer' }}>
                <i>
                  <PiExamBold
                    className="icon-i"
                  />
                </i>
                <span className="link_name">Procesos</span>
              </a>
              <BsFillCaretDownFill
                className="arrow"
              />
            </div>
            <ul className="sub-menu">
              <Link to="/admin/registration-rounds">Rondas de incripciones</Link>
              <Link to="/admin/processes">Procesos</Link>
            </ul>
          </li>
          <br></br>
          <br></br>
          <li>
            <div className="profile-details">
              <div className="profile-content">
                <CgProfile className="img-profile"></CgProfile>
              </div>
              <div className="name-job">
                <div className="profile_name">
                  {user?.first_name.split(" ")[0]}
                </div>
                <div className="job">{user.user_type === "ADMIN" && "Administrador"}</div>
              </div>
              <BiLogOut className="img-logout" onClick={onLogout}></BiLogOut>
            </div>
          </li>
        </ul>
      </div>
    </>
  );
};
