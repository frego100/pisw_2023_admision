import axios from "axios";
import { getEnvVariables } from "../helpers";
const { VITE_API_URL } = getEnvVariables()

const admisionApi = axios.create({
    baseURL: VITE_API_URL,
})

admisionApi.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        'Authorization': `Bearer ${localStorage.getItem("x-token")}`
    }
    return config;
})

export default admisionApi;