import { Route, Routes } from "react-router-dom";
import { GeneralRoutes } from "../modules/routes/GeneralRoutes";
import { AuthRoutes } from "../auth/routes/AuthRoutes";
import { useAuthStore } from "../hooks/useAuthStore";

export const AppRouter = () => {
  return (
    <Routes>
      <Route path="/auth/*" element={<AuthRoutes />} />
      <Route path="/*" element={<GeneralRoutes />} />
    </Routes>
  );
};
