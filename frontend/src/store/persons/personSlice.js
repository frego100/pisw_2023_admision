import { createSlice } from "@reduxjs/toolkit";

export const personSlice = createSlice({
    name: 'person',
    initialState: {
        isLoadingTeachers: true,
        teachers: [],
        isLoadingPersons: true,
        persons: [],
        isLoadingPersonJobs: true,
        personJobs: []
    },
    reducers: {
        onAddNewPerson: (state, { payload }) => {
            state.persons.push(payload);
        },
        onAddNewPersonJob: (state, { payload }) => {
            state.personJobs.push(payload);
        },
        onDeletePerson: (state, { payload }) => {

        },
        onUpdatePerson: (state, { payload }) => {

        },
        onLoadTeachers: (state, { payload = [] }) => {
            state.isLoadingTeachers = false;
            payload.forEach(teacher => {
                const exists = state.teachers.some(dbTeacher => dbTeacher.id === teacher.id);
                if (!exists) {
                    state.teachers.push(teacher);
                }
            })
        },
        onLoadPersons: (state, { payload = [] }) => {
            state.isLoadingPersons = false;
            payload.forEach(person => {
                const exists = state.persons.some(dbPerson => dbPerson.id === person.id);
                if (!exists) {
                    state.persons.push(person);
                }
            })
        },
        onLoadPersonJobs: (state, { payload = [] }) => {
            state.isLoadingPersonJobs = false;
            payload.forEach(personJob => {
                const exists = state.personJobs.some(dbPersonJob => dbPersonJob.id === personJob.id);
                if (!exists) {
                    state.personJobs.push(personJob);
                }
            })
        },
        onLogoutPerson: (state) => {
            state.isLoadingPersons = true;
            state.persons = []
        }
    }
})

export const {
    onAddNewPerson,
    onAddNewPersonJob,
    onDeletePerson,
    onUpdatePerson,
    onLoadTeachers,
    onLoadPersons,
    onLoadPersonJobs,
    onLogoutPerson
} = personSlice.actions;