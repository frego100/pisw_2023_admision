import { createSlice } from "@reduxjs/toolkit";

export const raffleSlice = createSlice({
    name: "raffle",
    initialState: {
        isLoadingRaffles: true,
        raffles: [],
        isLoadingRaffleWinners: true,
        raffleWinners: [],
        currentRaffleWinner: { id: "" },
    },
    reducers: {
        onAddNewRaffle: (state, { payload }) => {
            state.raffles.push(payload);
        },
        onDeleteRaffle: (state, { payload }) => {
            state.raffles = state.raffles.filter(raffle => raffle.id !== payload.id);
        },
        onUpdateRaffle: (state, { payload }) => {
        },
        onLoadRaffles: (state, { payload = [] }) => {
            state.isLoadingRaffles = false;
            payload.forEach(raffle => {
                const exists = state.raffles.some(dbRaffle => dbRaffle.id === raffle.id);
                if (!exists) {
                    state.raffles.push(raffle);
                }
            })
        },
        onUpdateIsLoadingRaffleWinners: (state) => {
            state.isLoadingRaffleWinners = true;
        },
        onUpdateRaffleWinners: (state, { payload = {} }) => {
            state.raffleWinners = state.raffleWinners.map(raffleWinner => {
                if (payload.delete) {
                    if (raffleWinner.id === payload.raffle_winner) {
                        return {
                            ...raffleWinner,
                            "is_user_assigned": false,
                            "assignment": null
                        }
                    }
                    return raffleWinner
                } else {
                    if (raffleWinner.id === payload.id) {
                        return {
                            ...raffleWinner,
                            "is_user_assigned": true,
                            "assignment": payload.assignment
                        }
                    }
                    return raffleWinner
                }
            })
        },
        onLoadRaffleWinners: (state, { payload = [] }) => {
            state.isLoadingRaffleWinners = false;
            state.raffleWinners = payload;
        },
        onSelectCurrentRaffleWinner: (state, { payload = {} }) => {
            state.currentRaffleWinner = payload;
        },
        onLogoutRaffle: (state) => {
            state.isLoadingRaffles = true;
            state.raffles = [];
        }
    }
})

export const {
    onLoadRaffles,
    onAddNewRaffle,
    onDeleteRaffle,
    onUpdateRaffle,
    onLoadRaffleWinners,
    onUpdateRaffleWinners,
    onLogoutRaffle,
    onSelectCurrentRaffleWinner,
    onUpdateIsLoadingRaffleWinners
} = raffleSlice.actions;