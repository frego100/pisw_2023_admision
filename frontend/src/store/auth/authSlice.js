import { createSlice } from "@reduxjs/toolkit";

const DEFAULT_STATE = {
    status: 'not-authenticated', //'authenticated' 'not-authenticated'
    user: null,
    errorMessage: undefined
}

const initialState = (() => {
    const persistedState = localStorage.getItem("user");
    if (persistedState) {
        return JSON.parse(persistedState);
    }
    return DEFAULT_STATE;
})();

export const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        onUpdateProfile: (state, { payload }) => {
            state.user = {
                ...state.user,
                "work": payload.work
            };
            state.errorMessage = undefined;
        },
        onChecking: (state) => {
            state.status = 'checking';
            state.user = {};
            state.errorMessage = undefined;
        },
        onLogin: (state, { payload }) => {
            state.status = 'authenticated';
            state.user = payload;
            state.errorMessage = undefined;
        },
        onLogout: (state, { payload }) => {
            state.status = 'not-authenticated';
            state.user = null;
            state.errorMessage = payload;
        },
        clearErrorMessage: (state) => {
            state.errorMessage = undefined
        }
    },
});

export const {
    onUpdateProfile,
    onChecking,
    onLogin, onLogout,
    clearErrorMessage
} = authSlice.actions;