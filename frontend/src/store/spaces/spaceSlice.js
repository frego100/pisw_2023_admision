import { createSlice } from "@reduxjs/toolkit";


export const spaceSlice = createSlice({
    name: 'space',
    initialState: {
        isLoadingAreas: true,
        areas: [],
        isLoadingClassrooms: true,
        classrooms: [],
        currentClassroom: { name: "", description: "", pavilion: { school: { area: "" } }, columns: "", rows: "" },
        isLoadingEntrances: true,
        entrances: [],
        currentEntrance: { name: "", area: "", description: "" },
        isLoadingPavilions: true,
        pavilions: [],
        currentPavilion: { name: "", description: "", school: { area: "" } },
        isLoadingSchools: true,
        schools: [],
        currentSchool: { name: "", area: "", description: "" }
    },
    reducers: {
        onLoadAreas: (state, { payload = [] }) => {
            state.isLoadingAreas = false;
            payload.forEach(area => {
                const exists = state.areas.some(dbArea => dbArea.id === area.id);
                if (!exists) {
                    state.areas.push(area);
                }
            });
        },
        onAddNewClassroom: (state, { payload = {} }) => {
            state.classrooms.push(payload);
        },
        onUpdateClassroom: (state, { payload = {} }) => {
            state.classrooms = state.classrooms.map(classroom => {
                if (classroom.id === payload.id) {
                    return payload;
                }
                return classroom;
            })
        },
        onDeleteClassroom: (state, { payload }) => {
            state.classrooms = state.classrooms.filter(classroom => classroom.id !== payload.id);
        },
        onLoadClassrooms: (state, { payload = [] }) => {
            state.isLoadingClassrooms = false;
            payload.forEach(classroom => {
                const exists = state.classrooms.some(dbClassroom => dbClassroom.id === classroom.id);
                if (!exists) {
                    state.classrooms.push(classroom);
                }
            });
        },
        onAddNewEntrance: (state, { payload = {} }) => {
            state.entrances.push(payload);
        },
        onUpdateEntrance: (state, { payload = {} }) => {
            state.entrances = state.entrances.map(entrance => {
                if (entrance.id === payload.id) {
                    return payload;
                }
                return entrance;
            })
        },
        onDeleteEntrance: (state, { payload }) => {
            state.entrances = state.entrances.filter(entrance => entrance.id !== payload.id);
        },
        onLoadEntrances: (state, { payload = [] }) => {
            state.isLoadingEntrances = false;
            payload.forEach(entrance => {
                const exists = state.entrances.some(dbEntrance => dbEntrance.id === entrance.id);
                if (!exists) {
                    state.entrances.push(entrance);
                }
            });
        },
        onAddNewPavilion: (state, { payload = {} }) => {
            state.pavilions.push(payload);
        },
        onUpdatePavilion: (state, { payload = {} }) => {
            state.pavilions = state.pavilions.map(pavilion => {
                if (pavilion.id === payload.id) {
                    return payload;
                }
                return pavilion;
            })
        },
        onDeletePavilion: (state, { payload }) => {
            state.pavilions = state.pavilions.filter(paviltion => paviltion.id !== payload.id);
        },
        onLoadPavilions: (state, { payload = [] }) => {
            state.isLoadingPavilions = false;
            payload.forEach(pavilion => {
                const exists = state.pavilions.some(dbPavilion => dbPavilion.id === pavilion.id);
                if (!exists) {
                    state.pavilions.push(pavilion);
                }
            });
        },
        onAddNewSchool: (state, { payload = {} }) => {
            state.schools.push(payload);
        },
        onUpdateSchool: (state, { payload = {} }) => {
            state.schools = state.schools.map(school => {
                if (school.id === payload.id) {
                    return payload;
                }
                return school;
            })
        },
        onDeleteSchool: (state, { payload }) => {
            state.schools = state.schools.filter(school => school.id !== payload.id);
        },
        onLoadSchools: (state, { payload = [] }) => {
            state.isLoadingSchools = false;
            payload.forEach(school => {
                const exists = state.schools.some(dbSchool => dbSchool.id === school.id);
                if (!exists) {
                    state.schools.push(school);
                }
            });
        },
        onSelectCurrentClassroom: (state, { payload = {} }) => {
            state.currentClassroom = payload;
        },
        onSelectCurrentPavilion: (state, { payload = {} }) => {
            state.currentPavilion = payload;
        },
        onSelectCurrentSchool: (state, { payload = {} }) => {
            state.currentSchool = payload;
        },
        onSelectCurrentEntrance: (state, { payload = {} }) => {
            state.currentEntrance = payload;
        },

        onLogout: (state) => {
            state.isLoadingAreas = true;
            state.areas = [];
            state.isLoadingClassrooms = true;
            state.classrooms = [];
            state.isLoadingEntrances = true;
            state.entrances = [];
            state.isLoadingPavilions = true;
            state.pavilions = [];
            state.isLoadingSchools = true;
            state.schools = [];
        }
    }
})

export const {
    onLoadAreas,
    onAddNewClassroom,
    onUpdateClassroom,
    onDeleteClassroom,
    onLoadClassrooms,
    onAddNewEntrance,
    onUpdateEntrance,
    onDeleteEntrance,
    onLoadEntrances,
    onAddNewPavilion,
    onUpdatePavilion,
    onDeletePavilion,
    onLoadPavilions,
    onAddNewSchool,
    onUpdateSchool,
    onDeleteSchool,
    onLoadSchools,
    onSelectCurrentClassroom,
    onSelectCurrentPavilion,
    onSelectCurrentSchool,
    onSelectCurrentEntrance
} = spaceSlice.actions;