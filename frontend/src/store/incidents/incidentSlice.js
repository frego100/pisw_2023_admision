import { createSlice } from "@reduxjs/toolkit";

export const incidentSlice = createSlice({
    name: 'incident',
    initialState: {
        isLoadingIncidents: true,
        incidents: [],
        currentIncident: { name: "", description: "", level: "" },
        isLoadingTeacherIncidents: true,
        teacherIncidents: [],
        currentTeacherIncident: { name: "", description: "", level: "" }
    },
    reducers: {
        onAddNewIncident: (state, { payload }) => {
            state.incidents.push(payload);
        },
        onAddNewTeacherIncident: (state, { payload }) => {
            state.teacherIncidents.push(payload);
        },
        onDeleteIncident: (state, { payload }) => {
            state.incidents = state.incidents.filter(incident => incident.id !== payload.id)
        },
        onDeleteTeacherIncident: (state, { payload }) => {
            state.teacherIncidents = state.teacherIncidents.filter(teacherIncident => teacherIncident.id !== payload.id)
        },
        onUpdateIncident: (state, { payload = {} }) => {
            state.incidents = state.incidents.map(incident => {
                if (incident.id === payload.id) {
                    return payload;
                }
                return incident;
            })
        },
        onUpdateTeacherIncident: (state, { payload = {} }) => {
            state.teacherIncidents = state.teacherIncidents.map(teacherIncident => {
                if (teacherIncident.id === payload.id) {
                    return payload;
                }
                return teacherIncident;
            })
        },
        onLoadIncidents: (state, { payload = [] }) => {
            state.isLoadingIncidents = false;
            payload.forEach(incident => {
                const exists = state.incidents.some(dbIncident => dbIncident.id === incident.id);
                if (!exists) {
                    state.incidents.push(incident);
                }
            })
        },
        onLoadTeacherIncidents: (state, { payload = [] }) => {
            state.isLoadingTeacherIncidents = false;
            payload.forEach(teacherIncident => {
                const exists = state.teacherIncidents.some(dbTeacherIncident => dbTeacherIncident.id === teacherIncident.id);
                if (!exists) {
                    state.teacherIncidents.push(teacherIncident);
                }
            })
        },
        onLogoutIncident: (state) => {
            state.isLoadingIncidents = true;
            state.incidents = [];
        },
        onSelectCurrentIncident: (state, { payload = {} }) => {
            state.currentIncident = payload;
        },
        onSelectCurrentTeacherIncident: (state, { payload = {} }) => {
            state.currentTeacherIncident = payload;
        }
    }
})

export const {
    onAddNewIncident,
    onAddNewTeacherIncident,
    onDeleteIncident,
    onDeleteTeacherIncident,
    onUpdateIncident,
    onUpdateTeacherIncident,
    onLoadIncidents,
    onLoadTeacherIncidents,
    onLogoutIncident,
    onSelectCurrentIncident,
    onSelectCurrentTeacherIncident
} = incidentSlice.actions;