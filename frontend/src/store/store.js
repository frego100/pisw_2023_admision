import { configureStore } from "@reduxjs/toolkit";
import { authSlice, evaluationSlice, incidentSlice, personSlice, raffleSlice, registrationSlice, roundSlice, spaceSlice, assignmentSlice } from "./";

const persistanceLocalStorageMiddleware = (store) => (next) => (action) => {
    next(action);
    const nextState = store.getState();
    localStorage.setItem("user", JSON.stringify(nextState.auth));
    // localStorage.setItem("_user_", JSON.stringify(store.getState()));
}

export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        incident: incidentSlice.reducer,
        person: personSlice.reducer,
        raffle: raffleSlice.reducer,
        evaluation: evaluationSlice.reducer,
        registration: registrationSlice.reducer,
        round: roundSlice.reducer,
        space: spaceSlice.reducer,
        assignment: assignmentSlice.reducer,
    },
    middleware: [persistanceLocalStorageMiddleware],
});