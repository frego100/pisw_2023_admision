import { createSlice } from "@reduxjs/toolkit";


export const assignmentSlice = createSlice({
    name: 'assignment',
    initialState: {
        isLoadingCoordinations: true,
        coordinations: [], 
    },
    reducers: {
        onAddNewCoordination: (state, { payload = {} }) => {
            state.coordinations.push(payload);
        },
        onUpdateCoordination: (state, { payload = {} }) => {
            state.coordinations = state.coordinations.map(coordination => {
                if (coordination.id === coordination.id) {
                    return payload;
                }
                return coordination;
            })
        },
        onDeleteCoordination: (state, { payload }) => {
            state.coordinations = state.coordinations.filter(coordination => coordination.id !== payload.id);
        },
        onLoadCoordinations: (state, { payload = [] }) => {
            state.isLoadingCoordinations = false;
            payload.forEach(coordination => {
                const exists = state.coordinations.some(dbCoordination => dbCoordination.id === coordination.id);
                if (!exists) {
                    state.coordinations.push(coordination);
                }
            });
        },
        
        onLogout: (state) => {
            state.isLoadingCoordinations = true;
            state.coordinations = [];
        }
    }
})

export const {
    onLoadCoordinations,
    onAddNewCoordination,
    onUpdateCoordination,
    onDeleteCoordination,
} = assignmentSlice.actions;