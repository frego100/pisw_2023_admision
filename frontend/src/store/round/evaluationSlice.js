import { createSlice } from "@reduxjs/toolkit";

export const evaluationSlice = createSlice({
    name: "evaluation",
    initialState: {
        isLoadingAdmissionProcesses: true,
        admissionProcesses: [],
        isLoadingExams: true,
        exams: [],
        isLoadingEvaluations: true,
        evaluations: [],
        currentEvaluation: { number: "", type: "", exam: "", scheduled_day: "" }
    },
    reducers: {
        onAddNewAdmissionProcess: (state, { payload = {} }) => {
            state.admissionProcesses.push(payload);
        },
        onDeleteAdmissionProcess: (state, { payload }) => {
            state.admissionProcesses = state.admissionProcesses.filter(admissionProcess => admissionProcess.id !== payload.id)
        },
        onLoadAdmissionProcesses: (state, { payload = [] }) => {
            state.isLoadingAdmissionProcesses = false;
            payload.forEach(admissionProcess => {
                const exists = state.admissionProcesses.some(dbAdmissionProcess => dbAdmissionProcess.id === admissionProcess.id);
                if (!exists) {
                    state.admissionProcesses.push(admissionProcess);
                }
            })
        },
        onAddNewExam: (state, { payload = {} }) => {
            state.exams.push(payload);
        },
        onDeleteExam: (state, { payload }) => {
            state.exams = state.exams.filter(exam => exam.id !== payload.id);
        },
        onLoadExams: (state, { payload = [] }) => {
            state.isLoadingExams = false;
            payload.forEach(exam => {
                const exists = state.exams.some(dbExam => dbExam.id === exam.id);
                if (!exists) {
                    state.exams.push(exam);
                }
            })
        },
        onAddNewEvaluation: (state, { payload = {} }) => {
            state.evaluations.push(payload);
        },
        onUpdateEvaluation: (state, { payload = {} }) => {
            state.evaluations = state.evaluations.map(evaluation => {
                if (evaluation.id === payload.id) {
                    return payload;
                }
                return evaluation;
            })
        },
        onDeleteEvaluation: (state, { payload }) => {
            state.evaluations = state.evaluations.filter(evaluation => evaluation.id !== payload.id)
        },
        onLoadEvaluations: (state, { payload = [] }) => {
            state.isLoadingEvaluations = false;
            payload.forEach(evaluation => {
                const exists = state.evaluations.some(dbEvaluation => dbEvaluation.id === evaluation.id);
                if (!exists) {
                    state.evaluations.push(evaluation);
                }
            });
        },
        onSelectCurrentEvaluation: (state, { payload = {} }) => {
            state.currentEvaluation = payload;
        },
        onLogoutEvaluation: (state) => {
            state.isLoadingEvaluations = true;
            state.evaluations = [];
        }
    }
})

export const {
    onAddNewEvaluation,
    onUpdateEvaluation,
    onDeleteEvaluation,
    onLoadEvaluations,
    onAddNewAdmissionProcess,
    onDeleteAdmissionProcess,
    onLoadAdmissionProcesses,
    onAddNewExam,
    onDeleteExam,
    onLoadExams,
    onSelectCurrentEvaluation,
    onLogoutEvaluation
} = evaluationSlice.actions;