import { createSlice } from "@reduxjs/toolkit";

export const registrationSlice = createSlice({
    name: "registration",
    initialState: {
        isLoadingOpenRounds: true,
        openRounds: [],
        isLoadingRegistrations: true,
        registrations: [],
        currentRegistration: {}
    },
    reducers: {
        onLoadOpenRounds: (state, { payload = [] }) => {
            state.isLoadingOpenRounds = false;
            payload.forEach(openRound => {
                const exists = state.openRounds.some(dbOpenRound => dbOpenRound.id === openRound.id);
                if (!exists) {
                    state.openRounds.push(openRound);
                }
            })
        },
        onUpdateOpenRound: (state, { payload }) => {
            state.openRounds = state.openRounds.map(openRound => {
                if (openRound.id === payload.id) {
                    return {
                        ...openRound,
                        "is_user_inscripted": payload.is_user_inscripted
                    }
                }
                return openRound;
            })
        },
        onAddNewRegistration: (state, { payload }) => {
            state.registrations.push(payload);
        },
        onDeleteRegistration: (state, { payload }) => {
            state.registrations = state.registrations.filter(registration => registration.id !== payload.id)
        },
        onLoadRegistrations: (state, { payload = [] }) => {
            state.isLoadingRegistrations = false;
            payload.forEach(registration => {
                const exists = state.registrations.some(dbRegistration => dbRegistration.id === registration.id);
                if (!exists) {
                    state.registrations.push(registration);
                }
            })
        },
        onSelectCurrentRegistration: (state, { payload = {} }) => {
            state.currentRegistration = payload;
        },
        onLogoutRegistration: (state) => {
            state.isLoadingOpenRounds = true;
            state.openRounds = [];
            state.isLoadingRegistrations = true;
            state.registrations = [];
        }
    }
})

export const {
    onLoadOpenRounds,
    onUpdateOpenRound,
    onAddNewRegistration,
    onDeleteRegistration,
    onLoadRegistrations,
    onSelectCurrentRegistration,
    onLogoutRegistration
} = registrationSlice.actions;