import { createSlice } from "@reduxjs/toolkit";

export const roundSlice = createSlice({
    name: "round",
    initialState: {
        isLoadingRounds: true,
        rounds: []
    },
    reducers: {
        onAddNewRound: (state, { payload }) => {
            state.rounds.push(payload);
        },
        onUpdateRound: (state, { payload }) => {
            state.rounds = state.rounds.map(round => {
                if (round.id === payload.id && payload.other) {
                    return {
                        ...round,
                        "round_was_raffled": payload.round_was_raffled,
                        "is_blocked": payload.is_blocked
                    }
                } else if (round.id === payload.id) {

                    return payload;
                }
                return round;
            })
        },
        onDeleteRound: (state, { payload }) => {
            state.rounds = state.rounds.filter(round => round.id !== payload.id)
        },
        onLoadRounds: (state, { payload = [] }) => {
            state.isLoadingRounds = false;
            payload.forEach(round => {
                const exists = state.rounds.some(dbRound => dbRound.id === round.id);
                if (!exists) {
                    state.rounds.push(round);
                }
            });
        },
        onLogoutRound: (state) => {
            state.isLoadingRounds = true;
            state.rounds = [];
        }
    }
})

export const {
    onAddNewRound,
    onUpdateRound,
    onDeleteRound,
    onLoadRounds,
    onLogoutRound
} = roundSlice.actions;