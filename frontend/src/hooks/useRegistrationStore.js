import { useDispatch, useSelector } from "react-redux"
import admisionApi from "../api/admisionApi";
import { onAddNewRegistration, onDeleteRegistration, onLoadOpenRounds, onLoadRegistrations, onLogoutRegistration, onSelectCurrentRegistration, onUpdateOpenRound } from "../store";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useRegistrationStore = () => {
    const dispatch = useDispatch()
    const { registrations, openRounds, currentRegistration } = useSelector((state) => state.registration);

    const startLoadingOpenRounds = async () => {
        try {
            const { data } = await admisionApi.get('/roundinscription/roundinscription/');
            dispatch(onLoadOpenRounds(data));
        } catch (error) {
            toast.error('Error al cargar rondas.', MessageConfig);
        }
    }
    const startSavingRegistration = async (registration) => {
        try {
            const { data } = await admisionApi.post('/roundinscription/userinscription/', registration);
            const { data: round } = await admisionApi.get(`/roundinscription/roundinscription/${data.round_inscription.id}/`)
            dispatch(onAddNewRegistration(data));
            dispatch(onUpdateOpenRound(round));
            toast.success('Registro procesado.', MessageConfig);
        } catch (error) {
            toast.error('Error al guardar inscripción.', MessageConfig);
        }
    }
    const startDeletingRegistration = async (registration) => {
        try {
            await admisionApi.delete(`/roundinscription/userinscription/${registration.id}/`);
            const { data: round } = await admisionApi.get(`/roundinscription/roundinscription/${registration.round_inscription.id}/`)
            dispatch(onDeleteRegistration(registration));
            dispatch(onUpdateOpenRound(round));
            toast.success('Incripción eliminada', MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar inscripción', MessageConfig)
        }
    }
    const startLoadingRegistrations = async () => {
        try {
            const { data } = await admisionApi.get('/roundinscription/userinscription/');
            dispatch(onLoadRegistrations(data));
        } catch (error) {
            toast.error('Error al cargar las incripciones.', MessageConfig);
        }
    }

    const setCurrentRegistration = (registration) => {
        dispatch(onSelectCurrentRegistration(registration));
    }

    const startLogoutRegistration = () => {
        dispatch(onLogoutRegistration());
    }


    return {
        openRounds,
        registrations,
        currentRegistration,
        startLoadingOpenRounds,
        startSavingRegistration,
        startDeletingRegistration,
        setCurrentRegistration,
        startLoadingRegistrations,
        startLogoutRegistration
    }
}