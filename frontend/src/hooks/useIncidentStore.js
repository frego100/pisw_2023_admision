import { useDispatch, useSelector } from "react-redux"
import admisionApi from "../api/admisionApi";
import { onAddNewIncident, onAddNewTeacherIncident, onDeleteIncident, onDeleteTeacherIncident, onLoadIncidents, onLoadTeacherIncidents, onSelectCurrentIncident, onSelectCurrentTeacherIncident, onUpdateIncident, onUpdateTeacherIncident } from "../store";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useIncidentStore = () => {
    const dispatch = useDispatch();
    const { incidents, teacherIncidents, currentIncident, currentTeacherIncident } = useSelector((state) => state.incident);

    const startLoadingIncidents = async () => {
        try {
            const { data } = await admisionApi.get("/incident/incident/");
            dispatch(onLoadIncidents(data));
        } catch (error) {
            toast.error('Error al cargar incidentes.', MessageConfig);
        }
    }
    const startLoadingTeacherIncidents = async () => {
        try {
            const { data } = await admisionApi.get("/incident/userincident/");
            dispatch(onLoadTeacherIncidents(data));
        } catch (error) {
            toast.error('Error al cargar incidentes de docentes.', MessageConfig);
        }
    }
    const startExportTeacherIncidents = async () => {
        try {
            const response = await admisionApi.get("/incident/userincident/?export_excel=true",
                { 
                    responseType: 'blob'
                }
            );
            const contentDisposition = response.headers['content-disposition'];
            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = filenameRegex.exec(contentDisposition);
            const fileName = matches && matches[1] ? matches[1].replace(/['"]/g, '') : 'incident_report.xlsx';

            const blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();

        } catch (error) {
            toast.error('Error al exportar incidentes de docentes.', MessageConfig);
        }
    }
    const startSavingIncident = async (incident) => {
        try {
            const { data } = await admisionApi.post("/incident/incident/", incident, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            });
            dispatch(onAddNewIncident(data));
            toast.success('Incidencia creada.', MessageConfig);
        } catch (error) {
            toast.error('Error al crear incidencia.', MessageConfig);
        }
    }
    const startSavingTeacherIncident = async (teacherIncident) => {
        try {
            const { data } = await admisionApi.post("/incident/userincident/", teacherIncident, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            dispatch(onAddNewTeacherIncident(data));
            toast.success('Incidencia docente creada.', MessageConfig);
        } catch (error) {
            toast.error('Error al crear incidencia de docente.', MessageConfig);
        }
    }
    const startDeletingIncident = async (incident) => {
        try {
            await admisionApi.delete(`/incident/incident/${incident.id}/`)
            dispatch(onDeleteIncident(incident));
            toast.succcess('Incidencia eliminada.', MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar incidencia.', MessageConfig)
        }
    }
    const startDeletingTeacherIncident = async (teacherIncident) => {
        try {
            await admisionApi.delete(`/incident/userincident/${teacherIncident.id}/`);
            dispatch(onDeleteTeacherIncident(teacherIncident));
            toast.success('Incidencia docente eliminada.', MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar incidencia de docente.', MessageConfig)
        }
    }
    const startUpdatingIncident = async (incident) => {
        try {
            const { data } = await admisionApi.put(`/incident/incident/${incident.id}/`, incident);
            dispatch(onUpdateIncident(data));
            toast.success('Incidencia modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar incidencia.', MessageConfig);
        }
    }
    const startUpdatingTeacherIncident = async (teacherIncident) => {
        try {
            const { data } = await admisionApi.put(`/incident/incident/${teacherIncident.id}/`, teacherIncident)
            dispatch(onUpdateTeacherIncident(data));
            toast.success('Incidencia docente modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar incidencia de docente.', MessageConfig);
        }
    }
    const setCurrentIncident = (incident) => {
        dispatch(onSelectCurrentIncident(incident));
    }
    const setCurrentTeacherIncident = (teacherIncident) => {
        dispatch(onSelectCurrentTeacherIncident(teacherIncident));
    }

    return {
        incidents,
        teacherIncidents,
        currentIncident,
        currentTeacherIncident,
        startLoadingIncidents,
        startLoadingTeacherIncidents,
        startExportTeacherIncidents,
        startSavingIncident,
        startSavingTeacherIncident,
        startDeletingIncident,
        startDeletingTeacherIncident,
        startUpdatingIncident,
        startUpdatingTeacherIncident,
        setCurrentIncident,
        setCurrentTeacherIncident
    }
}