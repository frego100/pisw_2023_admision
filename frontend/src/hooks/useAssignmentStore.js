import { useDispatch, useSelector } from "react-redux";
import { onAddNewCoordination, onUpdateCoordination, onDeleteCoordination, onLoadCoordinations, onUpdateRaffleWinners } from "../store";
import admisionApi from "../api/admisionApi";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useAssignmentStore = () => {
    const dispatch = useDispatch();
    const { coordinations } = useSelector((state) => state.assignment);

    const startSavingCoordination = async (coordination) => {
        try {
            const { data } = await admisionApi.post('/coordination/', coordination);
            dispatch(onAddNewCoordination(data));
            toast.success("Coordinación creada.", MessageConfig)
        } catch (error) {
            toast.error('Error al crear la Coordinación.', MessageConfig);
        }
    }
    const startDeletingCoordination = async (coordination) => {
        try {
            await admisionApi.delete(`/coordination/${coordination.id}/`);
            dispatch(onDeleteCoordination(coordination));
            toast.success("Coordinación eliminada.", MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar la Coordinación.', MessageConfig);
        }
    }
    const startUpdatingCoordination = async (coordination) => {
        try {
            const { data } = await admisionApi.put(`/coordination/${coordination.id}/`, coordination);
            dispatch(onUpdateCoordination(data));
            toast.success('Coordinación modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar la Coordinación.', MessageConfig);
        }
    }
    const startLoadingCoordinations = async () => {
        try {
            const { data } = await admisionApi.get('/coordination/');
            dispatch(onLoadCoordinations(data));
        } catch (error) {
            toast.error('Error al cargar las Coordinaciones.', MessageConfig);
        }
    }
    const startSavingAssignment = async (asignment) => {
        try {
            const { data } = await admisionApi.post('/rafflewinnerassignment/', asignment);
            dispatch(onUpdateRaffleWinners(data.raffle_winner));
            toast.success("Asignación creada.", MessageConfig)
        } catch (error) {
            toast.error('Error al crear la Asignación.', MessageConfig);
        }
    }
    const startDeletingAssignment = async (assignment) => {
        try {
            await admisionApi.delete(`/rafflewinnerassignment/${assignment.id}/`);
            dispatch(onUpdateRaffleWinners({ raffle_winner: assignment.raffle_winner, delete: true }));
            toast.success("Asignación eliminada.", MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar Asignación.', MessageConfig);
        }
    }

    return {
        coordinations,
        startLoadingCoordinations,
        startSavingCoordination,
        startDeletingCoordination,
        startUpdatingCoordination,
        startSavingAssignment,
        startDeletingAssignment
    }
}