import { useDispatch, useSelector } from "react-redux"
import admisionApi from "../api/admisionApi";
import { onLoadPersonJobs, onLoadPersons, onLoadTeachers } from "../store";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const usePersonStore = () => {
    const dispatch = useDispatch();
    const { teachers, persons, personJobs } = useSelector((state) => state.person);

    const startLoadingTeachers = async () => {
        try {
            const { data } = await admisionApi.get("/user/user/?user_type=TEACHER");
            dispatch(onLoadTeachers(data));
        } catch (error) {
            toast.error('Error al cargar docentes.', MessageConfig);
        }
    }
    const startLoadingPersons = async () => {
        try {
            const { data } = await admisionApi.get("/");
        } catch (error) {
            toast.error('Error al cargar personas.', MessageConfig);
        }
    }
    const startLoadingPersonJobs = async () => {
        try {
            const { data } = await admisionApi.get("/");
        } catch (error) {
            toast.error('Error al cargar trabajos de personas.', MessageConfig);
        }
    }

    return {
        teachers,
        persons,
        personJobs,
        startLoadingTeachers,
        startLoadingPersons,
        startLoadingPersonJobs
    }
}