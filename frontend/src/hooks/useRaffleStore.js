import { useDispatch, useSelector } from "react-redux"
import admisionApi from "../api/admisionApi";
import { onAddNewRaffle, onDeleteRaffle, onLoadRaffleWinners, onLoadRaffles, onUpdateIsLoadingRaffleWinners, onUpdateRaffle, onUpdateRound, onSelectCurrentRaffleWinner } from "../store";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useRaffleStore = () => {
    const dispatch = useDispatch();
    const { raffles, raffleWinners, currentRaffleWinner } = useSelector((state) => state.raffle);
    const startSavingRaffle = async (raffle) => {
        try {
            const { data } = await admisionApi.post("/raffle/raffle/", raffle);
            dispatch(onAddNewRaffle(data));
            await admisionApi.patch(`/roundinscription/roundinscription/${raffle.round_inscription}/`, { "is_blocked": true });
            dispatch(onUpdateRound({ "id": data.round_inscription.id, "round_was_raffled": true, "is_blocked": true, "other": true }));
            toast.success('Sorteo creado.', MessageConfig);
        } catch (error) {
            toast.error('Error al guardar el sorteo.', MessageConfig);
        }
    }
    const startDeletingRaffle = async (raffle) => {
        try {
            await admisionApi.delete(`/raffle/raffle/${raffle.id}/`);
            dispatch(onDeleteRaffle(raffle));
            await admisionApi.patch(`/roundinscription/roundinscription/${raffle.round_inscription.id}/`, { "is_blocked": false });
            dispatch(onUpdateRound({ "id": raffle.round_inscription.id, "round_was_raffled": false, "is_blocked": false, "other": true }));
            toast.success("Sotero eliminado.", MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar sorteo.', MessageConfig)
        }
    }
    const startLoadingRaffles = async () => {
        try {
            const { data } = await admisionApi.get("/raffle/raffle/");
            dispatch(onLoadRaffles(data));
        } catch (error) {
            toast.error('Error al cargar los sorteos.', MessageConfig);
        }
    }
    const startLoadingRaffleWinners = async (raffle) => {
        dispatch(onUpdateIsLoadingRaffleWinners());
        try {
            const { data } = await admisionApi.get("/raffle/rafflewinner/", {
                params: {
                    "raffle": raffle.id
                }
            });
            dispatch(onLoadRaffleWinners(data.filter(winner => winner.is_active === true)));
        } catch (error) {
            toast.error('Error cargando ganadores de sorteo.', MessageConfig);
        }
    }
    const startExportRaffleWinners = async (raffle) => {
        try {

            const response = await admisionApi.get('/rafflewinnerassignment/?export_excel=true&raffle_winner__raffle=' + raffle.id,
                {
                    responseType: 'blob'
                }
            );
            const contentDisposition = response.headers['content-disposition'];
            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = filenameRegex.exec(contentDisposition);
            const fileName = matches && matches[1] ? matches[1].replace(/['"]/g, '') : 'assignment_report.xlsx';

            const blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();

        } catch (error) {
            toast.error('Error al exportar incidentes de docentes.', MessageConfig);
        }
    }
    const startLoadingRaffleWinnersById = async (id) => {
        dispatch(onUpdateIsLoadingRaffleWinners());
        try {
            const { data } = await admisionApi.get("/raffle/rafflewinner/", {
                params: {
                    "raffle": id
                }
            });
            dispatch(onLoadRaffleWinners(data.filter(winner => winner.is_active === true)));
        } catch (error) {
            toast.error('Error cargando ganadores de sorteo.', MessageConfig);
        }
    }
    const setCurrentRaffleWinner = (raffleWinner) => {
        dispatch(onSelectCurrentRaffleWinner(raffleWinner));
    }

    return {
        raffles,
        raffleWinners,
        currentRaffleWinner,
        startLoadingRaffles,
        startDeletingRaffle,
        startSavingRaffle,
        startLoadingRaffleWinners,
        startLoadingRaffleWinnersById,
        setCurrentRaffleWinner,
        startExportRaffleWinners
    }
}