import { useDispatch, useSelector } from "react-redux"

import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";
import admisionApi from "../api/admisionApi";
import { onAddNewClassroom, onAddNewEntrance, onAddNewPavilion, onAddNewSchool, onDeleteClassroom, onDeleteEntrance, onDeletePavilion, onDeleteSchool, onLoadAreas, onLoadClassrooms, onLoadEntrances, onLoadPavilions, onLoadSchools, onSelectCurrentClassroom, onSelectCurrentEntrance, onSelectCurrentPavilion, onSelectCurrentSchool, onUpdateClassroom, onUpdateEntrance, onUpdatePavilion, onUpdateSchool } from "../store";

export const useSpaceStore = () => {
    const dispatch = useDispatch();
    const { areas, classrooms, entrances, pavilions, schools, currentClassroom, currentPavilion, currentSchool, currentEntrance } = useSelector((state) => state.space);

    const startLoadingAreas = async () => {
        try {
            const { data } = await admisionApi.get('/area/');
            dispatch(onLoadAreas(data));
        } catch (error) {
            toast.error("Error cargando los areas.", MessageConfig);
        }
    }
    const startSavingClassroom = async (classroom) => {
        try {
            const { data } = await admisionApi.post('/classroom/', classroom);
            dispatch(onAddNewClassroom(data));
            toast.success("Aula creada.", MessageConfig);
        } catch (error) {
            toast.error("Error al guardar aula.", MessageConfig);
        }
    }
    const startDeletingClassroom = async (classroom) => {
        try {
            await admisionApi.delete(`/classroom/${classroom.id}/`);
            dispatch(onDeleteClassroom(classroom));
            toast.success("Aula eliminada.", MessageConfig);
        } catch (error) {
            toast.error("Error al eliminar aula.", MessageConfig);
        }
    }
    const startUpdatingClassroom = async (classroom) => {
        try {
            const { data } = await admisionApi.put(`/classroom/${classroom.id}/`, classroom);
            dispatch(onUpdateClassroom(data));
            toast.success('Aula modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar aula.', MessageConfig);
        }
    }
    const startLoadingClassrooms = async () => {
        try {
            const { data } = await admisionApi.get('/classroom/');
            dispatch(onLoadClassrooms(data));
        } catch (error) {
            toast.error("Error al cargar aulas.", MessageConfig);
        }
    }
    const startSavingEntrance = async (entrance) => {
        try {
            const { data } = await admisionApi.post('/entrance/', entrance);
            dispatch(onAddNewEntrance(data));
            toast.success("Entrada creada.", MessageConfig);
        } catch (error) {
            toast.error("Error al guardar entrada.", MessageConfig);
        }
    }
    const startDeletingEntrance = async (entrance) => {
        try {
            await admisionApi.delete(`/entrance/${entrance.id}/`);
            dispatch(onDeleteEntrance(entrance));
            toast.success("Entrada eliminada.", MessageConfig);
        } catch (error) {
            toast.error("Error al eliminar entrada.", MessageConfig);
        }
    }
    const startUpdatingEntrance = async (entrance) => {
        try {
            const { data } = await admisionApi.put(`/entrance/${entrance.id}/`, entrance);
            dispatch(onUpdateEntrance(data));
            toast.success('Entrada modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar entrada.', MessageConfig);
        }
    }
    const startLoadingEntrances = async () => {
        try {
            const { data } = await admisionApi.get('/entrance/');
            dispatch(onLoadEntrances(data));
        } catch (error) {
            toast.error("Error al cargar entradas.", MessageConfig);
        }
    }
    const startSavingPavilion = async (pavilion) => {
        try {
            const { data } = await admisionApi.post('/pavilion/', pavilion);
            dispatch(onAddNewPavilion(data));
            toast.success("Pabellón creado.", MessageConfig);
        } catch (error) {
            toast.error("Error al guardar pabellón.", MessageConfig);
        }
    }
    const startDeletingPavilion = async (pavilion) => {
        try {
            await admisionApi.delete(`/pavilion/${pavilion.id}/`);
            dispatch(onDeletePavilion(pavilion));
            toast.success("Pabellón eliminado.", MessageConfig);
        } catch (error) {
            toast.error("Error al eliminar paabellón.", MessageConfig);
        }
    }
    const startUpdatingPavilion = async (pavilion) => {
        try {
            const { data } = await admisionApi.put(`/pavilion/${pavilion.id}/`, pavilion);
            dispatch(onUpdatePavilion(data));
            toast.success('Pabellón modificado.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar pabellón.', MessageConfig);
        }
    }
    const startLoadingPavilions = async () => {
        try {
            const { data } = await admisionApi.get('/pavilion/');
            dispatch(onLoadPavilions(data));
        } catch (error) {
            toast.error("Error al cargar pabellones.", MessageConfig);
        }
    }
    const startSavingSchool = async (school) => {
        try {
            const { data } = await admisionApi.post('/school/', school);
            dispatch(onAddNewSchool(data));
            toast.success("Escuela creada.", MessageConfig);
        } catch (error) {
            toast.error("Error al guardar escuela.", MessageConfig);
        }
    }
    const startDeletingSchool = async (school) => {
        try {
            await admisionApi.delete(`/school/${school.id}/`);
            dispatch(onDeleteSchool(school));
            toast.success("EScuela eliminada.", MessageConfig);
        } catch (error) {
            toast.error("Error al eliminar escuela.", MessageConfig);
        }
    }
    const startUpdatingSchool = async (school) => {
        try {
            const { data } = await admisionApi.put(`/school/${school.id}/`, school);
            dispatch(onUpdateSchool(data));
            toast.success('Escuela modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar escuela.', MessageConfig);
        }
    }
    const startLoadingSchools = async () => {
        try {
            const { data } = await admisionApi.get('/school/');
            dispatch(onLoadSchools(data));
        } catch (error) {
            toast.error("Error al cargar escuelas.", MessageConfig);
        }
    }

    const setCurrentClassroom = (classroom) => {
        dispatch(onSelectCurrentClassroom(classroom));
    }
    const setCurrentPavilion = (pavilion) => {
        dispatch(onSelectCurrentPavilion(pavilion));
    }
    const setCurrentSchool = (school) => {
        dispatch(onSelectCurrentSchool(school));
    }
    const setCurrentEntrance = (entrance) => {
        dispatch(onSelectCurrentEntrance(entrance));
    }

    return {
        areas,
        classrooms,
        entrances,
        pavilions,
        schools,
        currentClassroom,
        currentPavilion,
        currentSchool,
        currentEntrance,
        startLoadingAreas,
        startSavingClassroom,
        startDeletingClassroom,
        startUpdatingClassroom,
        startLoadingClassrooms,
        startSavingEntrance,
        startDeletingEntrance,
        startUpdatingEntrance,
        startLoadingEntrances,
        startSavingPavilion,
        startDeletingPavilion,
        startUpdatingPavilion,
        startLoadingPavilions,
        startSavingSchool,
        startDeletingSchool,
        startUpdatingSchool,
        startLoadingSchools,
        setCurrentClassroom,
        setCurrentPavilion,
        setCurrentSchool,
        setCurrentEntrance
    }
}