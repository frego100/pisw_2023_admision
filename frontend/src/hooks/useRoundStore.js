import { useDispatch, useSelector } from "react-redux";
import { onAddNewRound, onUpdateRound, onDeleteRound, onLoadRounds, onLogoutRound } from "../store";
import admisionApi from "../api/admisionApi";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useRoundStore = () => {
    const dispatch = useDispatch();
    const { rounds } = useSelector((state) => state.round);

    const startSavingRound = async (round) => {
        try {
            const { data } = await admisionApi.post('/roundinscription/roundinscription/', round);
            dispatch(onAddNewRound(data));
            toast.success('Ronda creada.', MessageConfig);
        } catch (error) {
            toast.error('Error al guardar ronda.', MessageConfig);
        }
    }

    const startDeletingRound = async (round) => {
        try {
            await admisionApi.delete(`/roundinscription/roundinscription/${round.id}/`);
            dispatch(onDeleteRound(round));
        } catch (error) {
            toast.error('Error al eliminar ronda.', MessageConfig);
        }
    }

    const startLoadingRounds = async () => {
        try {
            const { data } = await admisionApi.get('/roundinscription/roundinscription/');
            dispatch(onLoadRounds(data));
        } catch (error) {
            toast.error('Error cargando las rondas.', MessageConfig);
        }
    }

    const startChangingStateRound = async (round) => {
        try {

            const { data } = await admisionApi.patch(`/roundinscription/roundinscription/${round.id}/`, { is_blocked: round.is_blocked })
            dispatch(onUpdateRound(data));
        } catch (error) {
            toast.error('Error al cambiar estado de la ronda', MessageConfig);
        }
    }

    return {
        rounds,
        startLoadingRounds,
        startSavingRound,
        startDeletingRound,
        startChangingStateRound
    }
}