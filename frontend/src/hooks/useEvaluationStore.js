import { useDispatch, useSelector } from "react-redux";
import { onAddNewEvaluation, onUpdateEvaluation, onDeleteEvaluation, onLoadEvaluations, onLogoutEvaluation, onLoadExams, onLoadAdmissionProcesses, onAddNewAdmissionProcess, onAddNewExam, onDeleteExam, onDeleteAdmissionProcess, onSelectCurrentEvaluation } from "../store";
import admisionApi from "../api/admisionApi";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useEvaluationStore = () => {
    const dispatch = useDispatch();
    const { evaluations, admissionProcesses, exams, currentEvaluation } = useSelector((state) => state.evaluation);

    const startSavingEvaluation = async (evaluation) => {
        try {
            const { data } = await admisionApi.post('/admissionprocess/evaluation/', evaluation);
            dispatch(onAddNewEvaluation(data));
            toast.success("Evaluación creada.", MessageConfig)
        } catch (error) {
            toast.error('Error al crear evaluación.', MessageConfig);
        }
    }
    const startDeletingEvaluation = async (evaluation) => {
        try {
            await admisionApi.delete(`/admissionprocess/evaluation/${evaluation.id}/`);
            dispatch(onDeleteEvaluation(evaluation));
            toast.success("Evaluación eliminada.", MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar evaluación.', MessageConfig);
        }
    }
    const startUpdatingEvaluation = async (evaluation) => {
        try {
            const { data } = await admisionApi.put(`/admissionprocess/evaluation/${evaluation.id}/`, evaluation);
            dispatch(onUpdateEvaluation(data));
            toast.success('Evaluación modificada.', MessageConfig);
        } catch (error) {
            toast.error('Error al editar evaluación.', MessageConfig);
        }
    }
    const startLoadingEvaluations = async () => {
        try {
            const { data } = await admisionApi.get('/admissionprocess/evaluation/');
            dispatch(onLoadEvaluations(data));
        } catch (error) {
            toast.error('Error al cargar las evaluaciones.', MessageConfig);
        }
    }
    const startSavingAdmissionProcess = async (admissionExam) => {
        try {
            const { data } = await admisionApi.post('/admissionprocess/admissionprocess/', admissionExam);
            dispatch(onAddNewAdmissionProcess(data));
            toast.success("Proceso de Admisión creado.", MessageConfig)
        } catch (error) {
            toast.error('Error al crear proceso de admisión.', MessageConfig);
        }
    }
    const startDeletingAdmissionProcess = async (admissionProcess) => {
        try {
            await admisionApi.delete(`/admissionprocess/admissionprocess/${admissionProcess.id}/`);
            dispatch(onDeleteAdmissionProcess(admissionProcess));
            toast.success("Proceso de Admisión eliminado.", MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar proceso de admisión.', MessageConfig);
        }
    }
    const startLoadingAdmissionProcesses = async () => {
        try {
            const { data } = await admisionApi.get('/admissionprocess/admissionprocess/');
            dispatch(onLoadAdmissionProcesses(data));
        } catch (error) {
            toast.error('Error al cargar los procesos de admisión.', MessageConfig);
        }
    }
    const startSavingExam = async (exam) => {
        try {
            const { data } = await admisionApi.post('/admissionprocess/exam/', exam);
            dispatch(onAddNewExam(data));
            toast.success("Examen creado.", MessageConfig)
        } catch (error) {
            toast.error('Error al crear examen.', MessageConfig);
        }
    }
    const startDeletingExam = async (exam) => {
        try {
            await admisionApi.delete(`/admissionprocess/exam/${exam.id}/`);
            dispatch(onDeleteExam(exam));
            toast.success("Examen eliminado.", MessageConfig)
        } catch (error) {
            toast.error('Error al eliminar examen.', MessageConfig);
        }
    }
    const startLoadingExams = async () => {
        try {
            const { data } = await admisionApi.get('/admissionprocess/exam/');
            dispatch(onLoadExams(data));
        } catch (error) {
            toast.error('Error al cargar los exámenes.', MessageConfig);
        }
    }

    const setCurrentEvaluation = (evaluation) => {
        dispatch(onSelectCurrentEvaluation(evaluation));
    }

    return {
        evaluations,
        currentEvaluation,
        admissionProcesses,
        exams,
        startSavingEvaluation,
        startDeletingEvaluation,
        startUpdatingEvaluation,
        setCurrentEvaluation,
        startLoadingEvaluations,
        startSavingAdmissionProcess,
        startDeletingAdmissionProcess,
        startLoadingAdmissionProcesses,
        startSavingExam,
        startDeletingExam,
        startLoadingExams
    }
}