import { useDispatch, useSelector } from "react-redux";
import { onChecking, onLogin, onLogout, clearErrorMessage, onLogoutRound, onLogoutEvaluation, onLogoutRegistration, onUpdateProfile } from "../store";
import loginApi from "../api/loginApi";
import admisionApi from "../api/admisionApi";
import { toast } from 'react-toastify';
import { MessageConfig } from "../modules/components/ToastMessage/MessageConfig";

export const useAuthStore = () => {
    const dispatch = useDispatch();
    const { status, user, errorMessage } = useSelector((state) => state.auth);

    const startLogin = async (tokenGoogle) => {
        dispatch(onChecking());
        try {
            localStorage.setItem('tokenGoogle', tokenGoogle.access_token);

            const { data } = await loginApi.post(`/auth/transformGoogleToken/`, {
                access_token: tokenGoogle.access_token
            })

            localStorage.setItem('x-token', data.access_token)

            dispatch(onLogin(data.user));
            toast.success('Autenticación correcta!!', MessageConfig)
        } catch (error) {
            toast.error('Credenciales incorrectas.', MessageConfig);
            dispatch(onLogout('Incorrect credentials.'));
            setTimeout(() => {
                dispatch(clearErrorMessage());
            }, 10)
        }
    }

    const startUpdateProfile = async (profile) => {
        try {
            const { data } = await admisionApi.patch(`/user/user/${profile.user}/`, {
                "work": profile.work
            })
            dispatch(onUpdateProfile(data));
            toast.success('Perfil actualizado.', MessageConfig);
        } catch (error) {
            toast.error('Error al actualizar el perfil.', MessageConfig);
        }
    }

    const startLogout = () => {
        localStorage.clear();
        dispatch(onLogout());
        dispatch(onLogoutRound());
        dispatch(onLogoutEvaluation());
        dispatch(onLogoutRegistration());
    }
    return {
        status,
        user,
        errorMessage,
        startUpdateProfile,
        startLogin,
        startLogout
    };
}