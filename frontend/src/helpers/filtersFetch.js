import admisionApi from "../api/admisionApi"

export const filtersFetch = () => {
    const getSchoolsByArea = async (area) => {
        try {
            const { data } = await admisionApi.get(`/school/?area=${area}`)
            return data;
        } catch (error) {
            return [];
        }
    }
    const getPavilionsBySchool = async (school) => {
        try {
            const { data } = await admisionApi.get(`/pavilion/?school=${school}`)
            return data;
        } catch (error) {
            return [];
        }
    }
    const getClassroomsByPavilion = async (pavilion) => {
        try {
            const { data } = await admisionApi.get(`/classroom/?pavilion=${pavilion}`)
            return data;
        } catch (error) {
            return [];
        }
    }
    const getEntrancesByArea = async (area) => {
        try {
            const { data } = await admisionApi.get(`/entrance/?area=${area}`);
            return data;
        } catch (error) {
            return [];
        }
    }

    return {
        getSchoolsByArea,
        getPavilionsBySchool,
        getClassroomsByPavilion,
        getEntrancesByArea
    }
}