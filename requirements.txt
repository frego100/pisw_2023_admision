#Django libraries
Django==5.0
djangorestframework==3.14.0
django-cors-headers==4.3.1
django-filter==23.5
djangorestframework-simplejwt==5.3.1
#Environment control
environs==9.5.0
#Database
psycopg2-binary==2.9.9
#Request
requests==2.31.0
drf-yasg
faker
reportlab==4.0.9
django-cleanup==8.1.0
xlsxwriter
pandas
openpyxl